import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';
import { BsModalModule } from 'ng2-bs3-modal';
import { NgxSpinnerModule } from 'ngx-spinner';

// http
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';

// Security
import { JwtModule } from '@auth0/angular-jwt';
export function tokenGetter() {
	return localStorage.getItem('access_token');
}

// service
import { Api } from './service/api';
import { Authentication } from './service/authentication';

//primeng
import { TableModule } from 'primeng/table';
import { ButtonModule } from 'primeng/button';
import { DropdownModule } from 'primeng/dropdown';
import { CheckboxModule } from 'primeng/checkbox';
import { CalendarModule } from 'primeng/calendar';
import { FileUploadModule } from 'primeng/fileupload';
import { RadioButtonModule} from 'primeng/radiobutton';
import { MultiSelectModule } from 'primeng/multiselect';
import {ChartModule} from 'primeng/chart';

// google map api
import { AgmCoreModule } from '@agm/core';
import { AgmSnazzyInfoWindowModule } from '@agm/snazzy-info-window';

// templating
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login';
import { SidebarComponent } from './sidebar/sidebar';
import { TopnavigationComponent } from './topnavigation/topnavigation';

// content
import { HomeComponent } from './content/home/home.component';
import { FooterComponent } from './footer/footer';
import { UserComponent } from './content/user/user';
import { ProfileComponent } from './content/profile/profile';
import { UserPrivilegeComponent } from './content/user-privilege/user-privilege';
import { BoxComponent } from './content/box/box';
import { BoxStandardComponent } from './content/box-standard/box-standard';
import { BoxTopupComponent } from './content/box-topup/box-topup';
import { SPBComponent } from './content/spb/spb';
import { TopupStokComponent } from './content/topup-stok/topup-stok';
import { TopupStokListComponent } from './content/topup-stok-list/topup-stok-list';
import { ChecklistComponent } from './content/checklist/checklist';
import { WarehouseStockComponent } from './content/warehouse-stock/warehouse-stock';
import { RencanaOperasiComponent } from './content/rencana-operasi/rencana-operasi';
import { WarehouseLocationComponent } from './content/warehouse-location/warehouse-location';
import { WarehouseStockOpnameComponent } from './content/warehouse-stock-opname/warehouse-stock-opname';
import { WarehouseStockOpnameOrderComponent } from './content/warehouse-stock-opname-order/warehouse-stock-opname-order';
import { WarehouseStockOpnameListComponent } from './content/warehouse-stock-opname-list/warehouse-stock-opname-list';
import { WarehouseStockOpnameResultComponent } from './content/warehouse-stock-opname-result/warehouse-stock-opname-result';
import { WarehouseReplacementComponent } from './content/warehouse-replacement/warehouse-replacement';
import { PengirimanBarangComponent } from './content/pengiriman-barang/pengiriman-barang';
import { PenggunaanBarangComponent } from './content/penggunaan-barang/penggunaan-barang';
import { PenarikanBarangComponent } from './content/penarikan-barang/penarikan-barang';
import { EconomicOrderComponent } from './content/economic-order/economic-order';
import { EconomicOrderAlertComponent } from './content/economic-order-alert/economic-order-alert';
import { DashboardComponent } from './content/dashboard/dashboard';
import { LaporanPemakaianComponent } from './content/laporan-pemakaian/laporan-pemakaian';
import { PurchaseRequestComponent } from './content/purchase-request/purchase-request';
import { PemesananBarangComponent } from './content/pemesanan-barang/pemesanan-barang';
import { PurchaseRequestDetailComponent } from './content/purchase-request/purchase-request-detail/purchase-request-detail';
import { PurchaseReceiveComponent } from './content/purchase-receive/purchase-receive';
import { PurchaseReceiveDetailComponent } from './content/purchase-receive/purchase-receive-detail/purchase-receive-detail';
import { AreaSalesComponent } from './content/report/area-sales/area-sales';
import { ProductQuantityComponent } from './content/report/product-quantity/product-quantity';
import { DeliveryPerformanceComponent } from './content/report/delivery-performance/delivery-performance';
import { InstrumentRusakComponent } from './content/instrument-rusak/instrument-rusak';
import { ReportInstrumentRusakComponent } from './content/report/report-instrument-rusak/report-instrument-rusak';
import { TopInstrumentComponent } from './content/report/top-instrument/top-instrument';
import { SummaryStokComponent } from './content/summary-stok/summary-stok';

// master
import { PrincipleComponent } from './content/master/principle/principle';
import { DoctorComponent } from './content/master/doctor/doctor';
import { HospitalComponent } from './content/master/hospital/hospital';
import { ProductComponent } from './content/master/product/product';
import { WarehouseComponent } from './content/master/warehouse/warehouse';
import { ProductDetailComponent } from './content/master/product-detail/product-detail';
import { DiagnosaCaseComponent } from './content/master/diagnosa-case/diagnosa-case';
import { DiagnosaSubcaseComponent } from './content/master/diagnosa-subcase/diagnosa-subcase';
import { JenisTagihanComponent } from './content/master/jenis-tagihan/jenis-tagihan';
import { SpineAnatomiComponent } from './content/master/spine-anatomi/spine-anatomi';
import { KendaraanComponent } from './content/master/kendaraan/kendaraan';
import { DivisionComponent } from './content/master/division/division';
import { AreaComponent } from './content/master/area/area';
import { SubareaComponent } from './content/master/subarea/subarea';
import { SubdistComponent } from './content/master/subdist/subdist';

// pipe
import { DatetimediffPipe } from './pipe/datetimediff.pipe';
import { ProductDetailNamePipe } from './pipe/product-detail-name.pipe';
import { CountArrayPipe } from './pipe/countarray';

// report
import { TopPrincipleComponent } from './content/report/top-principle/top-principle';
import { TopImplantComponent } from './content/report/top-implant/top-implant';
import { PotongRodComponent } from './content/potong-rod/potong-rod';




@NgModule({
	declarations: [
	AppComponent,
	LoginComponent,
	SidebarComponent,
	TopnavigationComponent,
	HomeComponent,
	FooterComponent,
	UserComponent,
	ProfileComponent,
	PrincipleComponent,
	DoctorComponent,
	HospitalComponent,
	ProductComponent,
	WarehouseComponent,
	WarehouseStockComponent,
	RencanaOperasiComponent,
	WarehouseLocationComponent,
	WarehouseLocationComponent,
	ProductDetailComponent,
	DiagnosaCaseComponent,
	DiagnosaSubcaseComponent,
	JenisTagihanComponent,
	SpineAnatomiComponent,
	UserPrivilegeComponent,
	DatetimediffPipe,
	BoxComponent,
	BoxStandardComponent,
	BoxTopupComponent,
	SPBComponent,
	ProductDetailNamePipe,
	CountArrayPipe,
	TopupStokComponent,
	TopupStokListComponent,
	ChecklistComponent,
	WarehouseStockOpnameComponent,
	WarehouseStockOpnameOrderComponent,
	WarehouseStockOpnameListComponent,
	WarehouseStockOpnameResultComponent,
	WarehouseReplacementComponent,
	PengirimanBarangComponent,
	PenggunaanBarangComponent,
	PenarikanBarangComponent,
	EconomicOrderComponent,
	EconomicOrderAlertComponent,
	DashboardComponent,
	LaporanPemakaianComponent,
	PurchaseRequestComponent,
	PemesananBarangComponent,
	KendaraanComponent,
	DivisionComponent,
	AreaComponent,
	SubareaComponent,
	TopPrincipleComponent,
	PurchaseRequestDetailComponent,
	PurchaseReceiveComponent,
	PurchaseReceiveDetailComponent,
	AreaSalesComponent,
	ProductQuantityComponent,
	DeliveryPerformanceComponent,
	InstrumentRusakComponent,
	ReportInstrumentRusakComponent,
	TopInstrumentComponent,
	TopImplantComponent,
	SummaryStokComponent,
	SubdistComponent,
	PotongRodComponent,
	],
	imports: [
	BrowserModule,
	AgmCoreModule.forRoot({
		apiKey:'AIzaSyAN1nyNYatLuzdle7u-mY36a3jjiwcXVI8'
	}),
	ToastrModule.forRoot({
		timeOut: 2000,
		positionClass: 'toast-bottom-right',
		preventDuplicates: true,
	}),
	JwtModule.forRoot({
		config: {
			tokenGetter: tokenGetter,
			whitelistedDomains: ['localhost:3001'],
			blacklistedRoutes: ['localhost:3001/auth/']
		}
	}),
	AgmSnazzyInfoWindowModule,
	AppRoutingModule,
	HttpModule,
	HttpClientModule,
	TableModule,
	ButtonModule,
	FormsModule,
	ReactiveFormsModule,
	BrowserAnimationsModule,
	BsModalModule,
	NgxSpinnerModule,
	DropdownModule,
	CheckboxModule,
	CalendarModule,
	FileUploadModule,
	RadioButtonModule,
	MultiSelectModule,
	ChartModule
	],
	providers: [
	Api,
	Authentication,
	],
	bootstrap: [AppComponent]
})
export class AppModule { }
