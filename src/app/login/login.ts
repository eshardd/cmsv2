import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { Authentication } from '../service/authentication';

import { NgxSpinnerService } from 'ngx-spinner';
import { JwtHelperService } from '@auth0/angular-jwt';

@Component({
	selector: 'app-login',
	templateUrl: './login.html',
	styleUrls: ['./login.css']
})
export class LoginComponent {

	public login_form  : FormGroup;

	constructor(
		private _router				: Router,
		private _fb					: FormBuilder,
		private _ts					: ToastrService,
		private _authentication		: Authentication,
		private _spinner			: NgxSpinnerService,
		private _jwtHelperService	: JwtHelperService
	) {
		this.login_form = _fb.group({
			'email'   : [null, Validators.email],
			'password': [null, [Validators.minLength(5),Validators.maxLength(16),Validators.required]]
		});
	}

	ngOnInit() {
	}

	login(){
		this._spinner.show();
		this._authentication.login(this.login_form.value).subscribe((data: any)=> {
			if (data.user != null) {
				this._ts.success('Success', data.message);
				localStorage.setItem('currentUser', JSON.stringify({ data: data.user,token: data.token }));
				if(data.user.level == 1){
					this._router.navigate(['dashboard']);
				}else{
					this._router.navigate(['profile']);
				}
				location.reload();
			}else{
				this._ts.warning('warning',data.message);
			}
		},(error: any) => {

		},() => {
			this._spinner.hide();
		});
	}
}