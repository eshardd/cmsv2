import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { BsModalComponent  } from 'ng2-bs3-modal';
import { NgxSpinnerService } from 'ngx-spinner';
import { Api } from '../../service/api';

@Component({
  selector: 'app-warehouse-replacement',
  templateUrl: './warehouse-replacement.html',
  styleUrls: ['./warehouse-replacement.css']
})
export class WarehouseReplacementComponent implements OnInit {
	public title_table 			: string = 'Tabel Replacement';

	public privilege_list		: any = JSON.parse(localStorage.getItem('currentUser')).data.privilege_list;

	public maindata				: any;
	public cols					: any[];
	public add_form				: FormGroup;
	public edit_form			: FormGroup;
	public id_rowselect 		: number;
	public sisa_in				: any;
	public sisa_out				: any;
	public sisa_out_validator	: number = 1;

	//dropdown
	public dropdown_temp			: any;
	public dropdown			: any;

	// effect animate
	public animate_effect		: string = '';

	//flag
	public button_export		: boolean = false;

	@ViewChild('create')	public modalCreate	: BsModalComponent;
	@ViewChild('edit')		public modalEdit	: BsModalComponent;

	constructor(
		private _api		: Api,
		private _fb			: FormBuilder,
		private _ts			: ToastrService,
		private _spinner	: NgxSpinnerService
	) {

		this.add_form = _fb.group({
			'product_in'					: [null, Validators.required],
			'product_out'					: [null, Validators.required],
			'product_detail_in'				: [null, Validators.required],
			'product_detail_out'			: [null, Validators.required],
			'quantity'	  					: [null, [Validators.required, Validators.min(0)]],
		});

		this.edit_form = _fb.group({
			'product_in'					: [null, Validators.required],
			'product_out'					: [null, Validators.required],
			'product_detail_in'				: [null, Validators.required],
			'product_detail_out'			: [null, Validators.required],
			'quantity'	  					: [null, [Validators.required, Validators.min(0)]],
		});
	}

	/* -- Lifecycle Hooks Code -- */
	ngOnInit() {
    	this.getMainData();
   		this.getDataDropdown();
	}

	/* -- Function Code -- */
	OpenModal() {
		this.modalCreate.open();
	}

	handleRowSelect(event: any){
    	this.id_rowselect = event.id;
		this.edit_form.get('product_in').setValue(event.product_in);
    	this.edit_form.get('product_out').setValue(event.product_out);

    	this.dropdown['product_detail_in'] = this.dropdown_temp.product_detail.filter( x => x['id_product'] === event.product_in);
		this.dropdown['product_detail_in'].unshift({label:'-Select-', value:null});
    
    	this.dropdown['product_detail_out'] = this.dropdown_temp.product_detail.filter( x => x['id_product'] === event.product_out);
		this.dropdown['product_detail_out'].unshift({label:'-Select-', value:null});

		this.edit_form.get('product_detail_in').setValue(event.product_detail_in);
		this.edit_form.get('product_detail_out').setValue(event.product_detail_out);
		this.edit_form.get('quantity').setValue(event.quantity);

		this.sisa_in = [];
		this._api.getData('replacement/item_stock/'+event.product_detail_in).subscribe((data: any)=> {
			this.sisa_in['wh'] = data.sisa;
			this.sisa_in['box'] = data.inbox;
		});

		this.sisa_out = [];
		this._api.getData('replacement/item_stock/'+event.product_detail_out).subscribe((data: any)=> {
			this.sisa_out['wh'] = data.sisa;
			this.sisa_out['box'] = data.inbox;
			this.sisa_out_validator = data.sisa;
		});

    	this.modalEdit.open();
	}

	getMainData(){
		// setup column P-table
		this.cols = [
			{ field: 'name', header: 'Nama' },
		];

		// get data from API
		this._api.getData('replacement/all').subscribe((data: any)=> {
			this.maindata = data;
		},(error: any) => {
				
		},() => {
			this._spinner.hide();
		});
	}

 	getDataDropdown(){
		this.dropdown = [];
		this._api.getData('dropdown/replacement').subscribe((data: any)=> {
			this.dropdown_temp 					    = data;
			this.dropdown['product_in'] 				= data.product;
      		this.dropdown['product_out'] 				= data.product;
      		this.dropdown['product_in'].unshift({label:'-Pilih-', value:null});
		},(error: any) => {
				
		},() => {
			this._spinner.hide();
		});
	}

	select_product_in(id_product){
		this.dropdown['product_detail_in'] = this.dropdown_temp.product_detail.filter( x => x['id_product'] === id_product);
		this.dropdown['product_detail_in'].unshift({label:'-Select-', value:null});
	}

	select_product_detail_in(id_product_detail){
		this.sisa_in = [];
		this._api.getData('replacement/item_stock/'+id_product_detail).subscribe((data: any)=> {
			this.sisa_in['wh'] = data.sisa;
			this.sisa_in['box'] = data.inbox;
		});
	}

	select_product_detail_out(id_product_detail){
		this.sisa_out = [];
		this._api.getData('replacement/item_stock/'+id_product_detail).subscribe((data: any)=> {
			this.sisa_out['wh'] = data.sisa;
			this.sisa_out['box'] = data.inbox;
			this.sisa_out_validator = data.sisa;
		});
	}

	select_product_out(id_product){
		this.dropdown['product_detail_out'] = this.dropdown_temp.product_detail.filter( x => x['id_product'] === id_product);
		this.dropdown['product_detail_out'].unshift({label:'-Select-', value:null});
	}

	onSubmit(){
		this.add_form.value.quantity

		if (this.add_form.value.quantity > this.sisa_out_validator) {
			alert("Stok Tidak Mencukupi untuk replacement");
		}else{
			this.add_form.value.user_id = JSON.parse(localStorage.getItem('currentUser')).data.id;
			this._api.insertData('replacement/add', this.add_form.value).subscribe((data: any) => {
				this._ts.success('Success', data.message);
				this.getMainData();
				this.modalCreate.dismiss();
			},(error: any) => {
				
			},() => {
				this._spinner.hide();
			});
			
			this.animate_effect = 'swing';
		}
	}
		
	onSubmitEdit(){
		this.edit_form.value.id = this.id_rowselect;
		
		if ((this.edit_form.value.quantity * 1 ) > this.sisa_out_validator) {
			alert("Stok Tidak Mencukupi untuk replacement");
		}else{
			this._api.updateData('replacement/edit', this.edit_form.value).subscribe((data: any) => {
				this._ts.success('Success', data.message);
				this.getMainData();
				this.modalEdit.dismiss();
			},(error: any) => {
					
			},() => {
				this._spinner.hide();
			});
		}
	}

	integration(event: any){
		if(confirm("Apakah anda yakin ingin Menyesuaikan dengan stok gudang? Stok Gudang akan otomatis Disesuaikan dalam proses Integrasi")) {
			this._api.insertData('replacement/integrate', event).subscribe((data: any) => {
				this._ts.success('Success', data.message);
				this.getMainData();
				this.modalCreate.dismiss();
			},(error: any) => {
				
			},() => {
				this._spinner.hide();
			});
		}
	}

}
