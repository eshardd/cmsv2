import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';
import { Api } from '../../service/api';
import { Wh_store_data } from '../../master/class';


@Component({
  selector: 'app-topup-stok',
  templateUrl: './topup-stok.html',
  styleUrls: ['./topup-stok.css']
})
export class TopupStokComponent implements OnInit {
	public selected_product_detail 	: string;

	// data , table dan kolom
	public store_data			= new Wh_store_data();
	public store_temp1			= new Array;
	public store_temp2			= new Array;
	public sisa_stok			: any;
	public cols					: any[];
	public filter_form			: FormGroup;
	
	//dropdown
	public dropdown_form_temp	: any;
	public dropdown_form		: any;

	constructor(
		private _api		: Api,
		private _fb			: FormBuilder,
		private _ts			: ToastrService,
		private _spinner	: NgxSpinnerService
	) {

		this.filter_form 	= _fb.group({
			'transfer_to'		: [null, Validators.required],
			'principle'			: [null, Validators.required],
			'product'			: [null, Validators.required],
			'product_detail'	: [null, Validators.required],
			'quantity'			: [null, Validators.required],
		});
	}

	/* -- Lifecycle Hooks Code -- */
	ngOnInit() {
		this.getDataForm();
	}

	getDataForm(){
		this.dropdown_form = [];
		this._api.getData('warehouse-stock/form_dropdown').subscribe((data: any)=> {
			this.dropdown_form_temp = data;
			this.dropdown_form['warehouse'] 		= data.warehouse;
			this.dropdown_form['principle'] 		= data.principle;
			this.dropdown_form['product'] 			= [];
			this.dropdown_form['product_detail'] 	= [];
			this.dropdown_form['warehouse'].unshift({label:'-Select-', value:null});
			this.dropdown_form['principle'].unshift({label:'-Select-', value:null});
		});
	}

	getData(selected_product_detail,wh_pusat,wh_tujuan){
		this.sisa_stok = [];
		this._api.getData('warehouse-stock/wh_topup/'+wh_pusat+'/'+wh_tujuan+'/'+selected_product_detail).subscribe((data: any)=> {
			this.sisa_stok['pusat'] = data.sisa_pusat.sisa;
			this.sisa_stok['tujuan'] = data.sisa_tujuan.sisa;
		});
	}

	// ==================================== Chain Select ================================================
	select_warehouse(id_warehouse){
		//this.filter_form.reset();
		this.store_temp1 = new Array;
		let wh_pusat = 1;
		let wh_tujuan = this.filter_form.value.transfer_to;
		this.getData(this.filter_form.value.product_detail,wh_pusat,wh_tujuan);
	}
	select_principle(id_principle){
		this.dropdown_form['product'] = this.dropdown_form_temp.product.filter( x => x['id_principle'] === id_principle);
		this.dropdown_form['product'].unshift({label:'-Select-', value:null});
	}
	select_product(id_product){
		this.dropdown_form['product_detail'] = this.dropdown_form_temp.product_detail.filter( x => x['id_product'] === id_product);
		this.dropdown_form['product_detail'].unshift({label:'-Select-', value:null});
	}
	select_product_detail(id_product_detail){	
		let wh_pusat = 1;
		let wh_tujuan = this.filter_form.value.transfer_to;
		this._spinner.show();
		this.getData(id_product_detail,wh_pusat,wh_tujuan);
		this.onSubmitFilter();
		this._spinner.hide();
	}
	//======================================================================================================

	onStore(){
		this.filter_form.value.id_user = JSON.parse(localStorage.getItem('currentUser')).data.id;

		this.store_data.from 	= "1";
		this.store_data.to 		= this.filter_form.value.transfer_to;
		this.store_data.id_user = this.filter_form.value.id_user;

		this._api.getData('warehouse-stock/product_detail_name/'+this.filter_form.value.product_detail).subscribe((data: any)=> {
			var valueToPush = { };
			valueToPush["product_detail"] 		= this.filter_form.value.product_detail;
			valueToPush["product_detail_name"] 	= data.code+' | '+data.name;
			valueToPush["quantity"] 			= this.filter_form.value.quantity;

			// prevent duplicate row with same id
			var store = true;
			for( var i = 0, len = this.store_temp1.length; i < len; i++ ) {
				if( this.store_temp1[i]['product_detail'] == this.filter_form.value.product_detail ) {
					if ((this.store_temp1[i]['quantity']*1 + this.filter_form.value.quantity*1) > this.sisa_stok['pusat']*1) {
						alert("Stok Gudang Tidak Mencukupi");
					}else{
						this.store_temp1[i]['quantity'] = this.store_temp1[i]['quantity']*1 + this.filter_form.value.quantity*1; 
					}
					
					store = false;
				}
			}
			if (store == true) {
				this.store_temp1.push(valueToPush);
			}
		});
		

	}

	onSubmit(){
		this.filter_form.value.id_user = JSON.parse(localStorage.getItem('currentUser')).data.id;
		this._api.insertData('warehouse-stock/transfer', this.filter_form.value).subscribe((data: any) => {
			this._ts.success('Success', data.message);
		});
	}

	submitpost(){
		if(confirm("Apakah anda yakin ingin Topup Box ini? Stok Gudang akan otomatis dikurangi dalam proses pengisian")) {
			this._spinner.show();
			this.store_data.data = this.store_temp1;
			this._api.insertData('warehouse-stock/topup_external', this.store_data).subscribe((data: any) => {
				this._ts.success('Success', data.message);
				this.getData(this.filter_form.value.product_detail,1,this.filter_form.value.transfer_to);
				this.store_temp1 = new Array;
				this._spinner.hide();
			});
		}
	}

	onSubmitFilter(){
		var label = this.dropdown_form_temp.product_detail.filter( x => x['value'] === this.filter_form.value.product_detail);
		this.selected_product_detail = label[0]['label'];
	}

	removeList(selected_data:any){
		let data = this.store_temp1.filter( x => x['product_detail'] != selected_data.product_detail);
		this.store_temp1 = data;
	}

	numberOnly(event) {
		const charCode = (event.which) ? event.which : event.keyCode;
		if (charCode > 31 && (charCode < 48 || charCode > 57)) {
		  return false;
		}
		return true;
	}
}
