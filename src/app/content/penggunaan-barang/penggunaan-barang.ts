import { Component, OnInit, Input, ViewChild, ɵConsole } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { BsModalComponent  } from 'ng2-bs3-modal';
import { NgxSpinnerService } from 'ngx-spinner';
import { Api } from '../../service/api';
import { Info_Rencana_Operasi } from '../../master/class';
import { environment } from '../../../environments/environment';

export class PenggunaanBarang{
	id_ro;
	id_user;
	data;
}

class count_data {
	all: any;
	sync: any;
	unsync: any;
	notcount: any;
}


@Component({
  selector: 'app-penggunaan-barang',
  templateUrl: './penggunaan-barang.html',
  styleUrls: ['./penggunaan-barang.css']
})
export class PenggunaanBarangComponent implements OnInit {
	public title_table 			: string = 'Tabel Diagnosa Case';

	public privilege_list		: any = JSON.parse(localStorage.getItem('currentUser')).data.privilege_list;
	public user_division		: any = JSON.parse(localStorage.getItem('currentUser')).data.division;

	public selected_box				: any;
	public selected_product_detail	: any;
	public selected_product_code	: any;

	public selected_product_detail_pendukung	: any;
	public selected_product_code_pendukung	: any;

  	//store temp
	public store_data			= new PenggunaanBarang();
	public store_temp1			= new Array;
	public store_temp2			= new Array;

	public maindata				: any;
	public temp_maindata		: any;
	public count_maindata		= new count_data();

	public detaildata			: any;
	public cols					: any[];
	public add_form				: FormGroup;
	public edit_form			: FormGroup;
	public update_ro_form		: FormGroup;
	public tambahan_form		: FormGroup;
	public filter_form			: FormGroup;
	public id_rowselect 		: number;
	public id_edit_rowselect 	: number;
	public info_ro 				= new Info_Rencana_Operasi();

	//dropdown
	public dropdown_temp		: any;
	public dropdown_temp_ro		: any;
	public dropdown				: any;
	public dropdown_edit		: any;
	public dropdown_tambahan	: any;


	// effect animate
	public animate_effect		: string = '';

	//flag
	public button_export		: boolean = false;
	public button_sinkron		: boolean = false;
	public integrated_status	: boolean = false;
	public show_update_form		: boolean = false;
	public loading		: boolean = false;
	public allow_revisi : boolean = false;

	public API_URL = 'http://'+environment.API_URL;

	@ViewChild('create')	public modalCreate	: BsModalComponent;
	@ViewChild('edit')		public modalEdit	: BsModalComponent;
	@ViewChild('tambahan')	public modalTambahan: BsModalComponent;

	constructor(
		private _api		: Api,
		private _fb			: FormBuilder,
		private _ts			: ToastrService,
		private _spinner	: NgxSpinnerService
	) {

		this.add_form = _fb.group({
			'id_ro'					    	: [null],
			'id_box'						: [null],
			'id_product_detail'				: [null],
			'quantity'						: [null],
			'id_product_detail_pendukung'	: [null],
			'quantity_pendukung'			: [null],
		});

		this.edit_form = _fb.group({
			'id_box'					: [null, Validators.required],
			'id_product_detail'			: [null, Validators.required],
			'quantity'					: [null, Validators.required],
			'id_product_detail_pendukung'	: [null],
			'quantity_pendukung'			: [null],
		});

		this.update_ro_form = _fb.group({
			'id_ro'		: [null, Validators.required],
			'name'		: [null, Validators.required],
			'no_mr'		: [null, Validators.required],
			'case'		: [null, Validators.required],
			'subcase'		: [null, Validators.required],
		});

		this.tambahan_form = _fb.group({
			'id'				: [null],
			'product'			: [null],
			'product_detail'	: [null],
			'quantity'			: [null],
		});

		this.filter_form 	= _fb.group({
			'date': [''],
			'month': [''],
			'year': [''],
		});
	}

	/* -- Lifecycle Hooks Code -- */
	ngOnInit() {
		if(this.user_division == '1' || this.user_division == '3'){
			this.button_sinkron = true;
			this.allow_revisi = true;
		}
		console.log(this.allow_revisi);
		this.getMainData();
		this.getDropDown();
		this.dropdown_edit = [];
	}

	/* -- Function Code -- */
	OpenModal() {
		this.add_form.reset();
		this.update_ro_form.reset();
		this.info_ro = new Info_Rencana_Operasi();
		this.store_temp1 = [];
		this.modalCreate.open('lg');
	}

	handleRowSelect(event: any){
		this.edit_form.reset();

		this.id_rowselect = event.id; // RO

		if (event.integrated == 0){
			this.integrated_status = false;
		}else{
			this.integrated_status = true;
		}

		
		this._api.getData('spb/list/'+event.id).subscribe((data: any)=> {
			this.info_ro.id = data.id;
			this.info_ro.no_ro = data.no_ro;
			this.info_ro.hospital_name = data.hospital_name;
			this.info_ro.doctor_name = data.doctor_name;
			this.info_ro.request_instrument_name = data.request_instrument_name;
			this.info_ro.jenis_tagihan_name = data.jenis_tagihan_name;
			this.info_ro.pasien = data.pasien;
			this.info_ro.no_mr = data.no_mr;
			this.info_ro.type_ro_name = data.type_ro_name;
			this.info_ro.remarks = data.remarks;
			this.info_ro.status_box = data.status_box;
			this.info_ro.date = data.date;
			this.info_ro.integrated = data.integrated;
			this.update_ro_form.get('id_ro').setValue(data.id);
			this.update_ro_form.get('name').setValue(data.pasien);
			this.update_ro_form.get('no_mr').setValue(data.no_mr);
		});

		this._api.getData('penggunaan-barang/detail/'+this.id_rowselect).subscribe((data: any)=> {
			this.detaildata = data;
		});

		this.dropdown['product'] = this.dropdown_temp.product.filter( x => x['id_ro'] === event.id);
		this.dropdown['product'].unshift({label:'-Pilih-', value:null, disabled:true});
		
		this.modalEdit.open('lg');
	}

	product_request(event: any){
		this.id_rowselect = event.id; // RO

		if (event.integrated == 0){
			this.integrated_status = false;
		}else{
			this.integrated_status = true;
		}

		this._api.getData('penggunaan-barang/detail/'+this.id_rowselect).subscribe((data: any)=> {
			this.detaildata = data;
		});
		
		this.modalTambahan.open('lg');
	}

	edit_data_detail(event){
		this.edit_form.reset();
		if(event.data.code_box == null){
			this.id_edit_rowselect = event.data.id;
			this.edit_form.get('quantity').setValue(event.data.quantity);
		}else{
			this.id_edit_rowselect = event.data.id;
			this._api.getData('penggunaan-barang/list_box/'+this.id_rowselect).subscribe((data: any)=> {
				this.dropdown_edit['box'] = data;
			});
			this._api.getData('penggunaan-barang/list_item/'+event.data.id_box).subscribe((data: any)=> {
				this.dropdown_edit['product_detail'] = data;
			});
			this.edit_form.get('id_box').setValue(event.data.id_box);
			this.edit_form.get('id_product_detail').setValue(event.data.id_product_detail);
			this.edit_form.get('quantity').setValue(event.data.quantity);
		}
	}

	edit_quantity_pendukung(){
		// get dulu id record nya
		this.edit_form.value.id = this.id_edit_rowselect;
		this._api.updateData('penggunaan-barang/edit_alat_pendukung', this.edit_form.value).subscribe((data: any) => {
			this._ts.success('Success', data.message);
			this.getMainData();
			this._api.getData('penggunaan-barang/detail/'+this.id_rowselect).subscribe((data: any)=> {
				this.detaildata = data;
			});
		},(error: any) => {
				
		},() => {
			this._spinner.hide();
		});
	}

	getMainData(){
		// get data from API
		this._api.getData('penggunaan-barang/all').subscribe((data: any)=> {
			this.maindata = data;
			this.temp_maindata = data;
			this.calculate_quantity(data);
		});
	 }

	 print(event){
		//console.log(event);
		window.open(this.API_URL+'/print/penggunaan/'+event.id,'_blank');
	}
	 
	 calculate_quantity(data){
		this.temp_maindata = data;
		this.count_maindata.all = data.length;
		this.count_maindata.sync = data.filter( x => x['integrated'] == 1).length;
		this.count_maindata.unsync = data.filter( x => x['integraed'] == 0).length;
	}
  
  	getDropDown(){
		// get data from API
    	this.dropdown = [];
    	this.dropdown_tambahan = [];
    	this._api.getData('dropdown/penggunaan-barang').subscribe((data: any)=> {
			this.dropdown_temp = data;
			this.dropdown['rencana_operasi'] = data.rencana_operasi;
			this.count_maindata.notcount = this.dropdown['rencana_operasi'].length;
			this.dropdown['rencana_operasi'].unshift({label:'-Pilih-', value:null, disabled:true});
			this.dropdown_tambahan['product'] = data.product_all;
			this.dropdown_tambahan['product'].unshift({label:'-Pilih-', value:null, disabled:true});
		});
		this._api.getData('rencana-operasi/dropdown').subscribe((data: any) => {
			this.dropdown_temp_ro = data;
			this.dropdown['case'] = data.case;
			this.dropdown['subcase'] = data.subcase;
			this.dropdown['case'].unshift({ label: '-Pilih-', value: null });
			this.dropdown['subcase'].unshift({ label: '-Pilih-', value: null });
		});

		this.dropdown['date'] = [
			{value:1,label:'1'},
			{value:2,label:'2'},
			{value:3,label:'3'},
			{value:4,label:'4'},
			{value:5,label:'5'},
			{value:6,label:'6'},
			{value:7,label:'7'},
			{value:8,label:'8'},
			{value:9,label:'9'},
			{value:10,label:'10'},
			{value:11,label:'11'},
			{value:12,label:'12'},
			{value:13,label:'13'},
			{value:14,label:'14'},
			{value:15,label:'15'},
			{value:16,label:'16'},
			{value:17,label:'17'},
			{value:18,label:'18'},
			{value:19,label:'19'},
			{value:20,label:'20'},
			{value:21,label:'21'},
			{value:22,label:'22'},
			{value:23,label:'23'},
			{value:24,label:'24'},
			{value:25,label:'25'},
			{value:26,label:'26'},
			{value:27,label:'27'},
			{value:28,label:'28'},
			{value:29,label:'29'},
			{value:30,label:'30'},
			{value:31,label:'31'},
		];

		this.dropdown['month'] = [
			{value:1,label:'Jan'},
			{value:2,label:'Feb'},
			{value:3,label:'Mar'},
			{value:4,label:'Apr'},
			{value:5,label:'Mei'},
			{value:6,label:'Jun'},
			{value:7,label:'Jul'},
			{value:8,label:'Aug'},
			{value:9,label:'Sep'},
			{value:10,label:'Oct'},
			{value:11,label:'Nov'},
			{value:12,label:'Des'},
		];

		this.dropdown['year'] = [
			{value:2019,label:'2019'},
		];

		this.dropdown['date'].unshift({ label: '-all-', value: null });
		this.dropdown['month'].unshift({ label: '-all-', value: null });
		this.dropdown['year'].unshift({ label: '-all-', value: null });
  	}

  	add_to_table(){
		var valueToPush = { };
		if(this.selected_product_detail == undefined){
			alert("Tidak ada Item yang dipilih");
		}else{
			valueToPush["product_detail"] 		= this.add_form.value.id_product_detail;
			valueToPush["product_detail_code"] 	= this.selected_product_code;
			valueToPush["product_detail_name"] 	= this.selected_product_detail;
			valueToPush["id_box"] 				= this.add_form.value.id_box;
			valueToPush["box_code"] 			= this.selected_box;
			valueToPush["quantity"] 			= this.add_form.value.quantity;
			

			//prevent duplicate row with same id
			var store = true;
			if(this.add_form.value.quantity > 0){
				for( var i = 0, len = this.store_temp1.length; i < len; i++ ) {
					if( this.store_temp1[i]['product_detail'] == this.add_form.value.id_product_detail ) {
						this.store_temp1[i]['quantity'] = this.store_temp1[i]['quantity']*1 + this.add_form.value.quantity*1; 					
						store = false;
					}
				}
				if (store == true) {
					this.store_temp1.push(valueToPush);
				}
			}else{
				alert("quantity harus lebih dari 0");
			}
		}
	}

	onSubmitFilter(){
		this.loading = true;
		this._api.insertData('penggunaan-barang/filter',this.filter_form.value).subscribe((data: any) => {
			this.maindata = data;
			this.temp_maindata = data;
			this.calculate_quantity(data);
			this.loading = false;
		});
	}

	revisi(){
		this.edit_form.value.id_ro = this.id_rowselect;
		if(confirm("Apakah anda yakin ingin Melakukan Revisi data penggunaan ini?")) {
			this._api.updateData('penggunaan-barang/revisi', this.edit_form.value).subscribe((data: any) => {
				this._ts.success('Success', data.message);
				this.getMainData();
				this.getDropDown();
				this._api.getData('penggunaan-barang/detail/'+this.id_rowselect).subscribe((data: any)=> {
					this.detaildata = data;
				});
			},(error: any) => {

			},() => {
				this._spinner.hide();
			});
		}
	}

	add_to_table_pendukung(){
		var valueToPush = { };
		if(this.selected_product_detail_pendukung == undefined){
			alert("Tidak ada Item yang dipilih");
		}else{

			valueToPush["product_detail"] 		= this.add_form.value.id_product_detail_pendukung;
			valueToPush["product_detail_code"] 	= this.selected_product_code_pendukung;
			valueToPush["product_detail_name"] 	= this.selected_product_detail_pendukung;
			valueToPush["id_box"] 				= null;
			valueToPush["box_code"] 			= null;
			valueToPush["quantity"] 			= this.add_form.value.quantity_pendukung;
			
			//prevent duplicate row with same id
			var store = true;
			if(this.add_form.value.quantity_pendukung > 0){
				for( var i = 0, len = this.store_temp1.length; i < len; i++ ) {
					if( this.store_temp1[i]['product_detail'] == this.add_form.value.id_product_detail_pendukung ) {
						this.store_temp1[i]['quantity'] = this.store_temp1[i]['quantity']*1 + this.add_form.value.quantity_pendukung*1; 					
						store = false;
					}
				}
				if (store == true) {
					this.store_temp1.push(valueToPush);
				}
			}else{
				alert("quantity harus lebih dari 0");
			}
		}
	}

	onChangeDiagnosaCase(event) {
		this.dropdown['subcase'] = this.dropdown_temp_ro.subcase.filter(x => x['id_case'] === event.value);
		this.dropdown['subcase'].unshift({ label: '-Pilih-', value: null });
	}

	onChangeProductTambahan(event) {
		var id_product = event.value;
		this._api.getData('laporan-pemakaian/product_item_list/'+id_product).subscribe((data: any)=> {
			this.dropdown_tambahan['product_detail'] = data.product_detail;
			this.dropdown_tambahan['product_detail'].unshift({ label: '-Pilih-', value: null });
		});
	}

	add_tambahan_to_table(){
		// get dulu id record nya
		this.tambahan_form.value.id = this.id_rowselect;
		
		if(this.tambahan_form.value.quantity > 0){

			this._api.insertData('penggunaan-barang/tambahan', this.tambahan_form.value).subscribe((data: any) => {
				this._ts.success('Success', data.message);
			});
			
			this._api.getData('penggunaan-barang/detail/'+this.id_rowselect).subscribe((data: any)=> {
				this.detaildata = data;
			});
		}else{
			alert("quantity harus lebih dari 0");
		}
	}
	
	removeList(selected_data:any){
		let data = this.store_temp1.filter( x => x['product_detail'] != selected_data.product_detail);
		this.store_temp1 = data;
	}

	removeList_edit(selected_data:any){
		if(confirm("Apakah anda yakin ingin Menghapus data penggunaan ini?")) {
			this._api.deleteData('penggunaan-barang/delete/'+selected_data.id).subscribe((data: any) => {
				this._ts.success('Success', data.message);
				this._api.getData('penggunaan-barang/detail/'+this.id_rowselect).subscribe((data: any)=> {
					this.detaildata = data;
				});
			});
		}
	}

	

	select_ro(event){
		console.log(event);
		this.add_form.get('id_box').setValue(null);
		this.add_form.get('id_product_detail').setValue(null);
		this.add_form.get('quantity').setValue(null);
		this.add_form.get('id_product_detail_pendukung').setValue(null);
		this.add_form.get('quantity_pendukung').setValue(null);

		this.show_update_form = true;
		this._api.getData('spb/list/'+event).subscribe((data: any)=> {
			this.info_ro.id = data.id;
			this.info_ro.no_ro = data.no_ro;
			this.info_ro.hospital_name = data.hospital_name;
			this.info_ro.doctor_name = data.doctor_name;
			this.info_ro.request_instrument_name = data.request_instrument_name;
			this.info_ro.jenis_tagihan_name = data.jenis_tagihan_name;
			this.info_ro.pasien = data.pasien;
			this.info_ro.type_ro_name = data.type_ro_name;
			this.info_ro.remarks = data.remarks;
			this.info_ro.status_box = data.status_box;
			this.info_ro.date = data.date;
			this.info_ro.no_mr = data.no_mr;
			this.update_ro_form.get('id_ro').setValue(data.id);
			this.update_ro_form.get('name').setValue(data.pasien);
			this.update_ro_form.get('no_mr').setValue(data.no_mr);
			this.update_ro_form.get('case').setValue(data.case);
			this.update_ro_form.get('subcase').setValue(data.subcase);
		});
		// get alat pendukung yang digunakan di RO ini
		this._api.getData('dropdown/alat-pendukung/'+event).subscribe((data: any)=> {
			this.dropdown['alat_pendukung'] = data;
			this.dropdown['alat_pendukung'].unshift({label:'-Pilih-', value:null, disabled:true});
		});

		this.dropdown['product'] = this.dropdown_temp.product.filter( x => x['id_ro'] === event);
		this.dropdown['product'].unshift({label:'-Pilih-', value:null, disabled:true});
		this._spinner.hide();
	}
  
  	select_product(event){
		var tes = this.dropdown['product'].filter( x => x['value'] === event);
		this.selected_box = tes[0].label;
		this._api.getData('dropdown/penggunaan-barang/'+event).subscribe((data: any)=> {
			this.dropdown['product_detail'] = data.list;
			this.dropdown['product_detail'].unshift({label:'-Pilih-', value:null,disabled:true});
		});
  	}

  	select_product_detail(event){
		var tes = this.dropdown['product_detail'].filter( x => x['value'] === event.value);
		this.selected_product_detail = tes[0].label;
		this.selected_product_code = tes[0].code;
	}

	select_product_detail_pendukung(event){
		var tes = this.dropdown['alat_pendukung'].filter( x => x['value'] === event.value);
		this.selected_product_detail_pendukung = tes[0].label;
		this.selected_product_code_pendukung = tes[0].code;
	}

	onSubmit(){
		if(confirm("Apakah anda yakin Akan Mengupdate Penggunaan barang? bila table kosong maka akan dianggap tidak ada penggunaan barang")) {
			this.store_data.id_ro = this.add_form.value.id_ro;
			if(this.store_data.id_ro != null){
				this.store_data.data = this.store_temp1;
				this.store_data.id_user = JSON.parse(localStorage.getItem('currentUser')).data.id;

				this._api.insertData('penggunaan-barang/add', this.store_data).subscribe((data: any) => {
					this._ts.success('Success', data.message);
					this.getMainData();
					this.getDropDown();
					this.modalCreate.dismiss();
				},(error: any) => {
					
				},() => {
					this._spinner.hide();
				});
			}else{
				alert("silahkan pilih nomor Rencana Operasi terlebih dahulu");
			}
		}
	}

	onSubmitEdit(){
		this.edit_form.value.id_ro = this.id_rowselect;
		this.edit_form.value.id_user = JSON.parse(localStorage.getItem('currentUser')).data.id;
		
		if(this.edit_form.value.quantity > 0){
			

			if(confirm("Apakah anda yakin Akan Mengupdate Penggunaan barang? Action ini akan langsung mengurangi stok dalam box, apabila dilakukan setelah proses syncronize")) {
				this._api.insertData('penggunaan-barang/penambahan_barang_baru', this.edit_form.value).subscribe((data: any) => {
					this._ts.success('Success', data.message);
					this.getMainData();

					this._api.getData('penggunaan-barang/detail/'+this.id_rowselect).subscribe((data: any)=> {
						this.detaildata = data;
					});
				});	
			}
		}else{
			alert("Harap masukan quantity");
		}
	}

	syncronize(event){
		event.id_user = JSON.parse(localStorage.getItem('currentUser')).data.id;
		if(confirm("Apakah anda yakin Akan melakukan Syncronize? Stok Box akan disesuaikan dalam proses ini, harap mengecek kembali penggunaan barang")) {
			this._api.updateData('penggunaan-barang/syncronize', event).subscribe((data: any) => {
				this._ts.success('Success', data.message);
				this.getMainData();
				// this._api.getData('penggunaan-barang/detail/'+this.id_rowselect).subscribe((data: any)=> {
				// 	this.detaildata = data;
				// });
			});
		}
	}

	onSubmitUpdate(){
		this._api.updateData('penggunaan-barang/edit_ro', this.update_ro_form.value).subscribe((data: any) => {
			this._ts.success('Success', data.message);
		});
	}

	filter_fix(param){
		if (param == 'all') {
			this.maindata = this.temp_maindata;
		}else if (param == 'sync'){
			this.maindata = this.temp_maindata.filter( x => x['integrated'] == 1);
		}else if (param == 'unsync'){
			this.maindata = this.temp_maindata.filter( x => x['integrated'] == 0);
		}
	}

}
