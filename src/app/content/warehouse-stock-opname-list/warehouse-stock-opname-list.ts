import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { BsModalComponent  } from 'ng2-bs3-modal';
import { NgxSpinnerService } from 'ngx-spinner';
import { Api } from '../../service/api';
import { Router, Routes, ActivatedRoute, Params } from '@angular/router';

@Component({
  selector: 'app-warehouse-stock-opname-list',
  templateUrl: './warehouse-stock-opname-list.html',
  styleUrls: ['./warehouse-stock-opname-list.css']
})
export class WarehouseStockOpnameListComponent implements OnInit {
	public title_table 			: string = 'Tabel Stock Opname List';

	public privilege_list		: any = JSON.parse(localStorage.getItem('currentUser')).data.privilege_list;

	public maindata				: any;
	public cols					: any[];
	public add_form				: FormGroup;
	public edit_form			: FormGroup;
	public id_rowselect 		: number;

	//dropdown
	public user_level			: any;

	// effect animate
	public animate_effect		: string = '';

	//flag
	public button_export		: boolean = false;

	@ViewChild('create')	public modalCreate	: BsModalComponent;
	@ViewChild('edit')		public modalEdit	: BsModalComponent;

	constructor(
		private _api		: Api,
		private _fb			: FormBuilder,
		private _ts			: ToastrService,
		private _spinner	: NgxSpinnerService,
		public _router		: Router
	) {

		this.add_form = _fb.group({
			'name'					: [null, Validators.required],
		});

		this.edit_form = _fb.group({
			'name'					: [null, Validators.required],
		});
	}

	/* -- Lifecycle Hooks Code -- */
	ngOnInit() {
		this.getMainData();
	}

	/* -- Function Code -- */
	OpenModal() {
		this.modalCreate.open();
	}

	handleRowSelect(event: any){
		this.id_rowselect = event.id;
		this.edit_form.get('name').setValue(event.name);
		this.modalEdit.open();
	}

	getMainData(){
		// setup column P-table
		this.cols = [
			{ field: 'no_so', header: 'Nomor SO' },
			{ field: 'created_by_name', header: 'Dibuat Oleh' },
			{ field: 'created_date', header: 'Start' },
			{ field: 'finish_date', header: 'Selesai' },
			{ field: 'status', header: 'Status' },
		];

		// get data from API
		this._api.getData('stock_opname/list_so').subscribe((data: any)=> {
			this.maindata = data;
		},(error: any) => {
				
		},() => {
			this._spinner.hide();
		});
	}

	viewResult(event){
		this._router.navigate(['warehouse-stock-opname-result',event.id]);
	}

	onSubmit(){
		this._api.insertData('diagnosa-case/add', this.add_form.value).subscribe((data: any) => {
			this._ts.success('Success', data.message);
			this.getMainData();
			this.modalCreate.dismiss();
		},(error: any) => {
				
		},() => {
			this._spinner.hide();
		});

		this.animate_effect = 'swing';
	}

	onSubmitEdit(){
		// get dulu id record nya
		this.edit_form.value.id = this.id_rowselect;
		
		this._api.updateData('diagnosa-case/edit', this.edit_form.value).subscribe((data: any) => {
			this._ts.success('Success', data.message);
			this.getMainData();
			this.modalEdit.dismiss();
		},(error: any) => {
				
		},() => {
			this._spinner.hide();
		});
	}

	integration(event: any){
		if(confirm("Apakah anda yakin ingin Menyesuaikan dengan stok gudang? Stok Gudang akan otomatis Disesuaikan dalam proses Integrasi")) {
			
			console.log(event);
			
			this._api.insertData('stock_opname/integrate', event).subscribe((data: any) => {
				this._ts.success('Success', data.message);
				this.getMainData();
				this.modalCreate.dismiss();
			},(error: any) => {
				
			},() => {
				this._spinner.hide();
			});
		}
	}

}
