import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { BsModalComponent } from 'ng2-bs3-modal';
import { NgxSpinnerService } from 'ngx-spinner';
import { DomSanitizer } from '@angular/platform-browser';
import { Api } from '../../service/api';
import * as jsPDF from 'jspdf';
import html2canvas from 'html2canvas';
import { environment } from '../../../environments/environment';

class Data_product {
	ro: any;
	id_product: any;
}

class count_data {
	fix: any;
	reschedule: any;
	canceled: any;
	onprogress: any;
	fix_percentage: any;
	reschedule_percentage: any;
	canceled_percentage: any;
}

class Data_pendukung {
	ro: any;
	id_product_detail: any;
	quantity: any;
}

class Add_data_RO {
	ro: any;
	product: any;
	alat_pendukung: any;
}

@Component({
	selector: 'app-rencana-operasi',
	templateUrl: './rencana-operasi.html',
	styleUrls: ['./rencana-operasi.css']
})
export class RencanaOperasiComponent implements OnInit {

	public title_table: string = 'Tabel Rencana Operasi';

	public privilege_list		: any = JSON.parse(localStorage.getItem('currentUser')).data.privilege_list;
	public user_division		: any = JSON.parse(localStorage.getItem('currentUser')).data.division;
	public user_privilege		: any = JSON.parse(localStorage.getItem('currentUser')).data.privilege;

	// Class
	public assign_product = new Data_product();
	public assign_alat_pendukung = new Data_pendukung();
	public add_ro = new Add_data_RO();

	// data , table dan kolom
	public maindata: any;
	public count_maindata = new count_data();
	public temp_maindata: any;
	public data_product_list: any;
	public data_alat_pendukung_list: any;
	public data_comment: any;
	public data_log: any;
	public cols: any[];

	// data temp add product
	//public store_data			= new Wh_store_data();
	public store_temp1			= new Array;
	public store_temp2			= new Array;

	// Form
	public add_form: FormGroup;
	public edit_form: FormGroup;
	public comment_form: FormGroup;
	public filter_form			: FormGroup;
	
	// utilities
	public id_rowselect: number;
	public progress_bar: string;

	//dropdown
	public dropdown_temp: any;
	public dropdown: any;
	public ts_list: any;

	//button print
	public show_button_print: boolean = true;
	public tab_box			: boolean = false;
	public tab_ts			: boolean = false;

	// loading
	loading: boolean;

	public user_id = JSON.parse(localStorage.getItem('currentUser')).data.id;

	today = Date();

	// data detail
	datadetail: any;

	public API_URL = 'http://'+environment.API_URL;

	@ViewChild('create') public modalCreate: BsModalComponent;
	@ViewChild('edit') public modalEdit: BsModalComponent;
	@ViewChild('projectview') public modalProjectview: BsModalComponent;
	@ViewChild('printview') public modalPDFview: BsModalComponent;

	constructor(
		private _api: Api,
		private _fb: FormBuilder,
		private _ts: ToastrService,
		private _spinner: NgxSpinnerService,
		private _sanitize: DomSanitizer,
	) {

		const validator = _fb.group({
			'id_user': [null],
			'date': [null, Validators.required],
			'doctor': [null, Validators.required],
			'hospital': [null, Validators.required],
			'pasien': [null],
			'spine_anatomi': [null, Validators.required],
			'subcase': [null, Validators.required],
			'case': [null, Validators.required],
			'remarks': [null],
			'jenis_tagihan': [null, Validators.required],
			'request_instrument': [null],
			'alat_pendukung': [null],
			'quantity_alat_pendukung': [null],
			'no_mr': [null],
			'ts': [null],
			'status_box': [null],
			'status_ro': [null],
			'type_ro': [null],
			'stage': [null],
		});

		const comment = _fb.group({
			'id_user': [''],
			'message': ['', Validators.required],
		});

		const filter = _fb.group({
			'date': [''],
			'month': [''],
			'year': [''],
		});

		this.add_form = validator;
		this.edit_form = validator;
		this.comment_form = comment;

		this.filter_form 	= filter;

	}

	/* -- Lifecycle Hooks Code -- */
	ngOnInit() {

		if(this.user_division == 1 || this.user_division == 3){
			this.tab_box = true;
		}

		if(this.user_division == 1 || this.user_division == 3 || this.user_division == 6){
			this.tab_ts = true;
		}

		this.getData();
		this.getDataDropdown();
	}

	// GET FUNCTION
	getData() {
		this.loading = true;
		this._api.getData('rencana-operasi/all').subscribe((data: any) => {
			this.maindata = data;
			this.temp_maindata = data;
			// count
			this.count_maindata.fix = data.filter( x => x['status_ro'] == 1).length;
			this.count_maindata.reschedule = data.filter( x => x['status_ro'] == 2).length;
			this.count_maindata.canceled = data.filter( x => x['status_ro'] == 3).length;
			
			var tgl = new Date().getDate();
			var filterTgl : any;
			if(tgl < 10){filterTgl = '0'+tgl;}else{filterTgl = tgl;}
			
			var month = new Date().getMonth() + 1;
			var filterMonth : any;
			if (month < 10) {filterMonth = "0" + month;} else {filterMonth = month;}
			
			var filterYear = new Date().getFullYear();
			
			var today = filterTgl+'-'+filterMonth+'-'+filterYear;
			this.count_maindata.onprogress = data.filter( x => (x['stage'] >= 5 && x['stage'] < 9 && x['status_ro'] == 1)).filter(x => x['datenow'] == today).length;

			// percentage
			this.count_maindata.fix_percentage = (((data.filter( x => x['status_ro'] == 1).length) / data.length) * 100).toFixed(2);
			this.count_maindata.reschedule_percentage =(((data.filter( x => x['status_ro'] == 2).length) / data.length) * 100).toFixed(2);
			this.count_maindata.canceled_percentage = (((data.filter( x => x['status_ro'] == 3).length) / data.length) * 100).toFixed(2);

			this.loading = false;
		});
	}

	get_product_list(id_ro) {
		this._api.getData('rencana-operasi/product_list/' + id_ro).subscribe((data: any) => {
			this.data_product_list = data;
		});
	}

	get_alat_pendukung_list(id_ro) {
		this._api.getData('rencana-operasi/alat_pendukung_list/' + id_ro).subscribe((data: any) => {
			this.data_alat_pendukung_list = data;
			//console.log(this.data_alat_pendukung_list);
		});
	}

	getDataComment(id: number) {
		this._api.getData('rencana-operasi/comment/' + id).subscribe((data: any) => {
			this.data_comment = data.comment;
			this.data_log = data.log;
		});
	}

	getDataDropdown() {
		this.dropdown = [];
		this._api.getData('rencana-operasi/dropdown').subscribe((data: any) => {
			this.dropdown_temp = data;
			this.dropdown['user'] = data.user;
			this.dropdown['hospital'] = data.hospital;
			this.dropdown['doctor'] = data.doctor;
			this.dropdown['spine_anatomi'] = data.spine_anatomi;
			this.dropdown['case'] = data.case;
			this.dropdown['subcase'] = data.subcase;
			this.dropdown['jenis_tagihan'] = data.jenis_tagihan;
			this.dropdown['product'] = data.product;
			this.dropdown['product'] = data.product;
			this.dropdown['ts_list'] = data.ts_list;
			this.dropdown['status_ro'] = data.status_ro;
			this.dropdown['type_ro'] = data.ro_type;
			this.dropdown['alat_pendukung'] = data.alat_pendukung;
			this.dropdown['stage'] = data.stage;

			this.dropdown['product'].unshift({ label: '-Pilih-', value: null });
			this.dropdown['user'].unshift({ label: '-Pilih-', value: null });
			this.dropdown['hospital'].unshift({ label: '-Pilih-', value: null });
			this.dropdown['doctor'].unshift({ label: '-Pilih-', value: null });
			this.dropdown['spine_anatomi'].unshift({ label: '-Pilih-', value: null });
			this.dropdown['case'].unshift({ label: '-Pilih-', value: null });
			this.dropdown['subcase'].unshift({ label: '-Pilih-', value: null });
			this.dropdown['jenis_tagihan'].unshift({ label: '-Pilih-', value: null });
			this.dropdown['ts_list'].unshift({ label: '-Non TS-', value: null });
			this.dropdown['status_ro'].unshift({ label: '-Pilih-', value: null });
			this.dropdown['type_ro'].unshift({ label: '-Pilih-', value: null });
			this.dropdown['alat_pendukung'].unshift({ label: '-Pilih-', value: null });
			this.dropdown['stage'].unshift({ label: '-No Update-', value: null });

			this.dropdown['date'] = [
				{value:1,label:'1'},
				{value:2,label:'2'},
				{value:3,label:'3'},
				{value:4,label:'4'},
				{value:5,label:'5'},
				{value:6,label:'6'},
				{value:7,label:'7'},
				{value:8,label:'8'},
				{value:9,label:'9'},
				{value:10,label:'10'},
				{value:11,label:'11'},
				{value:12,label:'12'},
				{value:13,label:'13'},
				{value:14,label:'14'},
				{value:15,label:'15'},
				{value:16,label:'16'},
				{value:17,label:'17'},
				{value:18,label:'18'},
				{value:19,label:'19'},
				{value:20,label:'20'},
				{value:21,label:'21'},
				{value:22,label:'22'},
				{value:23,label:'23'},
				{value:24,label:'24'},
				{value:25,label:'25'},
				{value:26,label:'26'},
				{value:27,label:'27'},
				{value:28,label:'28'},
				{value:29,label:'29'},
				{value:30,label:'30'},
				{value:31,label:'31'},
			];

			this.dropdown['month'] = [
				{value:1,label:'Jan'},
				{value:2,label:'Feb'},
				{value:3,label:'Mar'},
				{value:4,label:'Apr'},
				{value:5,label:'Mei'},
				{value:6,label:'Jun'},
				{value:7,label:'Jul'},
				{value:8,label:'Aug'},
				{value:9,label:'Sep'},
				{value:10,label:'Oct'},
				{value:11,label:'Nov'},
				{value:12,label:'Des'},
			];

			this.dropdown['year'] = [
				{value:2019,label:'2019'},
			];

			this.dropdown['date'].unshift({ label: '-all-', value: null });
			this.dropdown['month'].unshift({ label: '-all-', value: null });
			this.dropdown['year'].unshift({ label: '-all-', value: null });
		});
	}

	// DISPLAY FUNCTION
	OpenModal() {
		this.add_form.reset();
		this.modalCreate.open('lg');
	}

	onSubmitFilter(){
		this.loading = true;
		this._api.insertData('rencana-operasi/filterdate',this.filter_form.value).subscribe((data: any) => {
			this.maindata = data;
			this.temp_maindata = data;
			// count
			this.count_maindata.fix = data.filter( x => x['status_ro'] == 1).length;
			this.count_maindata.reschedule = data.filter( x => x['status_ro'] == 2).length;
			this.count_maindata.canceled = data.filter( x => x['status_ro'] == 3).length;
			
			var tgl = new Date().getDate();
			var filterTgl : any;
			if(tgl < 10){filterTgl = '0'+tgl;}else{filterTgl = tgl;}
			
			var month = new Date().getMonth() + 1;
			var filterMonth : any;
			if (month < 10) {filterMonth = "0" + month;} else {filterMonth = month;}
			
			var filterYear = new Date().getFullYear();
			
			var today = filterTgl+'-'+filterMonth+'-'+filterYear;
			this.count_maindata.onprogress = data.filter( x => (x['stage'] >= 5 && x['stage'] < 9 && x['status_ro'] == 1)).filter(x => x['datenow'] == today).length;

			// percentage
			this.count_maindata.fix_percentage = (((data.filter( x => x['status_ro'] == 1).length) / data.length) * 100).toFixed(2);
			this.count_maindata.reschedule_percentage =(((data.filter( x => x['status_ro'] == 2).length) / data.length) * 100).toFixed(2);
			this.count_maindata.canceled_percentage = (((data.filter( x => x['status_ro'] == 3).length) / data.length) * 100).toFixed(2);

			this.loading = false;
		});
	}

	handleRowSelect(event: any) {
		this.id_rowselect = event.id;
		this.get_product_list(this.id_rowselect);
		this.get_alat_pendukung_list(this.id_rowselect);
		this.edit_form.get('date').setValue(new Date(event.date));
		this.edit_form.get('type_ro').setValue(event.type_ro);
		this.edit_form.get('case').setValue(event.case);
		this.edit_form.get('subcase').setValue(event.subcase);
		this.edit_form.get('doctor').setValue(event.id_doctor);
		this.edit_form.get('hospital').setValue(event.id_hospital);
		this.edit_form.get('jenis_tagihan').setValue(event.jenis_tagihan);
		this.edit_form.get('no_mr').setValue(event.no_mr);
		this.edit_form.get('pasien').setValue(event.pasien);
		this.edit_form.get('spine_anatomi').setValue(event.id_spine_anatomi);
		this.edit_form.get('remarks').setValue(event.remarks);
		this.edit_form.get('status_box').setValue(event.status_box);
		this.edit_form.get('status_ro').setValue(event.status_ro);
		this.edit_form.get('ts').setValue(event.technical_support);
		this.modalEdit.open('lg');
	}

	tambah_list() {
		this.assign_product.ro = this.id_rowselect;
		this.assign_product.id_product = this.edit_form.value.request_instrument;
		this._api.insertData('rencana-operasi/add_product_list', this.assign_product).subscribe((data: any) => {
			this._ts.success('Success', data.message);
			this.get_product_list(this.id_rowselect);
		});
	}

	edit_list_alat_pendukung() {
		this.assign_alat_pendukung.ro = this.id_rowselect;
		this.assign_alat_pendukung.id_product_detail = this.edit_form.value.alat_pendukung;
		this.assign_alat_pendukung.quantity = this.edit_form.value.quantity_alat_pendukung;
		if(this.assign_alat_pendukung.quantity != null || this.assign_alat_pendukung.quantity > 1){
			this._api.insertData('rencana-operasi/add_alat_pendukung', this.assign_alat_pendukung).subscribe((data: any) => {
				this._ts.success('Success', data.message);
				this.get_alat_pendukung_list(this.id_rowselect);
			});
		}else{
			alert("Harap masukan quantity");
		}
	}

	tambah_list_add(){
		this.add_form.value.request_instrument;
		this._api.getData('product/detail/'+this.add_form.value.request_instrument).subscribe((data: any)=> {
			var valueToPush = { };
			valueToPush["product"] 				= data.id_principle;
			valueToPush["product_name"] 		= data.principle_name;
			valueToPush["product_detail"] 		= data.id;
			valueToPush["product_detail_name"] 	= data.name;

			// prevent duplicate row with same id
			var store = true;
			for( var i = 0, len = this.store_temp1.length; i < len; i++ ) {
				if( this.store_temp1[i]['product_detail'] == this.add_form.value.request_instrument ) {
					store = false;
				}
			}
			if (store == true) {
				this.store_temp1.push(valueToPush);
			}
		});
	}

	add_alat_pendukung(){
		if(this.add_form.value.quantity_alat_pendukung != null || this.add_form.value.quantity_alat_pendukung > 1){
			
			this._api.getData('rencana-operasi/product_detail_list/'+this.add_form.value.alat_pendukung).subscribe((data: any)=> {
				var valueToPush = { };
				valueToPush["quantity"] 			= this.add_form.value.quantity_alat_pendukung;
				valueToPush["product_detail"] 		= data.id;
				valueToPush["product_detail_name"] 	= data.name;

				// prevent duplicate row with same id
				var store = true;
				for( var i = 0, len = this.store_temp2.length; i < len; i++ ) {
					if( this.store_temp2[i]['product_detail'] == this.add_form.value.alat_pendukung ) {
						store = false;
					}
				}
				if (store == true) {
					this.store_temp2.push(valueToPush);
				}
			});
		}else{
			alert("Harap masukan quantity");
		}
	}

	removeList(selected_data:any){		
		let data = this.store_temp1.filter( x => x['product_detail'] != selected_data.product_detail);
		this.store_temp1 = data;
	}
	removeList2(selected_data:any){
		let data = this.store_temp2.filter( x => x['product_detail'] != selected_data.product_detail);
		this.store_temp2 = data;
	}

	removeListEdit(selected_data:any){
		
		if(confirm("Apakah anda yakin ingin menghapus data?")) {
			this._api.deleteData('rencana-operasi/detail/'+selected_data.id).subscribe((data: any) => {
				this._ts.success('Success', data.message);
				this.get_product_list(this.id_rowselect);
			});
		}
	}

	removeList_Alat_pendukung(selected_data:any){
		
		if(confirm("Apakah anda yakin ingin menghapus data?")) {
			this._api.deleteData('rencana-operasi/alat_pendukung/'+selected_data.id).subscribe((data: any) => {
				this._ts.success('Success', data.message);
				this.get_alat_pendukung_list(this.id_rowselect);
			});
		}
	}

	onChangeDiagnosaCase(event) {
		this.dropdown['subcase'] = this.dropdown_temp.subcase.filter(x => x['id_case'] === event.value);
		this.dropdown['subcase'].unshift({ label: '-Pilih-', value: null });
	}

	// POST FUNCTION
	onSubmit() {
		this.add_form.value.id_user = JSON.parse(localStorage.getItem('currentUser')).data.id;
		this.add_ro.ro = this.add_form.value;
		this.add_ro.product = this.store_temp1;
		this.add_ro.alat_pendukung = this.store_temp2;
		
		if(this.add_ro.product.length == 0 && this.add_ro.alat_pendukung == 0){
			alert("Harap masukan Product yang akan digunakan dalam Rencana Operasi ini");
		}else{	
			this._spinner.show();
			this._api.insertData('rencana-operasi/add', this.add_ro).subscribe((data: any) => {
				this._ts.success('Success', data.message);
				this.getData();
				this.modalCreate.dismiss();
				this.add_form.reset();
				this._spinner.hide();
			});
		}
	}

	onSubmitEdit() {
		this._spinner.show();
		var id_user = JSON.parse(localStorage.getItem('currentUser')).data.id;
		this.edit_form.value.id_user = id_user;
		this.edit_form.value.id = this.id_rowselect;
		this._api.updateData('rencana-operasi/edit', this.edit_form.value).subscribe((data: any) => {
			this._ts.success('Success', data.message);
			this.getData();
			this.modalEdit.dismiss();
			this._spinner.hide();
		});
	}

	onSubmitComment(id: number) {
		this.comment_form.value.id = id;
		this.comment_form.value.id_user = JSON.parse(localStorage.currentUser).data.id;
		this._api.insertData('rencana-operasi/comment', this.comment_form.value).subscribe((data: any) => {
			this._ts.success('Success', data.message);
			this.getDataComment(id);
			this.comment_form.reset();
		});
	}


	// INTERNAL FUNCTION
	show_nama_product(id) {
		this._api.getData('rencana-operasi/product_list/' + id).subscribe((data: any) => {
			var namelist = [];
			if (data.length == 1) {
				this.datadetail['request_instrument_name'] = data[0].product_name;
			} else if (data.length > 1) {
				for (let i = 0; i < data.length; i++) {
					namelist.push(data[i]['product_name']);
				}
				this.datadetail['request_instrument_name'] = namelist.join(', ');
			}
		});
	}

	showImage(img: string) {
		return this._sanitize.bypassSecurityTrustUrl(img);
	}

	downloadPDF() {
		this.show_button_print = false;
		var data = document.getElementById('cobatesyangini');
		html2canvas(data).then(canvas => {
			var imgWidth = 208;
			var pageHeight = 295;
			var imgHeight = canvas.height * imgWidth / canvas.width;
			var heightLeft = imgHeight;

			const contentDataURL = canvas.toDataURL('image/png')
			let pdf = new jsPDF('p', 'mm', 'a4'); // A4 size page of PDF
			var position = 0;
			pdf.addImage(contentDataURL, 'PNG', 0, position, imgWidth, imgHeight)
			pdf.save('MYPdf.pdf'); // Generated PDF
		});
	}

	viewPDF(id) {
		//window.open(this.API_URL+'/print/rencana_operasi/'+id,'_blank');
		// this.datadetail = [];
		// this._api.getData('rencana-operasi/list/' + id).subscribe((data: any) => {
		// 	this.datadetail = data;
		// 	this.show_nama_product(id);
		// 	this.modalPDFview.open('lg');
		// });
	}

	viewProject(id: number) {
		this.datadetail = [];
		this._api.getData('rencana-operasi/list/' + id).subscribe((data: any) => {
			this.datadetail = data;
			this.progress_bar = data.progress;
			this.show_nama_product(id);
			this.modalProjectview.open('lg');
			this.getDataComment(id);
		});
	}

	export_ro(){
		var obj = JSON.stringify(this.filter_form.value);
		if(obj){
			window.open(this.API_URL+'/print/ro_range/'+obj,'_blank');
		}
	}

	filter_fix(param){
		if (param == 'fix') {
			this.maindata = this.temp_maindata.filter( x => x['status_ro'] == 1);
		}else if (param == 'reschedule'){
			this.maindata = this.temp_maindata.filter( x => x['status_ro'] == 2);
		}else if (param == 'canceled'){
			this.maindata = this.temp_maindata.filter( x => x['status_ro'] == 3);
		}else if (param == 'progress'){
			var tgl = new Date().getDate();
			var filterTgl : any;
			if(tgl < 10){filterTgl = '0'+tgl;}else{filterTgl = tgl;}
			
			var month = new Date().getMonth() + 1;
			var filterMonth : any;
			if (month < 10) {filterMonth = "0" + month;} else {filterMonth = month;}
			
			var filterYear = new Date().getFullYear();
			
			var today = filterTgl+'-'+filterMonth+'-'+filterYear;
			this.maindata = this.temp_maindata.filter( x => (x['stage'] >= 5 && x['stage'] < 9 && x['status_ro'] == 1)).filter(x => x['datenow'] == today);
		}else if (param == 'done'){
			this.maindata = this.temp_maindata.filter( x => x['stage'] == 9 );
		}
	}
}
