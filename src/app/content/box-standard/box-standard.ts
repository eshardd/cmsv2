import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { BsModalComponent  } from 'ng2-bs3-modal';
import { NgxSpinnerService } from 'ngx-spinner';
import { Api } from '../../service/api';

@Component({
  selector: 'app-box-standard',
  templateUrl: './box-standard.html',
  styleUrls: ['./box-standard.css']
})
export class BoxStandardComponent implements OnInit {
	public title_table 			: string = 'Tabel Product';

	public privilege_list		: any = JSON.parse(localStorage.getItem('currentUser')).data.privilege_list;

	public maindata_implant		: any;
	public maindata_instrument	: any;
	public product				: any;
	public temp_product				: any;
	public selectedProduct		: any;
	public cols					: any[];
	public id_rowselect 		: number;
	public label_selected		: number;

	public add_form				: FormGroup;
	public edit_form			: FormGroup;

	// dropdown
	public principle_list		:any;
	public dropdown				:any;
	public dropdownfilter		:any;

	//flag
	public button_export		: boolean = false;
	public splitshow			: boolean = false;

	@ViewChild('add_instrument')	public modalAddInstrument	: BsModalComponent;
	@ViewChild('add_implant')		public modalAddImplant	: BsModalComponent;
	@ViewChild('edit_data')			public modalEdit	: BsModalComponent;

	constructor(
		private _api		: Api,
		private _fb			: FormBuilder,
		private _ts			: ToastrService,
		private _spinner	: NgxSpinnerService
	) {
		this.add_form = _fb.group({
			'product_detail'		: [null, Validators.required],
			'quantity'				: [null, Validators.required],
			'quantity_skoliosis'	: [null, Validators.required]
		});

		this.edit_form = _fb.group({
			'type'					: [null],
			'product_detail'		: [null, Validators.required],
			'quantity'				: [null, Validators.required],
			'quantity_skoliosis'	: [null, Validators.required]
		});
	}

	/* -- Lifecycle Hooks Code -- */
	ngOnInit() {

		this.cols = [
			{ field: 'product_detail_name', header: 'Item' },
			{ field: 'quantity', header: 'Std' },
			{ field: 'quantity_skoliosis', header: 'Sco' }
		];

		this.getDataProduct();
		this.getDataDropdownFilter();
	}

	select_product(param){
		
		this.selectedProduct = param.value;
		this.label_selected = param.label;
		this.getItemList(param.value);
		this.splitshow = true;
	}

	filter_product(param){
		this.splitshow = false;
		this.product = this.temp_product.filter( x => x['principle_id'] === param);
	}

	/* -- Function Code -- */
	OpenModalAddInstrument() {
		this.modalAddInstrument.open();
	}

	OpenModalAddImplant() {
		this.modalAddImplant.open();
	}

	getDataProduct(){

		// get data from API
		this._api.getData('box/all_product').subscribe((data: any)=> {
			//this.maindata = data;
			this.product = data.product;
			this.temp_product = data.product;
			this.product.unshift({label:'-Pilih product-', value:null});
		});
	}

	getItemList(id_product:number){
		this._api.getData('box/standard/'+id_product).subscribe((data: any)=> {
			this.maindata_implant = data.filter( x => x['type'] === '1');
			this.maindata_instrument = data.filter( x => x['type'] === '2');
		});

		this.getDataDropdown(id_product);
	}

	getDataDropdown(id_product:number){
		// dropdown user level
		this.dropdown = [];
		this._api.getData('box/product/'+id_product).subscribe((data: any)=> {
			this.dropdown['implant'] = data.filter( x => x['type'] === '1');
			this.dropdown['instrument'] = data.filter( x => x['type'] === '2');
			this.dropdown['implant'].unshift({label:'-Pilih Implant-', value:null});
			this.dropdown['instrument'].unshift({label:'-Pilih Instrument-', value:null});
		});
	}

	getDataDropdownFilter(){
		// dropdown user level
		this.dropdownfilter = [];
		this._api.getData('box/dropdown_filter').subscribe((data: any)=> {
			this.dropdownfilter['principle'] = data.principle;
			//this.dropdownfilter['principle'].unshift({label:'-Pilih Principle-', value:null});
			console.log(this.dropdownfilter);
		});
	}

	rowSelect(event: any){
		this.id_rowselect = event.id;
		this.edit_form.get('product_detail').setValue(event.product_detail);
		this.edit_form.get('quantity').setValue(event.quantity);
		this.edit_form.get('quantity_skoliosis').setValue(event.quantity_skoliosis);
		this.edit_form.get('type').setValue(event.type);

		if(event.type == 1){
			this.dropdown['editlist'] = this.dropdown['implant'];
		}else{
			this.dropdown['editlist'] = this.dropdown['instrument'];
		}

		console.log(this.edit_form.value);
		this.modalEdit.open();
	}

	submit_instrument(){
		this.add_form.value.product = this.selectedProduct;
		this._api.insertData('box/add_instrument', this.add_form.value).subscribe((data: any) => {
			this._ts.success('Success', data.message);
			this.getItemList(this.selectedProduct);
		},(error: any) => {
			this._ts.error('Error','Duplicate Data');
		},() => {
			
		});
	}

	submit_implant(){
		
		this.add_form.value.product = this.selectedProduct;
		this._api.insertData('box/add_implant', this.add_form.value).subscribe((data: any) => {
			this._ts.success('Success', data.message);
			this.getItemList(this.selectedProduct);
		},(error: any) => {
			this._ts.error('Error','Duplicate Data');
		},() => {
			this.getItemList(this.selectedProduct);
		});
	}

	submit_edit(){
		this.edit_form.value.id = this.id_rowselect;
		this.edit_form.value.product = this.selectedProduct;
		this._api.updateData('box/edit_standard', this.edit_form.value).subscribe((data: any) => {
			this._ts.success('Success', data.message);
			this.modalEdit.dismiss();

			this.getItemList(this.selectedProduct);
		});
	}

	delete(param:any){
		if(confirm("Apakah anda yakin ingin menghapus data?")) {
			this._api.deleteData('box/standard/'+param).subscribe((data: any) => {
				this._ts.success('Success', data.message);
			});
		}
	}
}