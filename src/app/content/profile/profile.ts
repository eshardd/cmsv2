import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { DomSanitizer } from '@angular/platform-browser';
import { NgxSpinnerService } from 'ngx-spinner';
import { Api } from '../../service/api';
import { environment } from '../../../environments/environment';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.html',
  styleUrls: ['./profile.css']
})
export class ProfileComponent implements OnInit {

	public data_form  : FormGroup;
	public password_form  : FormGroup;
	public selectedFile: File;
	public imgUrl;
	public API_URL = 'http://'+environment.API_URL;
	public fileToUpload: any;
	public user_id = JSON.parse(localStorage.getItem('currentUser')).data.id;
	public datauser : any;
	show_loading : boolean = false;

	showSpinner: boolean = false;

  	constructor(
	  	private _fb			: FormBuilder,
		private _api		: Api,
		private _ts			: ToastrService,
		private _sanitize	: DomSanitizer,
		private _spinner	: NgxSpinnerService
	) { 
  		this.data_form = _fb.group({
			'id'   		: [null],
			'name'   	: [null, Validators.required],
			'email'   	: [null, Validators.email],
			'username'  : [null, Validators.required],
			'image'  	: [''],
			'address'  	: [null,Validators.required],
			'aboutme'   : [null],
		});
	
  		this.password_form = _fb.group({
			'id'   				: [null],
			'password' 			: [null, Validators.required],
			'confirmation'   	: [null, Validators.required]
		});
	}

	ngOnInit() {
		this.userProfile();
	}

	userProfile(){
		this.datauser = [];
		this._api.getData('profile/user/'+this.user_id).subscribe((data: any)=> {
			this.datauser['name'] = data.name;
			this.datauser['email'] = data.image;
			this.datauser['username'] = data.username;
			this.datauser['password'] = data.password;
			this.datauser['image'] = data.image;
			this.datauser['aboutme'] = data.aboutme;
			this.datauser['address'] = data.address;
			
			this.data_form.get('name').setValue(data.name);
			this.data_form.get('email').setValue(data.email);
			this.data_form.get('username').setValue(data.username);
			this.data_form.get('aboutme').setValue(data.aboutme);
			this.data_form.get('address').setValue(data.address);
		});
	}

	onBasicUpload(event:any){
		this.fileToUpload = event.xhr.responseText;
	}

	showImage(img:string) {
		return this._sanitize.bypassSecurityTrustUrl(img);
	}

	data_form_submit(){;
		this._spinner.show();
		this.data_form.get('id').setValue(this.user_id);
		this.data_form.get('image').setValue(this.fileToUpload);
		this._api.updateData('profile/edit', this.data_form.value).subscribe((data: any) => {
			this._ts.success('Success', data.message);
			this.userProfile();
		},(error: any) => {
				
		},() => {
			this._spinner.hide();
		});
	}

	password_form_submit(){
		this._spinner.show();
		this.password_form.get('id').setValue(this.user_id);
		this._api.updateData('profile/edit_password', this.password_form.value).subscribe((data: any) => {
			console.log(data);
			if(data.id != null){
				this._ts.success('Success', data.message);
				this.password_form.reset()
			}else{
				this._ts.warning('Error', data.message);
				this.password_form.reset()
			}
		},(error: any) => {
			
		},() => {
			this._spinner.hide();
		});
	}
}

