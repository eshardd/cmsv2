import { Component, OnInit, Input, ViewChild, NgZone } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { BsModalComponent  } from 'ng2-bs3-modal';
import { NgxSpinnerService } from 'ngx-spinner';
import { Api } from '../../service/api';

@Component({
  selector: 'app-summary-stok',
  templateUrl: './summary-stok.html',
  styleUrls: ['./summary-stok.css']
})
export class SummaryStokComponent implements OnInit {
	public title_table 			: string = 'Summary Stok';
	
	public privilege_list		: any = JSON.parse(localStorage.getItem('currentUser')).data.privilege_list;
	
	public maindata				  : any;
	public temp_maindata		: any;
	public dropdown				  : any;
	public cols					    : any[];
	public id_rowselect 		: number;
	
	//dropdown
	public user_level			: any;
	public month_list			: any;

	public filter_form			: FormGroup;

	constructor(
		private _api		  : Api,
		private _fb			  : FormBuilder,
		private _ts			  : ToastrService,
		private _spinner	: NgxSpinnerService,
		private zone		  : NgZone
	) {
		this.filter_form = _fb.group({
			'warehouse'		: [null, Validators.required],
		});
	}
		
	/* -- Lifecycle Hooks Code -- */
	ngOnInit() {
		this.getMainData();
		this.filter_form.get('warehouse').setValue('all');
	}

	/* -- Function Code -- */
	getMainData(){
		// setup column P-table
		this.cols = [
			{ field: 'warehouse_name', header: 'WH' },
			{ field: 'principle_name', header: 'Principle' },
			{ field: 'product_name', header: 'Product' },
			{ field: 'product_detail_name', header: 'Item' },
			{ field: 'code', header: 'Code 1' },
			{ field: 'code_x', header: 'Code 2' },
			{ field: 'in', header: 'In' },
			{ field: 'out', header: 'Out' },
			{ field: 'summary_stock', header: 'Stok' },
    ];
		// get data from API
		this._api.getData('summary_stok/all/all').subscribe((data: any)=> {
			this.temp_maindata = data;
			this.maindata = data;
    });

		this._api.getData('summary_stok/dropdown').subscribe((data: any)=> {
			this.dropdown = data;
			this.dropdown.unshift({label:'-ALL-', value:'all'});
    });
	}

	onSubmitFilter(){
		// get data from API
		this._api.getData('summary_stok/all/'+this.filter_form.value.warehouse).subscribe((data: any)=> {
			this.temp_maindata = data;
			this.maindata = data;
    });
	}
}
		