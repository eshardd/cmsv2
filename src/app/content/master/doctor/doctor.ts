import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { BsModalComponent  } from 'ng2-bs3-modal';
import { NgxSpinnerService } from 'ngx-spinner';
import { Api } from '../../../service/api';

@Component({
  selector: 'app-doctor',
  templateUrl: './doctor.html',
  styleUrls: ['./doctor.css']
})
export class DoctorComponent implements OnInit {
	public title_table 			: string = 'Tabel Dokter';

	public privilege_list		: any = JSON.parse(localStorage.getItem('currentUser')).data.privilege_list;

	public maindata				: any;
	public cols					: any[];
	public add_form				: FormGroup;
	public edit_form			: FormGroup;
	public id_rowselect 		: number;

	//dropdown
	public user_level			: any;

	// effect animate
	public animate_effect		: string = '';

	//flag
	public button_export		: boolean = false;

	@ViewChild('create')	public modalCreate	: BsModalComponent;
	@ViewChild('edit')		public modalEdit	: BsModalComponent;

	constructor(
		private _api		: Api,
		private _fb			: FormBuilder,
		private _ts			: ToastrService,
		private _spinner	: NgxSpinnerService
	) {

		this.add_form = _fb.group({
			'name'					: [null, Validators.required],
			'type'					: [null, Validators.required],
		});

		this.edit_form = _fb.group({
			'name'					: [null, Validators.required],
			'type'					: [null, Validators.required]
		});
	}

	/* -- Lifecycle Hooks Code -- */
	ngOnInit() {
		this.getMainData();
	}

	/* -- Function Code -- */
	OpenModal() {
		this.modalCreate.open();
	}

	handleRowSelect(event: any){
		console.log(event);
		this.id_rowselect = event.id;

		this.edit_form.get('name').setValue(event.name);
		this.edit_form.get('type').setValue(event.type);

		this.modalEdit.open();
	}

	getMainData(){
		// setup column P-table
		this.cols = [
			{ field: 'name', header: 'Nama' },
			{ field: 'type', header: 'Tipe' }
		];

		// get data from API
		this._api.getData('doctor/all').subscribe((data: any)=> {
			this.maindata = data;
		},(error: any) => {
				
		},() => {
			this._spinner.hide();
		});
	}

	onSubmit(){
		
		this._api.insertData('doctor/add', this.add_form.value).subscribe((data: any) => {
			this._ts.success('Success', data.message);
			this.getMainData();
			this.modalCreate.dismiss();
		},(error: any) => {
				
		},() => {
			this._spinner.hide();
		});

		this.animate_effect = 'swing';
	}

	onSubmitEdit(){
		// get dulu id record nya
		this.edit_form.value.id = this.id_rowselect;
		
		this._api.updateData('doctor/edit', this.edit_form.value).subscribe((data: any) => {
			this._ts.success('Success', data.message);
			this.getMainData();
			this.modalEdit.dismiss();
		},(error: any) => {
				
		},() => {
			this._spinner.hide();
		});
	}

}
