import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { BsModalComponent  } from 'ng2-bs3-modal';
import { NgxSpinnerService } from 'ngx-spinner';
import { Api } from '../../../service/api';

@Component({
  selector: 'app-product',
  templateUrl: './product.html',
  styleUrls: ['./product.css']
})
export class ProductComponent implements OnInit {
	public title_table 			: string = 'Tabel Product';

	public privilege_list		: any = JSON.parse(localStorage.getItem('currentUser')).data.privilege_list;

	public maindata		: any;
	public cols			: any[];
	public id_rowselect : number;

	public add_form		: FormGroup;
	public edit_form	: FormGroup;

	// dropdown
	public principle_list	:any;

	//flag
	public button_export		: boolean = false;

	@ViewChild('create')	public modalCreate	: BsModalComponent;
	@ViewChild('edit')		public modalEdit	: BsModalComponent;

	constructor(
		private _api		: Api,
		private _fb			: FormBuilder,
		private _ts			: ToastrService,
		private _spinner	: NgxSpinnerService
	) {
		this.add_form = _fb.group({
			'principle'				: [null, Validators.required],
			'name'					: [null, Validators.required]
		});

		this.edit_form = _fb.group({
			'principle'				: [null, Validators.required],
			'name'					: [null, Validators.required]
		});
	}

	/* -- Lifecycle Hooks Code -- */
	ngOnInit() {
		this.getDataUser();
		this.getDataDropdown();
	}

	/* -- Function Code -- */
	OpenModal() {
		this.modalCreate.open();
	}

	getDataUser(){
		// setup column P-table
		this.cols = [
			{ field: 'principle_name', header: 'Principle' },
			{ field: 'name', header: 'Nama' }
		];

		// get data from API
		this._api.getData('product/all').subscribe((data: any)=> {
			this.maindata = data;
		});
	}

	getDataDropdown(){
		// dropdown user level
		this._api.getData('product/dropdown').subscribe((data: any)=> {
			this.principle_list = data;
			this.principle_list.unshift({label:'-Select-', value:null});
		});
	}

	handleRowSelect(event: any){
		this.id_rowselect = event.id;
		this.edit_form.get('principle').setValue(event.id_principle);
		this.edit_form.get('name').setValue(event.name);
		this.modalEdit.open();
	}

	onSubmit(){
		
		this._api.insertData('product/add', this.add_form.value).subscribe((data: any) => {
			this._ts.success('Success', data.message);
			this.getDataUser();
			this.modalCreate.dismiss();
		});
	}

	onSubmitEdit(){
		// get dulu id record nya
		this.edit_form.value.id = this.id_rowselect;
		this._api.updateData('product/edit', this.edit_form.value).subscribe((data: any) => {
			this._ts.success('Success', data.message);
			this.getDataUser();
			this.modalEdit.dismiss();
		});
	}

}