import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { BsModalComponent  } from 'ng2-bs3-modal';
import { NgxSpinnerService } from 'ngx-spinner';
import { Api } from '../../../service/api';

@Component({
  selector: 'app-product-detail',
  templateUrl: './product-detail.html',
  styleUrls: ['./product-detail.css']
})
export class ProductDetailComponent implements OnInit {
	public title_table 			: string = 'Table Product Detail';

	public privilege_list		: any = JSON.parse(localStorage.getItem('currentUser')).data.privilege_list;

	public maindata		: any;
	public cols			: any[];
	public id_rowselect : number;

	public add_form		: FormGroup;
	public edit_form	: FormGroup;

	//flag
	public button_export		: boolean = false;

	// dropdown
	public product_list	:any;
	public product_type	:any;

	@ViewChild('create')	public modalCreate	: BsModalComponent;
	@ViewChild('edit')		public modalEdit	: BsModalComponent;

	constructor(
		private _api		: Api,
		private _fb			: FormBuilder,
		private _ts			: ToastrService,
		private _spinner	: NgxSpinnerService
	) {
		this.add_form = _fb.group({
			'id_product'		: [null, Validators.required],
			'code'				: [null, Validators.required],
			'name'				: [null, Validators.required],
			'type'				: [null, Validators.required]
		});

		this.edit_form = _fb.group({
			'id_product'		: [null, Validators.required],
			'code'				: [null, Validators.required],
			'name'				: [null, Validators.required],
			'type'				: [null, Validators.required]
		});
	}

	/* -- Lifecycle Hooks Code -- */
	ngOnInit() {
		this.getDataUser();
		this.getDataDropdown();
	}

	/* -- Function Code -- */
	OpenModal() {
		this.modalCreate.open();
	}

	getDataUser(){
		// setup column P-table
		this.cols = [
			{ field: 'principle_name', header: 'Principle' },
			{ field: 'product_name', header: 'Product' },
			{ field: 'code', header: 'Code' },
			{ field: 'code_x', header: 'Code_' },
			{ field: 'name', header: 'Name' }
		];

		// get data from API
		this._api.getData('productdetail/all').subscribe((data: any)=> {
			this.maindata = data;
		},(error: any) => {
				
		},() => {
			this._spinner.hide();
		});
	}

	getDataDropdown(){
		// dropdown user level
		this._api.getData('productdetail/dropdown').subscribe((data: any)=> {
			this.product_list = data.product;
			this.product_type = data.product_type;

			this.product_list.unshift({label:'-Select-', value:null});
			this.product_type.unshift({label:'-Select-', value:null});
		},(error: any) => {
				
		},() => {
			this._spinner.hide();
		});
	}

	handleRowSelect(event: any){
		this.id_rowselect = event.id;
		this.edit_form.get('id_product').setValue(event.id_product);
		this.edit_form.get('code').setValue(event.code);
		this.edit_form.get('name').setValue(event.name);
		this.edit_form.get('type').setValue(event.type);
		this.modalEdit.open();
	}

	onSubmit(){
		
		this._api.insertData('productdetail/add', this.add_form.value).subscribe((data: any) => {
			this._ts.success('Success', data.message);
			this.getDataUser();
			this.modalCreate.dismiss();
		},(error: any) => {
				
		},() => {
			this._spinner.hide();
		});
	}

	onSubmitEdit(){
		// get dulu id record nya
		this.edit_form.value.id = this.id_rowselect;
		this._api.updateData('productdetail/edit', this.edit_form.value).subscribe((data: any) => {
			this._ts.success('Success', data.message);
			this.getDataUser();
			this.modalEdit.dismiss();
		},(error: any) => {
				
		},() => {
			this._spinner.hide();
		});
	}

}