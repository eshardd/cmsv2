import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { BsModalComponent  } from 'ng2-bs3-modal';
import { NgxSpinnerService } from 'ngx-spinner';
import { Api } from '../../../service/api';

@Component({
  selector: 'app-hospital',
  templateUrl: './hospital.html',
  styleUrls: ['./hospital.css']
})
export class HospitalComponent implements OnInit {
	public title_table 			: string = 'Tabel Rumah Sakit';

	public privilege_list		: any = JSON.parse(localStorage.getItem('currentUser')).data.privilege_list;

	public maindata				: any;
	public cols					: any[];
	public add_form				: FormGroup;
	public edit_form			: FormGroup;
	public id_rowselect 		: number;

	//dropdown
	public user_level			: any;
	public area					:any;
	public subarea				:any;

	// effect animate
	public animate_effect		: string = '';

	//flag
	public button_export		: boolean = false;

	@ViewChild('create')	public modalCreate	: BsModalComponent;
	@ViewChild('edit')		public modalEdit	: BsModalComponent;

	constructor(
		private _api		: Api,
		private _fb			: FormBuilder,
		private _ts			: ToastrService,
		private _spinner	: NgxSpinnerService
	) {

		this.add_form = _fb.group({
			'no'					: [null, Validators.required],
			'name'					: [null, Validators.required],
			'type'					: [null, Validators.required],
			'area'					: [null, Validators.required],
			'subarea'				: [null, Validators.required],
		});

		this.edit_form = _fb.group({
			'no'					: [null, Validators.required],
			'name'					: [null, Validators.required],
			'type'					: [null, Validators.required],
			'area'					: [null, Validators.required],
			'subarea'				: [null, Validators.required],
		});
	}

	/* -- Lifecycle Hooks Code -- */
	ngOnInit() {
		this.getMainData();
		this.getDataDropdown();
	}

	/* -- Function Code -- */
	OpenModal() {
		this.modalCreate.open();
	}

	handleRowSelect(event: any){
		this.id_rowselect = event.id;
		this.edit_form.get('no').setValue(event.no);
		this.edit_form.get('name').setValue(event.name);
		this.edit_form.get('type').setValue(event.type);
		this.edit_form.get('area').setValue(event.area);
		this.edit_form.get('subarea').setValue(event.subarea);

		this.modalEdit.open();
	}

	getMainData(){
		// setup column P-table
		this.cols = [
			{ field: 'no', header: 'No' },
			{ field: 'name', header: 'Name' },
			{ field: 'type', header: 'Type' },
			{ field: 'area_name', header: 'Area' },
			{ field: 'subarea_name', header: 'SubArea' }
		];

		// get data from API
		this._api.getData('hospital/all').subscribe((data: any)=> {
			this.maindata = data;
		},(error: any) => {
				
		},() => {
			this._spinner.hide();
		});
	}

	getDataDropdown(){
		// dropdown user level
		this._api.getData('hospital/dropdown').subscribe((data: any)=> {
			this.area 		= data.area;
			this.subarea 	= data.subarea;

			this.area.unshift({label:'-Select-', value:null});
			this.subarea.unshift({label:'-Select-', value:null});
		},(error: any) => {
				
		},() => {
			this._spinner.hide();
		});
	}

	onSubmit(){
		
		this._api.insertData('hospital/add', this.add_form.value).subscribe((data: any) => {
			this._ts.success('Success', data.message);
			this.getMainData();
			this.modalCreate.dismiss();
		},(error: any) => {
				
		},() => {
			this._spinner.hide();
		});

		this.animate_effect = 'swing';
	}

	onSubmitEdit(){
		// get dulu id record nya
		this.edit_form.value.id = this.id_rowselect;
		
		this._api.updateData('hospital/edit', this.edit_form.value).subscribe((data: any) => {
			this._ts.success('Success', data.message);
			this.getMainData();
			this.modalEdit.dismiss();
		},(error: any) => {
				
		},() => {
			this._spinner.hide();
		});
	}

}
