import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { BsModalComponent  } from 'ng2-bs3-modal';
import { NgxSpinnerService } from 'ngx-spinner';
import { Api } from '../../../service/api';

@Component({
  selector: 'app-diagnosa-subcase',
  templateUrl: './diagnosa-subcase.html',
  styleUrls: ['./diagnosa-subcase.css']
})
export class DiagnosaSubcaseComponent implements OnInit {
	public title_table 			: string = 'Tabel Diagnosa Subcase';

	public privilege_list		: any = JSON.parse(localStorage.getItem('currentUser')).data.privilege_list;

	public maindata				: any;
	public cols					: any[];
	public add_form				: FormGroup;
	public edit_form			: FormGroup;
	public id_rowselect 		: number;

	//dropdown
	public user_level			: any;
	public case_list			: any;

	// effect animate
	public animate_effect		: string = '';

	//flag
	public button_export		: boolean = false;

	@ViewChild('create')	public modalCreate	: BsModalComponent;
	@ViewChild('edit')		public modalEdit	: BsModalComponent;

	constructor(
		private _api		: Api,
		private _fb			: FormBuilder,
		private _ts			: ToastrService,
		private _spinner	: NgxSpinnerService
	) {

		this.add_form = _fb.group({
			'id_case'				: [null, Validators.required],
			'name'					: [null, Validators.required],
		});

		this.edit_form = _fb.group({
			'id_case'				: [null, Validators.required],
			'name'					: [null, Validators.required],
		});
	}

	/* -- Lifecycle Hooks Code -- */
	ngOnInit() {
		this.getMainData();
		this.getDataDropdown();
	}

	/* -- Function Code -- */
	OpenModal() {
		this.modalCreate.open();
	}

	handleRowSelect(event: any){
		this.id_rowselect = event.data.id;

		this.edit_form.get('id_case').setValue(event.data.id_case);
		this.edit_form.get('name').setValue(event.data.name);

		this.modalEdit.open();
	}

	getMainData(){
		// setup column P-table
		this.cols = [
			{ field: 'case_name', header: 'Case' },
			{ field: 'name', header: 'Nama' },
		];

		// get data from API
		this._api.getData('diagnosa-subcase/all').subscribe((data: any)=> {
			this.maindata = data;
		},(error: any) => {
				
		},() => {
			this._spinner.hide();
		});
	}

	getDataDropdown(){
		// dropdown user level
		this._api.getData('diagnosa-subcase/dropdown').subscribe((data: any)=> {
			this.case_list = data;
			this.case_list.unshift({label:'-Select-', value:null});
		});
	}

	onSubmit(){
		
		this._api.insertData('diagnosa-subcase/add', this.add_form.value).subscribe((data: any) => {
			this._ts.success('Success', data.message);
			this.getMainData();
			this.modalCreate.dismiss();
		},(error: any) => {
				
		},() => {
			this._spinner.hide();
		});

		this.animate_effect = 'swing';
	}

	onSubmitEdit(){
		// get dulu id record nya
		this.edit_form.value.id = this.id_rowselect;
		
		this._api.updateData('diagnosa-subcase/edit', this.edit_form.value).subscribe((data: any) => {
			this._ts.success('Success', data.message);
			this.getMainData();
			this.modalEdit.dismiss();
		},(error: any) => {
				
		},() => {
			this._spinner.hide();
		});
	}

}
