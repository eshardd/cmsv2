import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { BsModalComponent  } from 'ng2-bs3-modal';
import { NgxSpinnerService } from 'ngx-spinner';
import { Api } from '../../../service/api';

@Component({
	selector: 'app-warehouse',
	templateUrl: './warehouse.html',
	styleUrls: ['./warehouse.css']
})
export class WarehouseComponent implements OnInit {
	public title_table 			: string = 'Table Warehouse';

	public privilege_list		: any = JSON.parse(localStorage.getItem('currentUser')).data.privilege_list;

	public maindata		: any;
	public cols			: any[];
	public id_rowselect : number;

	public add_form		: FormGroup;
	public edit_form	: FormGroup;

	//flag
	public button_export		: boolean = false;

	//dropdown
	public subarea				:any;

	@ViewChild('create')	public modalCreate	: BsModalComponent;
	@ViewChild('edit')		public modalEdit	: BsModalComponent;

	constructor(
		private _api		: Api,
		private _fb			: FormBuilder,
		private _ts			: ToastrService,
		private _spinner	: NgxSpinnerService
	) {
		this.add_form = _fb.group({
			'name'					: [null, Validators.required],
			'subarea'				: [null, Validators.required],
			'location'				: [null]
		});
		
		this.edit_form = _fb.group({
			'name'					: [null, Validators.required],
			'subarea'				: [null, Validators.required],
			'location'				: [null]
		});
	}

	/* -- Lifecycle Hooks Code -- */
	ngOnInit() {
		this.getDataUser();
		this.getDataDropdown();
	}

	/* -- Function Code -- */
	OpenModal() {
		this.modalCreate.open();
	}

	getDataUser(){
		// setup column P-table
		this.cols = [
			{ field: 'name', header: 'Nama' },
			{ field: 'subarea_name', header: 'Sub Area' },
			{ field: 'location', header: 'Lokasi' }
		];

		// get data from API
		this._api.getData('warehouse/all').subscribe((data: any)=> {
			this.maindata = data;
		},(error: any) => {
				
		},() => {
			this._spinner.hide();
		});
	}

	handleRowSelect(event: any){
		this.id_rowselect = event.id;

		this.edit_form.get('name').setValue(event.name);
		this.edit_form.get('location').setValue(event.location);
		this.modalEdit.open();
	}

	onSubmit(){
		
		this._api.insertData('warehouse/add', this.add_form.value).subscribe((data: any) => {
			this._ts.success('Success', data.message);
			this.getDataUser();
			this.modalCreate.dismiss();
		},(error: any) => {
				
		},() => {
			this._spinner.hide();
		});
	}

	onSubmitEdit(){
		// get dulu id record nya
		this.edit_form.value.id = this.id_rowselect;
		this._api.updateData('warehouse/edit', this.edit_form.value).subscribe((data: any) => {
			this._ts.success('Success', data.message);
			this.getDataUser();
			this.modalEdit.dismiss();
		},(error: any) => {
				
		},() => {
			this._spinner.hide();
		});
	}

	getDataDropdown(){
		// dropdown user level
		this._api.getData('warehouse/dropdown').subscribe((data: any)=> {
			this.subarea 		= data.subarea;
			this.subarea.unshift({label:'-Select-', value:null});
			
		},(error: any) => {
				
		},() => {
			this._spinner.hide();
		});
	}
}