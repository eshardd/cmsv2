import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { BsModalComponent  } from 'ng2-bs3-modal';
import { NgxSpinnerService } from 'ngx-spinner';
import { Api } from '../../service/api';
import { Info_Rencana_Operasi } from '../../master/class';

class count_data {
	all: any;
	on_the_way: any;
	arrived: any;
}


@Component({
  selector: 'app-penarikan-barang',
  templateUrl: './penarikan-barang.html',
  styleUrls: ['./penarikan-barang.css']
})
export class PenarikanBarangComponent implements OnInit {
	public title_table 			: string = 'Tabel Diagnosa Case';

	public privilege_list		: any = JSON.parse(localStorage.getItem('currentUser')).data.privilege_list;

	public maindata				: any;
	public temp_maindata		: any;
	public count_maindata		= new count_data();

	public cols					: any[];
	public add_form				: FormGroup;
	public edit_form			: FormGroup;
	public id_rowselect 		: number;
	public info_ro 				= new Info_Rencana_Operasi();
	public info_box 			: any;
	public info_alat_pendukung  : any;
	public no_spb				: any;

	//dropdown
	public dropdown			: any;

	// effect animate
	public animate_effect		: string = '';

	//flag
	public button_export		: boolean = false;

	@ViewChild('create')	public modalCreate	: BsModalComponent;
	@ViewChild('edit')		public modalEdit	: BsModalComponent;

	constructor(
		private _api		: Api,
		private _fb			: FormBuilder,
		private _ts			: ToastrService,
		private _spinner	: NgxSpinnerService
	) {

		this.add_form = _fb.group({
			'id_spb'				: [null],
			'id_ro'					: [null, Validators.required],
			'kurir'					: [null, Validators.required],
			'no_kendaraan'			: [null, Validators.required],
			'etd'					: [null, Validators.required],
			'eta'					: [null, Validators.required],
		});

		this.edit_form = _fb.group({
			//'id_ro'					: [null, Validators.required],
			'kurir'					: [null, Validators.required],
			'no_kendaraan'			: [null, Validators.required],
			'etd'					: [null, Validators.required],
			'eta'					: [null, Validators.required],
		});
	}

	/* -- Lifecycle Hooks Code -- */
	ngOnInit() {
		this.getMainData();
		this.getDataDropdown();
	}

	/* -- Function Code -- */
	OpenModal() {
		this.add_form.reset();
		this.info_ro = new Info_Rencana_Operasi();
		this.info_box = null;
		this.info_alat_pendukung = null;
		this.modalCreate.open('lg');
	}

	onChangeRO(event){
		var id_ro = event.value;
		this._api.getData('penarikan-barang/list/'+id_ro).subscribe((data: any)=> {
			this.info_ro.id = data.id;
			this.info_ro.no_ro = data.no_ro;
			this.info_ro.hospital_name = data.hospital_name;
			this.info_ro.doctor_name = data.doctor_name;
			this.info_ro.request_instrument_name = data.request_instrument_name;
			this.info_ro.jenis_tagihan_name = data.jenis_tagihan_name;
			this.info_ro.pasien = data.pasien;
			this.info_ro.type_ro_name = data.type_ro_name;
			this.info_ro.remarks = data.remarks;
			this.info_ro.status_box = data.status_box;
			this.info_ro.date = data.date;
			this.add_form.get('id_spb').setValue(data.id_spb);
		});
		this._api.getData('spb/list_box/'+id_ro).subscribe((data: any)=> {
			this.info_box = data;
		});
		this._api.getData('spb/list_alat_pendukung/'+id_ro).subscribe((data: any)=> {
			this.info_alat_pendukung = data;
		});
	}
	
	getDataDropdown(){
		// dropdown user level
		this.dropdown = [];
		this._api.getData('penarikan-barang/dropdown').subscribe((data: any)=> {
			this.dropdown['ro'] = data.ro,
			this.dropdown['spb'] = data.spb,
			this.dropdown['spb_edit'] = data.spb_edit,
			this.dropdown['kurir'] = data.kurir,
			this.dropdown['kendaraan'] = data.kendaraan,
			this.dropdown['ro'].unshift({label:'-Pilih RO-', value:null});
			this.dropdown['spb'].unshift({label:'-Pilih Spb-', value:null});
			this.dropdown['spb_edit'].unshift({label:'-Pilih Spb-', value:null});
			this.dropdown['kurir'].unshift({label:'-Pilih Kurir-', value:null});
			this.dropdown['kendaraan'].unshift({label:'-Pilih kendaraan-', value:null});
		});
	}

	handleRowSelect(event: any){
		this.id_rowselect = event.id;
		this.no_spb = event.id_spb;
		this._api.getData('penarikan-barang/get_no_ro/'+event.id_spb).subscribe((data: any)=> {
			var id_ro = data.id_ro;
			this._api.getData('spb/list/'+id_ro).subscribe((data: any)=> {
				this.info_ro.id = data.id;
				this.info_ro.no_ro = data.no_ro;
				this.info_ro.hospital_name = data.hospital_name;
				this.info_ro.doctor_name = data.doctor_name;
				this.info_ro.request_instrument_name = data.request_instrument_name;
				this.info_ro.jenis_tagihan_name = data.jenis_tagihan_name;
				this.info_ro.pasien = data.pasien;
				this.info_ro.type_ro_name = data.type_ro_name;
				this.info_ro.remarks = data.remarks;
				this.info_ro.status_box = data.status_box;
				this.info_ro.date = data.date;
			});
			this._api.getData('spb/list_box/'+id_ro).subscribe((data: any)=> {
				this.info_box = data;
			});
			this._api.getData('spb/list_alat_pendukung/'+event.id_ro).subscribe((data: any)=> {
				this.info_alat_pendukung = data;
			});
		});
		
		//this.edit_form.get('id_ro').setValue(event.id_ro);
		this.edit_form.get('kurir').setValue(event.courier);
		this.edit_form.get('no_kendaraan').setValue(event.no_kendaraan);
		this.edit_form.get('etd').setValue(new Date(event.etd));
		this.edit_form.get('eta').setValue(new Date(event.eta));
		this.modalEdit.open('lg');
	}

	getMainData(){
		// setup column P-table
		this.cols = [
			{ field: 'name', header: 'Nama' },
		];

		// get data from API
		this._api.getData('penarikan-barang/all').subscribe((data: any)=> {
			this.maindata = data;
			this.temp_maindata = data;
			this.calculate_quantity(data);
		},(error: any) => {
				
		},() => {
			this._spinner.hide();
		});
	}

	calculate_quantity(data){
		this.temp_maindata = data;

		this.count_maindata.all = data.length;
		this.count_maindata.on_the_way = data.filter( x => x['atd'] == null).length;
		this.count_maindata.arrived = data.filter( x => x['atd'] != null).length;
	}

	onSubmit(){
		this._spinner.show();
		this.add_form.value.id_user = JSON.parse(localStorage.getItem('currentUser')).data.id;
		this.add_form.value.id_ro = this.info_ro.id;
		this._api.insertData('penarikan-barang/add', this.add_form.value).subscribe((data: any) => {
			this._ts.success('Success', data.message);
			this.getMainData();
			this.getDataDropdown();
			this.modalCreate.dismiss();
		},(error: any) => {
				
		},() => {
			this._spinner.hide();
		});
	}

	onSubmitEdit(){
		this._spinner.show();
		// get dulu id record nya
		this.edit_form.value.id = this.id_rowselect;
		this.edit_form.value.id_ro = this.info_ro.id;
		this.edit_form.value.id_spb = this.no_spb;
		
		this._api.updateData('penarikan-barang/edit', this.edit_form.value).subscribe((data: any) => {
			this._ts.success('Success', data.message);
			this.getMainData();
			this.modalEdit.dismiss();
		},(error: any) => {
				
		},() => {
			this._spinner.hide();
		});
	}

	done(event){
		event.id_user = JSON.parse(localStorage.getItem('currentUser')).data.id;
		if(confirm("Apakah anda yakin ingin Telah selesai menarik barang? Waktu Tiba akan di update Otomatis")) {
			this._api.updateData('penarikan-barang/done', event).subscribe((data: any) => {
				this._ts.success('Success', data.message);
			},(error: any) => {
				
			},() => {
				this.getMainData();
				this.modalCreate.dismiss();
				this._spinner.hide();
			});
		}
	}

	filter_fix(param){
		if (param == 'all') {
			this.maindata = this.temp_maindata;
		}else if (param == 'on_the_way'){
			this.maindata = this.temp_maindata.filter( x => x['atd'] == null);
		}else if (param == 'arrived'){
			this.maindata = this.temp_maindata.filter( x => x['atd'] != null);
		}
	}

}
