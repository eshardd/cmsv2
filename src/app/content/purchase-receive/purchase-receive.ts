import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { BsModalComponent  } from 'ng2-bs3-modal';
import { NgxSpinnerService } from 'ngx-spinner';
import { Api } from '../../service/api';
import { Router, Routes, ActivatedRoute, Params } from '@angular/router';

class add_pr {
	data:any;
	detail:any;
}

class add_termin {
	id_user:null;
	page:number;
	date:null;
	data:any;
	id_pr:any;
}

@Component({
	selector: 'app-purchase-receive',
	templateUrl: './purchase-receive.html',
	styleUrls: ['./purchase-receive.css']
})
export class PurchaseReceiveComponent implements OnInit {
	public title_table 			: string = 'Tabel Penerimaan Barang';
	
	public privilege_list		: any = JSON.parse(localStorage.getItem('currentUser')).data.privilege_list;
	
	public maindata				: any;
	public cols				  	: any[];
	public add_form				: FormGroup;
	public edit_form			: FormGroup;
	public id_rowselect 		: number;
	public info_pr 				: number;
	public po_form_state		: number = 0;
	public flag_status			: number = 1;

	public list_pesanan			: any;
	
	public store		    	= new add_pr;
	public store_termin		   	= new add_termin;
	public store_temp1			= new Array;
	public store_temp2			= new Array;
	
	//dropdown
	public dropdown       		: any;
	public dropdown_temp 		: any;
	
	@ViewChild('create')	public modalCreate	: BsModalComponent;
	@ViewChild('edit')		public modalEdit	: BsModalComponent;
	@ViewChild('termin')	public modalTermin	: BsModalComponent;
	@ViewChild('printview')	public modalPDFview	: BsModalComponent;
	
	constructor(
		private _api		: Api,
		private _fb			: FormBuilder,
		private _ts			: ToastrService,
		private _spinner	: NgxSpinnerService,
		public _router		: Router
		) {
			
			this.add_form = _fb.group({
				'name'						: [null, Validators.required],
				'date'					    : [null, Validators.required],
				'purchase_request'		    : [null, Validators.required],
				'id_product_detail'		    : [null, Validators.required],
				'quantity'				    : [null, Validators.required],
			});
			
			this.edit_form = _fb.group({
				'product'       			: [null, Validators.required],
				'product_detail'			: [null, Validators.required],
				'quantity'					: [null, Validators.required],
				'date'					    : [{value:null,disabled:true}],
				'no_pr'					    : [{value:null,disabled:true}],
				'created_by'				: [{value:null,disabled:true}],
				'status'					: [null]
			});
	}
		
	/* -- Lifecycle Hooks Code -- */
	ngOnInit() {
		this.po_form_state = 1;
		this.getMainData();
		this.getDataDropdown()
	}
	
	/* -- Function Code -- */
	getMainData(){			
		// get data from API
		this._api.getData('purchase_receive/all').subscribe((data: any)=> {
			this.maindata = data;
		});
	}

	OpenModal() {
		this.po_form_state = 1;
		this.add_form.reset();
		this.store_temp1 = [];
		this.modalCreate.open('lg');
	}

	getDataDropdown(){
		this.dropdown = [];
		this.dropdown_temp = [];
		this._api.getData('purchase_receive/dropdown').subscribe((data: any)=> {
			this.dropdown['purchase_request'] = data.purchase_request;
			this.dropdown['purchase_request'].unshift({label:'-Pilih Nomor Request-', value:null});

			this.dropdown_temp = data;
		}); 
	}

	changePO(event){
		let id_po = event.value;

		this.dropdown['product_detail'] = this.dropdown_temp.product_detail.filter( x => x['id_pr'] == id_po);
		this.dropdown['product_detail'].unshift({label:'-Pilih Product-', value:null});

		this._api.getData('purchase_receive/request_list/'+id_po).subscribe((data: any)=> {
			this.list_pesanan = data;
			console.log(this.list_pesanan);
		}); 
	}

	tambah_list_add(){
		this._api.getData('purchase_request/product_detail/'+this.add_form.value.id_product_detail).subscribe((data: any)=> {
			var valueToPush = { };
			valueToPush["product"] 				= data.id_product;
			valueToPush["product_name"] 		= data.product_name;
			valueToPush["product_detail"] 		= data.id;
			valueToPush["product_detail_code"] 	= data.code;
			valueToPush["product_detail_name"] 	= data.name;
			valueToPush["quantity"] 			= this.add_form.value.quantity;
			
			// prevent duplicate row with same id
			var store = true;
			for( var i = 0, len = this.store_temp1.length; i < len; i++ ) {
				if( this.store_temp1[i]['product_detail'] == this.add_form.value.id_product_detail ) {
					store = false;
					this.store_temp1[i]['quantity'] = this.store_temp1[i]['quantity']*1 + this.add_form.value.quantity*1; 			
				}
			}
			if (store == true) {
				this.store_temp1.push(valueToPush);
			}
		});
	}

	removelist(selected_data:any){		
		let data = this.store_temp1.filter( x => x['id_product_detail'] != selected_data.id_product_detail);
		this.store_temp1 = data;
	}

	onSubmit(){
		if(this.store_temp1.length < 1){
			alert("Item tidak boleh kosong");
		}else{
			this.add_form.value.id_user = JSON.parse(localStorage.getItem('currentUser')).data.id;
			this.store.detail = this.store_temp1;
			this.store.data = this.add_form.value;

			if(confirm("Apakah anda yakin ingin?")) {
				this._api.insertData('purchase_receive/add', this.store).subscribe((data: any) => {
					this._ts.success('Success', data.message);
					this.ngOnInit();
					this.modalCreate.dismiss();
				});
			}
		}
	}

	handleRowSelect(event: any){
		this._router.navigate(['purchase-receive-detail/',event.id]);
	}
}
