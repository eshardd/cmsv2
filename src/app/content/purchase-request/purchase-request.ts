import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { BsModalComponent  } from 'ng2-bs3-modal';
import { NgxSpinnerService } from 'ngx-spinner';
import { Api } from '../../service/api';
import { Router, Routes, ActivatedRoute, Params } from '@angular/router';

class add_pr {
	id_user:null;
	page:number;
	date:null;
	data:any;
}

class add_termin {
	id_user:null;
	page:number;
	date:null;
	data:any;
	id_pr:any;
}

@Component({
	selector: 'app-purchase-request',
	templateUrl: './purchase-request.html',
	styleUrls: ['./purchase-request.css']
})
export class PurchaseRequestComponent implements OnInit {
	public title_table 			: string = 'Tabel Purchase Request';

	public privilege_list		: any = JSON.parse(localStorage.getItem('currentUser')).data.privilege_list;
	
	public maindata				: any;
	public data_po				: any;
	public data_po_detail		: any;
	public list_po				: any;
	public po_list_id			: any;
	public cols				  	: any[];
	public add_form				: FormGroup;
	public edit_form			: FormGroup;
	public termin_form			: FormGroup;
	public id_rowselect 		: number;
	public info_pr 				: number;
	public po_form_state		: number = 0;
	public flag_status			: number = 1;
	
	public store		    	= new add_pr;
	public store_termin		   	= new add_termin;
	public store_temp1			= new Array;
	public store_temp2			= new Array;
	
	//dropdown
	public dropdown       		: any;
	public dropdown_termin 		: any;
		
	@ViewChild('create')	public modalCreate	: BsModalComponent;
	@ViewChild('edit')		public modalEdit	: BsModalComponent;
	@ViewChild('termin')	public modalTermin	: BsModalComponent;
	@ViewChild('printview')	public modalPDFview	: BsModalComponent;
	
	constructor(
		private _api		: Api,
		private _fb			: FormBuilder,
		private _ts			: ToastrService,
		private _spinner	: NgxSpinnerService,
		public _router		: Router
		) {
			
			this.add_form = _fb.group({
				'product'       			: [null, Validators.required],
				'product_detail'			: [null, Validators.required],
				'quantity'					: [null, Validators.required],
				'date'					    : [null, Validators.required],
			});
			
			this.edit_form = _fb.group({
				'product'       			: [null, Validators.required],
				'product_detail'			: [null, Validators.required],
				'quantity'					: [null, Validators.required],
				'date'					    : [{value:null,disabled:true}],
				'no_pr'					    : [{value:null,disabled:true}],
				'created_by'				: [{value:null,disabled:true}],
				'status'					: [null]
			});

			this.termin_form = _fb.group({
				'product'       			: [null],
				'product_detail'			: [null],
				'quantity'					: [null],
				'date'					    : [null],
				'no_pr'					    : [null],
				'created_by'				: [null],
				'status'					: [null],
				'description'				: [null]
			});
		}
		
		/* -- Lifecycle Hooks Code -- */
		ngOnInit() {
			this.po_form_state = 1;
			this.getMainData();
			this.getDataDropdown();
		}
		
		/* -- Function Code -- */

		OpenModal() {
			this.po_form_state = 1;
			this.add_form.reset();
			this.store_temp1 = [];
			this.modalCreate.open('lg');
		}
		
		handleRowSelect(event: any){
			this._router.navigate(['purchase-request-detail/',event.id]);
			// this.flag_status = event.status;

			// this.dropdown.status = [
			// 	{value:'1',label:'Belum diproses'},
			// 	{value:'2',label:'On Proses'},
			// 	{value:'3',label:'Selesai'},
			//   ];

			// this.id_rowselect = event.id;
			
			// this.edit_form.get('date').setValue(new Date(event.date));
			// this.edit_form.get('created_by').setValue(event.created_by_name);
			// this.edit_form.get('no_pr').setValue(event.no_pr);
			// this.edit_form.get('status').setValue(event.status);
			// this.list_purchase_request();
			// this.modalEdit.open('lg');
		}
		
		list_purchase_request(){
			this._api.getData('purchase_request/list/'+this.id_rowselect).subscribe((data: any) => {
				this.list_po = data;
				//console.log(this.list_po);
			});
		}
		
		editPOList(select_data:any){
			this.po_form_state = 2;
			this.edit_form.get('product').setValue(select_data.id_product);
			this.select_product(select_data.id_product);
			this.edit_form.get('product_detail').setValue(select_data.id_product_detail);
			this.edit_form.get('quantity').setValue(select_data.quantity);
			this.po_list_id = select_data.id;
		}
		
		edit_list_po(){
			this.edit_form.value.po_list_id = this.po_list_id;
			this._api.updateData('purchase_request/edit_list', this.edit_form.value).subscribe((data: any) => {
				this._ts.success('Success', data.message);
				this.list_purchase_request();
			});
		}
		
		removePOList(selected_data:any){		
			if(confirm("Apakah anda yakin ingin menghapus data?")) {
				this._api.deleteData('purchase_request/remove/'+selected_data.id).subscribe((data: any) => {
					this._ts.success('Success', data.message);
					this.list_purchase_request();
				});
			}
		}
		
		removelist(selected_data:any){		
			let data = this.store_temp1.filter( x => x['id_product_detail'] != selected_data.id_product_detail);
			this.store_temp1 = data;
		}

		getMainData(){			
			// get data from API
			this._api.getData('purchase_request/all').subscribe((data: any)=> {
				this.maindata = data;
			});
		}
		
		receive_termin(event: any){
			this.id_rowselect = event.id;
			this.termin_form.get('date').setValue(new Date(event.date));
			this.getDataDropdownTermin();
			this.list_purchase_request();
			this.modalTermin.open('lg');
		}

		getDataDropdown(){
			this.dropdown = [];
			this._api.getData('purchase_request/dropdown_product').subscribe((data: any)=> {
				this.dropdown['product'] = data.product;
				this.dropdown['product'].unshift({label:'-Pilih product-', value:null});
			}); 
		}

		getDataDropdownTermin(){
			this.dropdown_termin = [];
			this._api.getData('purchase_request/dropdown_termin/'+this.id_rowselect).subscribe((data: any)=> {
				this.info_pr = data.purchase_request;
				this.dropdown['product_detail'] = data.purchase_request_detail;
				this.dropdown['product_detail'].unshift({label:'-Pilih product-', value:null});
				console.log(this.dropdown_termin['product_detail']);
			}); 
		}

		onChangeProduct(event){
			console.log(event);
		}

		tambah_list_masuk(){
			this._api.getData('purchase_request/product_detail/'+this.termin_form.value.product_detail).subscribe((data: any)=> {
				var valueToPush = { };
				valueToPush["product"] 				= data.id_product;
				valueToPush["product_name"] 		= data.product_name;
				valueToPush["product_detail"] 		= data.id;
				valueToPush["product_detail_code"] 	= data.code;
				valueToPush["product_detail_name"] 	= data.name;
				valueToPush["quantity"] 			= this.termin_form.value.quantity;
				
				// prevent duplicate row with same id
				var store = true;
				for( var i = 0, len = this.store_temp2.length; i < len; i++ ) {
					if( this.store_temp2[i]['product_detail'] == this.termin_form.value.product_detail ) {
						this.store_temp2[i]['quantity'] = this.termin_form.value.quantity;
						store = false;
					}
				}
				if (store == true) {
					this.store_temp2.push(valueToPush);
				}
				
				console.log(this.store_temp2);
			});
		}

		editStore2(data){
			console.log(data);
		}
		
		removeStore2(data){
			let data_ = this.store_temp2.filter( x => x['product_detail'] != data.product_detail);
			this.store_temp2 = data_;
		}
		
		onSubmitTermin(){
			this.store_termin.data     = this.store_temp2;
			this.store_termin.id_user  = JSON.parse(localStorage.getItem('currentUser')).data.id;
			this.store_termin.page		= 1;
			this.store_termin.id_pr		= this.id_rowselect;
			
			this._api.insertData('purchase_request/add_termin', this.store_termin).subscribe((data: any) => {
				this._ts.success('Success', data.message);
				this.getMainData();
				this.modalTermin.dismiss();
			});
		}

		select_product(event){
			this.dropdown['product_detail'] = [];
			this._api.getData('purchase_request/dropdown_product_detail/'+event).subscribe((data: any)=> {
				this.dropdown['product_detail'] = data;
				this.dropdown['product_detail'].unshift({label:'-Pilih-', value:null});
			});
		}
		
		onSubmit(){
			this.store.date     = this.add_form.value.date;
			this.store.data     = this.store_temp1;
			this.store.id_user  = JSON.parse(localStorage.getItem('currentUser')).data.id;
			this.store.page		= 1;
			
			this._api.insertData('purchase_request/add', this.store).subscribe((data: any) => {
				this._ts.success('Success', data.message);
				this.getMainData();
				this.modalCreate.dismiss();
			});
		}
		
		onSubmitEdit(){
			// get dulu id record nya
			this.edit_form.value.id = this.id_rowselect;
			
			this._api.updateData('diagnosa-case/edit', this.edit_form.value).subscribe((data: any) => {
				this._ts.success('Success', data.message);
				this.getMainData();
				this.modalEdit.dismiss();
			},(error: any) => {
				
			},() => {
				this._spinner.hide();
			});
		}

		update_data(){
			this.edit_form.value.id = this.id_rowselect;
			this.edit_form.value.id_user = JSON.parse(localStorage.getItem('currentUser')).data.id;
			this._api.updateData('purchase_request/edit_data',this.edit_form.value).subscribe((data: any)=> {
				this._ts.success('Success', data.message);
				this.getMainData();
				this.modalEdit.dismiss();
			});
		}
		
		tambah_list_add(){
			this._api.getData('purchase_request/product_detail/'+this.add_form.value.product_detail).subscribe((data: any)=> {
				var valueToPush = { };
				valueToPush["product"] 				= data.id_product;
				valueToPush["product_name"] 		= data.product_name;
				valueToPush["product_detail"] 		= data.id;
				valueToPush["product_detail_code"] 	= data.code;
				valueToPush["product_detail_name"] 	= data.name;
				valueToPush["quantity"] 			= this.add_form.value.quantity;
				
				// prevent duplicate row with same id
				var store = true;
				for( var i = 0, len = this.store_temp1.length; i < len; i++ ) {
					if( this.store_temp1[i]['product_detail'] == this.add_form.value.product_detail ) {
						store = false;
						this.store_temp1[i]['quantity'] = this.store_temp1[i]['quantity']*1 + this.add_form.value.quantity*1; 			
					}
				}
				if (store == true) {
					this.store_temp1.push(valueToPush);
				}
				
				console.log(this.store_temp1);
			});
		}
		
		
		viewPDF(id) {
			this.data_po = [];
			this.data_po_detail = [];
			
			this._api.getData('purchase_request/view_list/' + id).subscribe((data: any) => {
				this.data_po = data.po;
				this.data_po_detail = data.list;
				//this.show_nama_product(id);
			});
			this.modalPDFview.open('lg');
		}
	}
	