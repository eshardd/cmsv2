import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { BsModalComponent  } from 'ng2-bs3-modal';
import { NgxSpinnerService } from 'ngx-spinner';
import { Api } from '../../../service/api';
import { Router, Routes, ActivatedRoute, Params } from '@angular/router';
import { Dropdown } from 'primeng/dropdown';

@Component({
	selector: 'app-purchase-request-detail',
	templateUrl: './purchase-request-detail.html',
	styleUrls: ['./purchase-request-detail.css']
})
export class PurchaseRequestDetailComponent implements OnInit {
	
	public title_table 				: string = 'Tabel Purchase Request Detail';
	public privilege_list			: any = JSON.parse(localStorage.getItem('currentUser')).data.privilege_list;
	public purchase_request			: any;
	public purchase_request_detail 	: any;
	public id_pr 					: any;
	public purchase_request_form	: FormGroup;
	public purchase_request_detail_edit_form	: FormGroup;

	public dropdown					: any;
	
	@ViewChild('edit')		public modalEdit	: BsModalComponent;

	constructor(
		private _api		        : Api,
		private _fb			        : FormBuilder,
		private _ts			        : ToastrService,
		private _spinner	      	: NgxSpinnerService,
		public _router		      	: Router,
		private _activeroute		: ActivatedRoute
		) {
		this.purchase_request_form = _fb.group({
			'date'       	: [{value:null,disabled:true}, Validators.required],
			'no_pr'			: [{value:null,disabled:true}, Validators.required],
			'created_date'	: [null, Validators.required],
			'request_by'	: [{value:null,disabled:true}, Validators.required]
		});

		this.purchase_request_detail_edit_form = _fb.group({
			'id'       			: [null, Validators.required],
			'id_pr'       		: [null, Validators.required],
			'id_product_detail'	: [null, Validators.required],
			'quantity'			: [null, Validators.required],
		});
	}

	ngOnInit() {
		this.id_pr = this._activeroute.snapshot.paramMap.get('id');
		this.getData(this.id_pr);

		//tes
		this._api.getData('purchase_request_detail/log/'+this.id_pr).subscribe((data: any)=> {
		});
	}

	getData(id_pr){
		this._api.getData('purchase_request_detail/list/'+id_pr).subscribe((data: any)=> {
			this.purchase_request 			= data.purchase_request;
			this.purchase_request_detail 	= data.purchase_request_detail;

			// fill form
			this.purchase_request_form.get('date').setValue(this.purchase_request.date);
			this.purchase_request_form.get('no_pr').setValue(this.purchase_request.no_pr);
			this.purchase_request_form.get('request_by').setValue(this.purchase_request.created_by_name);
		});
	}
	
	edit_product_detail(param){
		this.getDataDropdown(param.id_product);
		this.purchase_request_detail_edit_form.get('id').setValue(param.id);
		this.purchase_request_detail_edit_form.get('id_pr').setValue(param.id_pr);
		this.purchase_request_detail_edit_form.get('id_product_detail').setValue(param.id_product_detail);
		this.purchase_request_detail_edit_form.get('quantity').setValue(param.quantity);
		this.modalEdit.open('sm');
	}

	remove_product_detail(param){

	}

	getDataDropdown(id_product){
		this.dropdown = [];
		this._api.getData('dropdown/product_principle/'+id_product).subscribe((data: any)=> {
			this.dropdown['product_detail'] = data.product_detail;
			console.log(data);
		});
	}

	onSubmitEdit(){
		if(confirm("Apakah anda yakin ingin Update data?")) {
			this._api.updateData('purchase_request_detail/edit', this.purchase_request_detail_edit_form.value).subscribe((data: any) => {
				this._ts.success('Success', data.message);
				this.modalEdit.dismiss();
				this.ngOnInit();
			});
		}
	}
}
	