import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { BsModalComponent  } from 'ng2-bs3-modal';
import { NgxSpinnerService } from 'ngx-spinner';
import { Api } from '../../service/api';

@Component({
  selector: 'app-potong-rod',
  templateUrl: './potong-rod.html',
  styleUrls: ['./potong-rod.css']
})
export class PotongRodComponent implements OnInit {
  public title_table 			: string = 'Tabel Potong Rod';

  public privilege_list		: any = JSON.parse(localStorage.getItem('currentUser')).data.privilege_list;

  public maindata				: any;
  public cols					: any[];
  public dropdown					: any[];

  public dropdown_product		: any[];
  public dropdown_item_potong		: any[];
  public dropdown_item_menjadi		: any[];


  public add_form				: FormGroup;
  public edit_form			: FormGroup;
  public id_rowselect 		: number;

  //dropdown
  public user_level			: any;

  // effect animate
  public animate_effect		: string = '';

  //flag
  public button_export		: boolean = false;

  @ViewChild('create')	public modalCreate	: BsModalComponent;
  @ViewChild('edit')		public modalEdit	: BsModalComponent;

constructor(
  private _api		: Api,
  private _fb			: FormBuilder,
  private _ts			: ToastrService,
  private _spinner	: NgxSpinnerService
  ) {
    
    this.add_form = _fb.group({
      'potong_product'					: [null, Validators.required],
      'potong_product_id'		    : [null, Validators.required],
      'menjadi_product'					: [null, Validators.required],
      'menjadi_product_id'	    : [null, Validators.required],
      'quantity_potong'					: [null, Validators.required],
      'quantity_menjadi'				: [null, Validators.required],
    });
    
    this.edit_form = _fb.group({
      'name'					: [null, Validators.required],
    });
  }
  
  /* -- Lifecycle Hooks Code -- */
  ngOnInit() {
    this.getMainData();
    this.getDropdown();
  }
  
  /* -- Function Code -- */
  OpenModal() {
    this.modalCreate.open();
  }

  get_item_potong(value){
    this._api.getData('potong-rod/product_item/'+value).subscribe((data: any)=> {
      this.dropdown_item_potong = data;
      this.dropdown_item_potong.unshift({label:'-Pilih Item-', value:null});
    });
  }

  get_item_menjadi(value){
    this._api.getData('potong-rod/product_item/'+value).subscribe((data: any)=> {
      this.dropdown_item_menjadi = data;
      this.dropdown_item_menjadi.unshift({label:'-Pilih Item-', value:null});
    });
  }

  getDropdown() {
    // get data from API
    this.dropdown_product = [];
    this.dropdown_item_potong = [];
    this.dropdown_item_menjadi = [];

    this._api.getData('potong-rod/dropdown').subscribe((data: any)=> {
      this.dropdown_product = data;
      this.dropdown_product.unshift({label:'-Pilih product-', value:null});
    },(error: any) => {
      
    },() => {
      this._spinner.hide();
    });
  }

  handleRowSelect(event: any){
    this.id_rowselect = event.id;
    
    this.edit_form.get('name').setValue(event.name);
  
    this.modalEdit.open();
  }
  
  getMainData(){
    // setup column P-table
    this.cols = [
      { field: 'no_pr', header: 'No' },
      { field: 'potong_product_id_name', header: 'Potong' },
      { field: 'potong_quantity', header: 'Out' },
      { field: 'menjadi_product_id_name', header: 'menjadi' },
      { field: 'menjadi_quantity', header: 'In' },
    ];
    
    // get data from API
    this._api.getData('potong-rod/all').subscribe((data: any)=> {
      this.maindata = data;
    },(error: any) => {
      
    },() => {
      this._spinner.hide();
    });
  }
  
  onSubmit(){
    if(confirm("Apakah anda yakin ingin menghapus data?")) {
      this.add_form.value.created_by = JSON.parse(localStorage.getItem('currentUser')).data.id;
      this._api.insertData('potong-rod/add', this.add_form.value).subscribe((data: any) => {
        this._ts.success('Success', data.message);
        this.getMainData();
        this.modalCreate.dismiss();
      },(error: any) => {
        
      },() => {
        this._spinner.hide();
      });
      
      this.animate_effect = 'swing';
    }
  }
  
  onSubmitEdit(){
    // get dulu id record nya
    this.edit_form.value.id = this.id_rowselect;
    
    this._api.updateData('area/edit', this.edit_form.value).subscribe((data: any) => {
      this._ts.success('Success', data.message);
      this.getMainData();
      this.modalEdit.dismiss();
    },(error: any) => {
      
    },() => {
      this._spinner.hide();
    });
  }
}
