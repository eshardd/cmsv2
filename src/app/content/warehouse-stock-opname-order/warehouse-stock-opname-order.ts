import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { BsModalComponent  } from 'ng2-bs3-modal';
import { NgxSpinnerService } from 'ngx-spinner';
import { Api } from '../../service/api';
import { Router, Routes, ActivatedRoute, Params } from '@angular/router';

@Component({
  selector: 'app-warehouse-stock-opname-order',
  templateUrl: './warehouse-stock-opname-order.html',
  styleUrls: ['./warehouse-stock-opname-order.css']
})
export class WarehouseStockOpnameOrderComponent implements OnInit {
	public title_table 			: string = 'Tabel Stok Opname';

	public privilege_list		: any = JSON.parse(localStorage.getItem('currentUser')).data.privilege_list;

	public maindata				: any;
	public cols					: any[];
	public add_form				: FormGroup;
	public edit_form			: FormGroup;
	public filter_form			: FormGroup;
	public id_rowselect 		: number;

	//dropdown
	public user_level			: any;
	public dropdown				: any;
	

	// effect animate
	public animate_effect		: string = '';

	//flag
	public button_export		: boolean = false;

	@ViewChild('create')		public modalCreate	: BsModalComponent;
	@ViewChild('edit')			public modalEdit	: BsModalComponent;

	constructor(
		private _api		: Api,
		private _fb			: FormBuilder,
		private _ts			: ToastrService,
		private _spinner	: NgxSpinnerService,
		public _router		: Router
	) {

		this.add_form = _fb.group({
			'warehouse'			: [null, Validators.required],
			'no_so'			    : [null],
			'date'			    : [null, Validators.required],
			'description'		: [null, Validators.required],
		});

		this.edit_form = _fb.group({
			'warehouse'			: [null, Validators.required],
			'no_so'			    : [null, Validators.required],
			'date'			    : [null, Validators.required],
			'description'		: [null, Validators.required],
			'status'			: [null]
		});

		const filter = _fb.group({
			'warehouse': [''],
			'month': [''],
			'year': [''],
		});

		this.filter_form 	= filter;
	}

	/* -- Lifecycle Hooks Code -- */
	OpenModal() {
		this.modalCreate.open();
	}


	ngOnInit() {
		this.getMainData();
		this.getDataDropdown();
	}

	/* -- Function Code -- */
	handleRowSelect(event: any){
		this._router.navigate(['warehouse-stock-opname',event.id]);
	}

	editForm(event:any){
		this.id_rowselect = event.id;
		this.edit_form.get('no_so').setValue(event.no_so);
		this.edit_form.get('warehouse').setValue(event.id_warehouse);
		this.edit_form.get('date').setValue(event.date);
		this.edit_form.get('description').setValue(event.description);
		this.edit_form.get('status').setValue(event.status);
		this.modalEdit.open();
	}

	getMainData(){
		// setup column P-table
		this.cols = [
			{ field: 'no_so', header: 'Nomor SO' },
			{ field: 'date', header: 'Tanggal' },
			{ field: 'warehouse_name', header: 'Gudang' },
			{ field: 'description', header: 'Keterangan' },
			{ field: 'status', header: 'Status' },
		];

		// get data from API
		this._api.getData('stock_opnam_order/all').subscribe((data: any)=> {
			this.maindata = data;
		});
	}

	getDataDropdown(){
		this.dropdown = [];
		this._api.getData('dropdown/wh').subscribe((data: any)=> {
			this.dropdown['warehouse'] = data.warehouse;
			this.dropdown['warehouse'].unshift({label:'-Pilih Warehouse-', value:null});
			this.dropdown['status'] = [
				{'value':0,'label':'Progress'},
				{'value':1,'label':'Completed'},
			]
		});

		this.dropdown['month'] = [
			{value:1,label:'Jan'},
			{value:2,label:'Feb'},
			{value:3,label:'Mar'},
			{value:4,label:'Apr'},
			{value:5,label:'Mei'},
			{value:6,label:'Jun'},
			{value:7,label:'Jul'},
			{value:8,label:'Aug'},
			{value:9,label:'Sep'},
			{value:10,label:'Oct'},
			{value:11,label:'Nov'},
			{value:12,label:'Des'},
		];
		this.dropdown['month'].unshift({label:'-Pilih Bulan-', value:null});

		this.dropdown['year'] = [
			{value:2019,label:'2019'},
		];
		this.dropdown['year'].unshift({label:'-Pilih Tahun-', value:null});
	}

	onSubmit(){
		this.add_form.value.user_id = JSON.parse(localStorage.getItem('currentUser')).data.id;
		
		this._api.insertData('stock_opnam_order/add', this.add_form.value).subscribe((data: any) => {
			this._ts.success('Success', data.message);
			this.getMainData();
		});

		this.animate_effect = 'swing';
	}

	onSubmitEdit(){
		// get dulu id record nya
		this.edit_form.value.id = this.id_rowselect;
		
		this._api.updateData('stock_opnam_order/edit', this.edit_form.value).subscribe((data: any) => {
			this._ts.success('Success', data.message);
			this.getMainData();
			this.modalEdit.dismiss();
		},(error: any) => {
				
		},() => {
			this._spinner.hide();
		});
	}

	onSubmitFilter(){
		this._api.insertData('stock_opnam_order/filterdate',this.filter_form.value).subscribe((data: any) => {
			this.maindata = data;
		});
	}

}
