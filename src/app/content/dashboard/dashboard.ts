import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { BsModalComponent  } from 'ng2-bs3-modal';
import { NgxSpinnerService } from 'ngx-spinner';
import { Api } from '../../service/api';
import { environment } from '../../../environments/environment';

import * as am4core from "@amcharts/amcharts4/core";
import * as am4charts from "@amcharts/amcharts4/charts";
import am4themes_animated from "@amcharts/amcharts4/themes/animated";

@Component({
	selector: 'app-dashboard',
	templateUrl: './dashboard.html',
	styleUrls: ['./dashboard.css']
})
export class DashboardComponent implements OnInit {


	masterdata : any;
	cols_master : any;
	@ViewChild('master')		public modalMaster		: BsModalComponent;

	principle : any;
	doctor : any;
	ro : any;
	penggunaan : any;
	pengiriman : any;
	rs_swasta : any;
	rs_negri : any;
	penarikan : any;
	operasi : any;
	total_operasi : any;
	API_URL = 'http://'+environment.API_URL;

	stage1 : any; stage1_total : any;
	stage2 : any; stage2_total : any;
	stage3 : any; stage3_total : any;
	stage4 : any; stage4_total : any;
	stage5 : any; stage5_total : any;
	stage6 : any; stage6_total : any;
	stage7 : any; stage7_total : any;

	datadetail: any;
	progress_bar: any;


	@ViewChild('projectview') public modalProjectview: BsModalComponent;

	constructor(
		private _api		  : Api,
		private _fb			  : FormBuilder,
		private _ts			  : ToastrService,
		private _spinner	: NgxSpinnerService
		) {}
		
		/* -- Lifecycle Hooks Code -- */
		ngOnInit() {

			this.cols_master = [
				{ field: 'no_ro', header: 'Nomor RO' },
				{ field: 'created_by_name', header: 'Dibuat Oleh' },
				{ field: 'date', header: 'tanggal' },
				{ field: 'operasi_type', header: 'Type' },
				{ field: 'hospital_name', header: 'RS' },
				{ field: 'doctor_name', header: 'doctor' },
				{ field: 'pasien', header: 'pasien' },
				{ field: 'jenis_tagihan_name', header: 'Jenis Tagihan' },
				{ field: 'spine_anatomi', header: 'spine anatomi' },
				{ field: 'case_name', header: 'case' },
				{ field: 'subcase_name', header: 'subcase' },
				{ field: 'ts_name', header: 'technical support' },
				{ field: 'code_box', header: 'Box' },
				{ field: 'product_name', header: 'product' },
				{ field: 'product_detail_name', header: 'Item' },
				{ field: 'quantity', header: 'Qty' },
				{ field: 'no_spb', header: 'SPB' },
				{ field: 'courier_name', header: 'Kurir' },
				{ field: 'etd', header: 'berangkat' },
				{ field: 'eta', header: 'sampai' },
				{ field: 'kendaraan_name', header: 'kendaraan' },
				{ field: 'nomor_polisi', header: 'no Polisi' },
				{ field: 'ditarik_oleh', header: 'ditarik oleh' },
				{ field: 'ditarik_pada', header: 'ditarik pada' },
				{ field: 'tiba_pada', header: 'Tiba pada' },
				{ field: 'integrated', header: 'disesuaikan' },
				{ field: 'stage_name', header: 'Status Rencana Operasi' },
			];


			//this.data_dashboard();
			this.dummy_chart();
			this.dummy_chart2();
		}
		show_card_detail(list){
			this.datadetail = [];
			this._api.getData('rencana-operasi/list/' + list.id).subscribe((data: any) => {
				this.datadetail = data;
				this.progress_bar = data.progress;
				this.show_nama_product(list.id);
				this.modalProjectview.open('lg');
			});
		}
		show_nama_product(id) {
			this._api.getData('rencana-operasi/product_list/' + id).subscribe((data: any) => {
				var namelist = [];
				if (data.length == 1) {
					this.datadetail['request_instrument_name'] = data[0].product_name;
				} else if (data.length > 1) {
					for (let i = 0; i < data.length; i++) {
						namelist.push(data[i]['product_name']);
					}
					this.datadetail['request_instrument_name'] = namelist.join(', ');
				}
			});
		}
		dummy_chart(){
			this._api.getData('dashboard/list').subscribe((data: any)=> {

				this.principle 	= data.principle['total'];
				this.doctor  	= data.doctor['total'];
				this.ro  		= data.ro['total'];
				this.penggunaan = data.penggunaan['total'];
				this.pengiriman = data.delivery;
				this.operasi 	= data.operasi;
				this.total_operasi = data.total_operasi; 
				this.penarikan = data.penarikan; 
				this.rs_negri = data.rs_negri['total']; 
				this.rs_swasta = data.rs_swasta['total']; 
				
				this.stage1 = data.stage1;
				this.stage2 = data.stage2;
				this.stage3 = data.stage3;
				this.stage4 = data.stage4;
				this.stage5 = data.stage5;
				this.stage6 = data.stage6;
				this.stage7 = data.stage7;

				this.stage1_total = data.stage1.length;
				this.stage2_total = data.stage2.length;
				this.stage3_total = data.stage3.length;
				this.stage4_total = data.stage4.length;
				this.stage5_total = data.stage5.length;
				this.stage6_total = data.stage6.length;
				this.stage7_total = data.stage7.length;
				/* Chart code */
				// Themes begin
				am4core.useTheme(am4themes_animated);
				// Themes end
				
				// Create chart instance
				let chart = am4core.create("chartdiv", am4charts.XYChart);

				this.operasi.forEach(function(element) {
					chart.data.push(
						{'date':element.tanggal,'value':element.total}
					);
				});

				// Set input format for the dates
				chart.dateFormatter.inputDateFormat = "yyyy-MM-dd";
				
				// Create axes
				let dateAxis = chart.xAxes.push(new am4charts.DateAxis());
				let valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
				
				// Create series
				let series = chart.series.push(new am4charts.LineSeries());
				series.dataFields.valueY = "value";
				series.dataFields.dateX = "date";
				series.tooltipText = "{value}"
				series.strokeWidth = 2;
				series.minBulletDistance = 15;
				
				// Drop-shaped tooltips
				series.tooltip.background.cornerRadius = 20;
				series.tooltip.background.strokeOpacity = 0;
				series.tooltip.pointerOrientation = "vertical";
				series.tooltip.label.minWidth = 40;
				series.tooltip.label.minHeight = 40;
				series.tooltip.label.textAlign = "middle";
				series.tooltip.label.textValign = "middle";
				
				// Make bullets grow on hover
				let bullet = series.bullets.push(new am4charts.CircleBullet());
				bullet.circle.strokeWidth = 2;
				bullet.circle.radius = 4;
				bullet.circle.fill = am4core.color("#fff");
				
				let bullethover = bullet.states.create("hover");
				bullethover.properties.scale = 1.3;
				
				// Make a panning cursor
				chart.cursor = new am4charts.XYCursor();
				chart.cursor.behavior = "panXY";
				chart.cursor.xAxis = dateAxis;
				chart.cursor.snapToSeries = series;
				
				// Create vertical scrollbar and place it before the value axis
				chart.scrollbarY = new am4core.Scrollbar();
				chart.scrollbarY.parent = chart.leftAxesContainer;
				chart.scrollbarY.toBack();
			});		
		}
		dummy_chart2(){

			this._api.getData('dashboard/list').subscribe((data: any)=> {
				/* Chart code */
				// Themes begin
				am4core.useTheme(am4themes_animated);
				// Themes end

				// Create chart instance
				let chart2 = am4core.create("chartdiv2", am4charts.XYChart3D);

				// Add data
				data.operasi_bulanan.forEach(function(element) {
					chart2.data.push(
						{'Month':element.month,'income':element.total,'color':chart2.colors.next()}
					);
				});

				// console.log(data.operasi_bulanan);

				// chart2.data = [{
				// "Month": "Januari",
				// "income": 23.5,
				// "color": chart2.colors.next()
				// }, {
				// "Month": "Feb",
				// "income": 26.2,
				// "color": chart2.colors.next()
				// }, {
				// "Month": "Mar",
				// "income": 30.1,
				// "color": chart2.colors.next()
				// }, {
				// "Month": "Apr",
				// "income": 29.5,
				// "color": chart2.colors.next()
				// }, {
				// "Month": "Mei",
				// "income": 24.6,
				// "color": chart2.colors.next()
				// }];

				// Create axes
				let categoryAxis = chart2.yAxes.push(new am4charts.CategoryAxis());
				categoryAxis.dataFields.category = "Month";
				categoryAxis.numberFormatter.numberFormat = "#";
				categoryAxis.renderer.inversed = true;

				let  valueAxis = chart2.xAxes.push(new am4charts.ValueAxis()); 

				// Create series
				let series = chart2.series.push(new am4charts.ColumnSeries3D());
				series.dataFields.valueX = "income";
				series.dataFields.categoryY = "Month";
				series.name = "Income";
				series.columns.template.propertyFields.fill = "color";
				series.columns.template.tooltipText = "{valueX}";
				series.columns.template.column3D.stroke = am4core.color("#fff");
				series.columns.template.column3D.strokeOpacity = 0.2;
			});
		}
		master_excel(){
			window.open(this.API_URL+'/print/master/','_blank');
			// this._api.getData('dashboard/master').subscribe((data: any)=> {
			// 	this.masterdata = data;
			// 	this.modalMaster.open('lg');
			// });
		}
	}
	