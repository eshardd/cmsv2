import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, FormArray, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { BsModalComponent  } from 'ng2-bs3-modal';
import { NgxSpinnerService } from 'ngx-spinner';
import { Api } from '../../service/api';

@Component({
	selector: 'app-warehouse-location',
	templateUrl: './warehouse-location.html',
	styleUrls: ['./warehouse-location.css']
})
export class WarehouseLocationComponent implements OnInit {


	location:any;

	lat: number = -1.9118051000223242; 
	lng: number = 116.41874913314746;

	constructor(
		private _api		: Api,
		private _fb			: FormBuilder,
		private _ts			: ToastrService,
		private _spinner	: NgxSpinnerService
	){}

	ngOnInit() {
		this.location = [
			{latitude: -6.909396, longitude: 107.700570},
			{latitude: 3.5367332511551766 , longitude: 98.67999887494625}
    	];
	}

	onChooseLocation(event:any){
		let newloc = {latitude: event.coords.lat, longitude: event.coords.lng};
		console.log(newloc);
		//this.location.push(newloc);
	}
}
