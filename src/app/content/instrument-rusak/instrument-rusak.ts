import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { BsModalComponent  } from 'ng2-bs3-modal';
import { NgxSpinnerService } from 'ngx-spinner';
import { Api } from '../../service/api';

@Component({
  selector: 'app-instrument-rusak',
  templateUrl: './instrument-rusak.html',
  styleUrls: ['./instrument-rusak.css']
})
export class InstrumentRusakComponent implements OnInit {
	public title_table 			: string = 'Tabel Instrument Rusak';
	
	public privilege_list		: any = JSON.parse(localStorage.getItem('currentUser')).data.privilege_list;
	
	public maindata				: any;
	public cols					: any[];
	public add_form				: FormGroup;
	public edit_form			: FormGroup;
	public id_rowselect 		: number;
	
	//dropdown
	public user_level			: any;
	
	// effect animate
	public animate_effect		: string = '';
	
	//flag
  public button_export		: boolean = false;
  public product_list	:any;
  public product_detail_temp	:any;
  public product_detail	:any;
	
	@ViewChild('create')	public modalCreate	: BsModalComponent;
	@ViewChild('edit')		public modalEdit	: BsModalComponent;
	
	constructor(
		private _api		: Api,
		private _fb			: FormBuilder,
		private _ts			: ToastrService,
		private _spinner	: NgxSpinnerService
		) {
			
			this.add_form = _fb.group({
				'id_product'					: [null, Validators.required],
				'id_product_detail'		: [null, Validators.required],
				'quantity'		        : [null, Validators.required],
				'description'	        : [null],
			});
			
			this.edit_form = _fb.group({
				'id_product'					: [null, Validators.required],
				'id_product_detail'		: [null, Validators.required],
				'quantity'		        : [null, Validators.required],
			});
		}
		
		/* -- Lifecycle Hooks Code -- */
		ngOnInit() {
      this.getMainData()
      this.getDataDropdown()
		}
		
		/* -- Function Code -- */
		OpenModal() {
			this.modalCreate.open();
    }
    
    getDataDropdown(){
      // dropdown user level
      this._api.getData('instrument_rusak/dropdown').subscribe((data: any)=> {
        this.product_list = data.product;
        this.product_detail_temp = data.product_detail;
  
        this.product_list.unshift({label:'-Select-', value:null});
      },(error: any) => {
          
      },() => {
        this._spinner.hide();
      });
    }
    
    onChangeProduct(event) {
      this.product_detail = this.product_detail_temp.filter(x => x['id_product'] === event.value);
      this.product_detail.unshift({ label: '-Pilih-', value: null });
    }

		handleRowSelect(event: any){
			this.id_rowselect = event.id;
			
			this.edit_form.get('name').setValue(event.name);
			
			this.modalEdit.open();
		}
		
		getMainData(){
			// setup column P-table
			this.cols = [
				{ field: 'product_name', header: 'Product' },
				{ field: 'product_detail_name', header: 'Item' },
				{ field: 'quantity', header: 'quantity' },
				{ field: 'created_date', header: 'tanggal' },
				{ field: 'created_by_name', header: 'dibuat oleh' },
			];
			
			// get data from API
			this._api.getData('instrument_rusak/all').subscribe((data: any)=> {
				this.maindata = data;
			},(error: any) => {
				
			},() => {
				this._spinner.hide();
			});
		}
		
		onSubmit(){
      this.add_form.value.created_by = JSON.parse(localStorage.getItem('currentUser')).data.id;
			this._api.insertData('instrument_rusak/add', this.add_form.value).subscribe((data: any) => {
				this._ts.success('Success', data.message);
				this.getMainData();
				this.modalCreate.dismiss();
			},(error: any) => {
				
			},() => {
				this._spinner.hide();
			});
			
			this.animate_effect = 'swing';
		}
		
		onSubmitEdit(){
			// get dulu id record nya
			this.edit_form.value.id = this.id_rowselect;
			
			this._api.updateData('instrument_rusak/edit', this.edit_form.value).subscribe((data: any) => {
				this._ts.success('Success', data.message);
				this.getMainData();
				this.modalEdit.dismiss();
			},(error: any) => {
				
			},() => {
				this._spinner.hide();
			});
		}
		
	}
	