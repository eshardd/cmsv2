import { Component, OnInit, ViewChild } from '@angular/core';
import { BsModalComponent  } from 'ng2-bs3-modal';
import { NgxSpinnerService } from 'ngx-spinner';
import { Api } from '../../service/api';

@Component({
  selector: 'app-topup-stok-list',
  templateUrl: './topup-stok-list.html',
  styleUrls: ['./topup-stok-list.css']
})
export class TopupStokListComponent implements OnInit {
	public title_table 			: string = 'Tabel Topup';

	public maindata				: any;
	public maindata_detail		: any;
	public cols					: any[];
	public cols_detail			: any[];
	public id_rowselect 		: number;
	public nomor_dokumen		: string;

	@ViewChild('detail')		public modalDetail	: BsModalComponent;

	constructor(
		private _api		: Api,
		private _spinner	: NgxSpinnerService
	) {

		this.cols = [
			{ field: 'no_topup', header: 'No' },
			{ field: 'from', header: 'Asal' },
			{ field: 'to', header: 'Tujuan' },
		    { field: 'created_by_name', header: 'Dibuat Oleh' },
		    { field: 'created_date', header: 'Tanggal' },
		];
		
		this.cols_detail = [
			{ field: 'product_detail', header: 'Product' },
			{ field: 'quantity', header: 'Quantity' },
    	];

	}

	/* -- Lifecycle Hooks Code -- */
	ngOnInit() {
		this.getMainData();
	}

	/* -- Function Code -- */
	handleRowSelect(event: any){
		this.id_rowselect = event.id;
		this.nomor_dokumen = event.no_topup;
		this._api.getData('warehouse-stock/topup_detail/'+event.id).subscribe((data: any)=> {
			this.maindata_detail = data;
		});
		this.modalDetail.open('lg');
	}

	getMainData(){
		// get data from API
		this._api.getData('warehouse-stock/list').subscribe((data: any)=> {
			this.maindata = data;
		});
	}
}
