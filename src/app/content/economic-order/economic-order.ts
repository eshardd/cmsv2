import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { BsModalComponent  } from 'ng2-bs3-modal';
import { NgxSpinnerService } from 'ngx-spinner';
import { Api } from '../../service/api';

@Component({
  selector: 'app-economic-order',
  templateUrl: './economic-order.html',
  styleUrls: ['./economic-order.css']
})
export class EconomicOrderComponent implements OnInit {
	public title_table 			: string = 'Tabel Product';

	public privilege_list		: any = JSON.parse(localStorage.getItem('currentUser')).data.privilege_list;

	public maindata_implant		: any;
	public maindata_instrument	: any;
	public product				: any;
	public selectedProduct		: any;
	public selectedWarehouse	: any;
	public cols					: any[];
	public id_rowselect 		: number;

	public add_form				: FormGroup;
	public edit_form			: FormGroup;
	public filter_form			: FormGroup;

	// dropdown
	public principle_list		:any;
	public dropdown				:any;
	public dropdown_filter		:any;

	//flag
	public button_export		: boolean = false;

	@ViewChild('add_instrument')	public modalAddInstrument	: BsModalComponent;
	@ViewChild('add_implant')		public modalAddImplant	: BsModalComponent;
	@ViewChild('edit_data')			public modalEdit	: BsModalComponent;

	constructor(
		private _api		: Api,
		private _fb			: FormBuilder,
		private _ts			: ToastrService,
		private _spinner	: NgxSpinnerService
	) {
		this.add_form = _fb.group({
			'product_detail'		: [null, Validators.required],
			'quantity'				: [null, Validators.required]
		});

		this.edit_form = _fb.group({
			'type'					: [null],
			'product_detail'		: [null, Validators.required],
			'quantity'				: [null, Validators.required]
		});

		this.filter_form = _fb.group({
			'product'		: [null, Validators.required],
			'warehouse'		: [null, Validators.required]
		})
	}

	/* -- Lifecycle Hooks Code -- */
	ngOnInit() {

		this.cols = [
			{ field: 'product_detail_name', header: 'Item' },
			{ field: 'quantity', header: 'EOQ' },
		];

		this.getDataProduct();
		this.getFormFilter();
	}

	getFormFilter(){
		this.dropdown_filter = [];
		this._api.getData('box/dropdown_filter').subscribe((data: any)=> {
			this.dropdown_filter = data;
			this.dropdown_filter['warehouse'] 		= data.warehouse;
			this.dropdown_filter['product'] 		= data.product;
			this.dropdown_filter['warehouse'].unshift({label:'-Select-', value:null});
			this.dropdown_filter['product'].unshift({label:'-Select-', value:null});
		});
	}

	//select_product(id_product:number){
	//	this.getItemList(id_product);
	//}

	/* -- Function Code -- */
	OpenModalAddInstrument() {
		this.modalAddInstrument.open();
	}

	OpenModalAddImplant() {
		this.modalAddImplant.open();
	}

	getDataProduct(){

		// get data from API
		this._api.getData('economic_order/all_product').subscribe((data: any)=> {
			this.product = data.product;
			this.product.unshift({label:'-Pilih product-', value:null});
		});
	}

	getItemList(){
		console.log("aaaa");
		this._api.getData('economic_order/standard/'+this.filter_form.value.product+'/'+this.filter_form.value.warehouse).subscribe((data: any)=> {
			this.maindata_implant = data.filter( x => x['type'] === '1');
			this.maindata_instrument = data.filter( x => x['type'] === '2');
			console.log(this.maindata_implant);
			this.selectedProduct = this.filter_form.value.product;
			this.selectedWarehouse = this.filter_form.value.warehouse;
		});

		this.getDataDropdown(this.filter_form.value.product);
	}

	getDataDropdown(id_product:number){
		// dropdown user level
		this.dropdown = [];
		this._api.getData('box/product/'+id_product).subscribe((data: any)=> {
			this.dropdown['implant'] = data.filter( x => x['type'] === '1');
			this.dropdown['instrument'] = data.filter( x => x['type'] === '2');
			this.dropdown['implant'].unshift({label:'-Pilih Implant-', value:null});
			this.dropdown['instrument'].unshift({label:'-Pilih Instrument-', value:null});
		});
	}

	rowSelect(event: any){
		this.id_rowselect = event.id;
		this.edit_form.get('product_detail').setValue(event.product_detail);
		this.edit_form.get('quantity').setValue(event.quantity);
		this.edit_form.get('type').setValue(event.type);
		if(event.type == 1){
			this.dropdown['editlist'] = this.dropdown['implant'];
		}else{
			this.dropdown['editlist'] = this.dropdown['instrument'];
		}

		console.log(this.edit_form.value);
		this.modalEdit.open();
	}

	submit_instrument(){
		this.add_form.value.product = this.selectedProduct;
		this.add_form.value.warehouse = this.selectedWarehouse;
		this._api.insertData('economic_order/add_instrument', this.add_form.value).subscribe((data: any) => {
			this._ts.success('Success', data.message);
			this.getItemList();
		},(error: any) => {
			this._ts.error('Error','Duplicate Data');
		},() => {
			
		});
	}

	submit_implant(){
		
		this.add_form.value.product = this.selectedProduct;
		this.add_form.value.warehouse = this.selectedWarehouse;
		this._api.insertData('economic_order/add_implant', this.add_form.value).subscribe((data: any) => {
			this._ts.success('Success', data.message);
			this.getItemList();
		},(error: any) => {
			this._ts.error('Error','Duplicate Data');
		},() => {
			this.getItemList();
		});
	}

	submit_edit(){
		this.edit_form.value.id = this.id_rowselect;
		this._api.updateData('economic_order/edit_standard', this.edit_form.value).subscribe((data: any) => {
			this._ts.success('Success', data.message);
			this.modalEdit.dismiss();

			this.getItemList();
		});
	}

	delete(param:any){
		if(confirm("Apakah anda yakin ingin menghapus data?")) {
			this._api.deleteData('economic_order/standard/'+param.id).subscribe((data: any) => {
				this._ts.success('Success', data.message);
				this.getItemList();
			});
		}
	}
}