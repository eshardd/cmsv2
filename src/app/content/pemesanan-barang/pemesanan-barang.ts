import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { BsModalComponent  } from 'ng2-bs3-modal';
import { NgxSpinnerService } from 'ngx-spinner';
import { Api } from '../../service/api';

class add_pr {
	id_user:null;
	page:number;
	date:null;
	data:any;
}

@Component({
  selector: 'app-pemesanan-barang',
  templateUrl: './pemesanan-barang.html',
  styleUrls: ['./pemesanan-barang.css']
})
export class PemesananBarangComponent implements OnInit {
	public title_table 			: string = 'Tabel Pemesanan Barang';

	public privilege_list		: any = JSON.parse(localStorage.getItem('currentUser')).data.privilege_list;
	
	public maindata				: any;
	public data_po				: any;
	public data_po_detail		: any;
	public list_po				: any;
	public po_list_id			: any;
	public cols				  	: any[];
	public add_form				: FormGroup;
	public edit_form			: FormGroup;
	public id_rowselect 		: number;
	public po_form_state		: number = 0;
	
	public store		    	= new add_pr;
	public store_temp1			= new Array;
	
	//dropdown
	public dropdown       		: any;
		
	@ViewChild('create')	public modalCreate	: BsModalComponent;
	@ViewChild('edit')		public modalEdit	: BsModalComponent;
	@ViewChild('printview')	public modalPDFview	: BsModalComponent;
	
	constructor(
		private _api		: Api,
		private _fb			: FormBuilder,
		private _ts			: ToastrService,
		private _spinner	: NgxSpinnerService
		) {
			
			this.add_form = _fb.group({
				'product'       			: [null, Validators.required],
				'product_detail'			: [null, Validators.required],
				'quantity'					  : [null, Validators.required],
				'date'					      : [null, Validators.required]
			});
			
			this.edit_form = _fb.group({
				'product'       			: [null, Validators.required],
				'product_detail'			: [null, Validators.required],
				'quantity'					  : [null, Validators.required],
				'date'					      : [null, Validators.required]
			});
		}
		
		/* -- Lifecycle Hooks Code -- */
		ngOnInit() {
			this.po_form_state = 1;
			this.getMainData();
			this.getDataDropdown();
		}
		
		/* -- Function Code -- */
		OpenModal() {
			this.po_form_state = 1;
			this.modalCreate.open('lg');
		}
		
		handleRowSelect(event: any){
			this.id_rowselect = event.id;
			
			this.edit_form.get('date').setValue(new Date(event.date));
			this.list_purchase_request();
			this.modalEdit.open('lg');
		}
		
		list_purchase_request(){
			this._api.getData('purchase_request/list/'+this.id_rowselect).subscribe((data: any) => {
				this.list_po = data;
				console.log(this.list_po);
			});
		}
		
		editPOList(select_data:any){
			this.po_form_state = 2;
			this.edit_form.get('product').setValue(select_data.id_product);
			this.select_product(select_data.id_product);
			this.edit_form.get('product_detail').setValue(select_data.id_product_detail);
			this.edit_form.get('quantity').setValue(select_data.quantity);
			this.po_list_id = select_data.id;
		}
		
		edit_list_po(){
			this.edit_form.value.po_list_id = this.po_list_id;
			this._api.updateData('purchase_request/edit_list', this.edit_form.value).subscribe((data: any) => {
				this._ts.success('Success', data.message);
				this.list_purchase_request();
			});
		}
		
		removePOList(selected_data:any){		
			if(confirm("Apakah anda yakin ingin menghapus data?")) {
				this._api.deleteData('purchase_request/remove/'+selected_data.id).subscribe((data: any) => {
					this._ts.success('Success', data.message);
					this.list_purchase_request();
				});
			}
		}
		
		getMainData(){			
			// get data from API
			this._api.getData('purchase_request/all').subscribe((data: any)=> {
				this.maindata = data;
			});
		}
		
		getDataDropdown(){
			this.dropdown = [];
			this._api.getData('purchase_request/dropdown_product').subscribe((data: any)=> {
				this.dropdown['product'] = data.product;
				this.dropdown['product'].unshift({label:'-Pilih product-', value:null});
			}); 
		}
		
		select_product(event){
			this.dropdown['product_detail'] = [];
			this._api.getData('purchase_request/dropdown_product_detail/'+event).subscribe((data: any)=> {
				this.dropdown['product_detail'] = data;
				this.dropdown['product_detail'].unshift({label:'-Pilih-', value:null});
			});
		}
		
		onSubmit(){
			this.store.date     = this.add_form.value.date;
			this.store.data     = this.store_temp1;
			this.store.id_user  = JSON.parse(localStorage.getItem('currentUser')).data.id;
			this.store.page		= 2;
			
			this._api.insertData('purchase_request/add', this.store).subscribe((data: any) => {
				this._ts.success('Success', data.message);
				this.getMainData();
				this.modalCreate.dismiss();
			});
		}
		
		onSubmitEdit(){
			// get dulu id record nya
			this.edit_form.value.id = this.id_rowselect;
			
			this._api.updateData('diagnosa-case/edit', this.edit_form.value).subscribe((data: any) => {
				this._ts.success('Success', data.message);
				this.getMainData();
				this.modalEdit.dismiss();
			},(error: any) => {
				
			},() => {
				this._spinner.hide();
			});
		}
		
		tambah_list_add(){
			this._api.getData('purchase_request/product_detail/'+this.add_form.value.product_detail).subscribe((data: any)=> {
				var valueToPush = { };
				valueToPush["product"] 				= data.id_product;
				valueToPush["product_name"] 		= data.product_name;
				valueToPush["product_detail"] 		= data.id;
				valueToPush["product_detail_code"] 	= data.code;
				valueToPush["product_detail_name"] 	= data.name;
				valueToPush["quantity"] 			= this.add_form.value.quantity;
				
				// prevent duplicate row with same id
				var store = true;
				for( var i = 0, len = this.store_temp1.length; i < len; i++ ) {
					if( this.store_temp1[i]['product_detail'] == this.add_form.value.product_detail ) {
						store = false;
					}
				}
				if (store == true) {
					this.store_temp1.push(valueToPush);
				}
				
				console.log(this.store_temp1);
			});
		}
		
		
		viewPDF(id) {
			this.data_po = [];
			this.data_po_detail = [];
			
			this._api.getData('purchase_request/view_list/' + id).subscribe((data: any) => {
				this.data_po = data.po;
				this.data_po_detail = data.list;
				//this.show_nama_product(id);
			});
			this.modalPDFview.open('lg');
		}
	}
	