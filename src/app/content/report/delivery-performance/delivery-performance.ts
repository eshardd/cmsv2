import { Component, OnInit, Input, ViewChild, NgZone } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { BsModalComponent  } from 'ng2-bs3-modal';
import { NgxSpinnerService } from 'ngx-spinner';
import { Api } from '../../../service/api';

import * as am4core from "@amcharts/amcharts4/core";
import * as am4charts from "@amcharts/amcharts4/charts";
import am4themes_animated from "@amcharts/amcharts4/themes/animated";

@Component({
  selector: 'app-delivery-performance',
  templateUrl: './delivery-performance.html',
  styleUrls: ['./delivery-performance.css']
})
export class DeliveryPerformanceComponent implements OnInit {
	public title_table 			: string = 'Top 5 Penggunaan Item / Principle';
	
	public privilege_list		: any = JSON.parse(localStorage.getItem('currentUser')).data.privilege_list;
	
	public maindata				: any;
	public rawdata				: any;
	public cols					: any[];
	public cols_raw					: any[];
	public add_form				: FormGroup;
	public edit_form			: FormGroup;
	public filter_form			: FormGroup;
	public id_rowselect 		: number;
	
	public dataCart: any;

	//dropdown
	public user_level			: any;
	public month_list			: any;

	// effect animate
	public animate_effect		: string = '';
	
	//flag
	public button_export		: boolean = false;
	public zone_				: any;
	public idChart				: any;

	
	@ViewChild('chart')		public modalChart	: BsModalComponent;
	@ViewChild('raw')		public modalRaw		: BsModalComponent;
	
	constructor(
		private _api		: Api,
		private _fb			: FormBuilder,
		private _ts			: ToastrService,
		private _spinner	: NgxSpinnerService,
		private zone		: NgZone
	) {
		
		this.add_form = _fb.group({
			'name'					: [null, Validators.required],
		});
		
		this.edit_form = _fb.group({
			'name'					: [null, Validators.required],
		});

		this.filter_form = _fb.group({
			'month'		: [null, Validators.required],
		});
	}
		
	/* -- Lifecycle Hooks Code -- */
	ngOnInit() {

		this.month_list = [
			{label:'Pilih Tipe', value:null},
			{label:'Januari', value:1},
			{label:'Februari', value:2},
			{label:'Maret', value:3},
			{label:'April', value:4},
			{label:'Mei', value:5},
			{label:'Juni', value:6},
			{label:'Juli', value:7},
			{label:'Agustus', value:8},
			{label:'September', value:9},
			{label:'Oktober', value:10},
			{label:'November', value:11},
			{label:'Desember', value:12},
		];


		this.getMainData();
		this.zone_ = this.zone;
	}

	/* -- Function Code -- */
	getMainData(){
		// setup column P-table
		this.cols = [
			{ field: 'no_ro', header: 'No RO' },
			{ field: 'operation_date', header: 'Tanggal' },
			{ field: 'atd', header: 'arrived' },
			{ field: 'delivery_time', header: 'Waktu pengiriman (Jam)' },
			{ field: 'durasi', header: 'Durasi' },
		];

		this.cols_raw = [
			{ field: 'no_ro', header: 'No RO' },
			{ field: 'operation_date', header: 'Tanggal' },
			{ field: 'courier_name', header: 'kurir' },
			{ field: 'atd', header: 'arrived' },
			{ field: 'delivery_time', header: 'Waktu pengiriman (Jam)' },
			{ field: 'durasi', header: 'Durasi' },
		];
		
		// get data from API
		this._api.getData('report/delivery_performance/all').subscribe((data: any)=> {
      		this.maindata = data.data
      		this.rawdata = data.raw
		});
	}

	onSubmitFilter(){
		// get data from API
		this._api.getData('report/delivery_performance/'+this.filter_form.value.month).subscribe((data: any)=> {
			this.maindata = data.data.filter( x => x['data']);
	  });
	}

	show_raw(){
		console.log(this.rawdata);
		this.modalRaw.open('lg');
	}

	OpenModalChart(param){
		this.idChart = param.info.name;
		this.zone_.runOutsideAngular(() => {
			let chart = am4core.create("chartdiv", am4charts.PieChart3D);
			chart.hiddenState.properties.opacity = 100; // this creates initial fade-in
			chart.legend = new am4charts.Legend();
			
			param.data.forEach(function(element) {
			chart.data.push(
				{'name':element.no_ro,'value':element.delivery_time}
				);
			});
			
			var series = chart.series.push(new am4charts.PieSeries3D());
			series.dataFields.value = "value";
			series.dataFields.category = "name";
		});
		this.modalChart.open('lg');
	}

	close_refresh(){
		this.modalChart.dismiss();
		this.ngOnInit();
		window.location.reload();
	}
}
		