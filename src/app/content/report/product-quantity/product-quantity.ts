import { Component, OnInit, Input, ViewChild, NgZone } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { BsModalComponent  } from 'ng2-bs3-modal';
import { NgxSpinnerService } from 'ngx-spinner';
import { Api } from '../../../service/api';

import * as am4core from "@amcharts/amcharts4/core";
import * as am4charts from "@amcharts/amcharts4/charts";
import am4themes_animated from "@amcharts/amcharts4/themes/animated";

@Component({
  selector: 'app-product-quantity',
  templateUrl: './product-quantity.html',
  styleUrls: ['./product-quantity.css']
})
export class ProductQuantityComponent implements OnInit {
	public title_table 			: string = 'Top 5 Penggunaan Item / Principle';
	
	public privilege_list		: any = JSON.parse(localStorage.getItem('currentUser')).data.privilege_list;
	
	public maindata_doc				: any;
	public maindata_rs				: any;
	public cols					: any[];
	public cols_rs					: any[];
	public add_form				: FormGroup;
	public edit_form			: FormGroup;
	public filter_form			: FormGroup;
	public id_rowselect 		: number;

	public rawdata				: any;
	public cols_raw				: any[];

	public rawdata_hospital		: any;
	public cols_raw_hospital	: any[];
	
	public dataCart: any;
	
	//dropdown
	public user_level			: any;
	public month_list			: any;
	
	// effect animate
	public animate_effect		: string = '';
	
	//flag
	public button_export		: boolean = false;
	public zone_				: any;
	public idChart				: any;

	
	@ViewChild('chart')		public modalChart	: BsModalComponent;
	@ViewChild('raw')		public modalRaw		: BsModalComponent;
	@ViewChild('raw_hospital')		public modalRawHospital		: BsModalComponent;
	
	constructor(
		private _api		: Api,
		private _fb			: FormBuilder,
		private _ts			: ToastrService,
		private _spinner	: NgxSpinnerService,
		private zone		: NgZone
	) {
		
		this.add_form = _fb.group({
			'name'					: [null, Validators.required],
		});
		
		this.edit_form = _fb.group({
			'name'					: [null, Validators.required],
		});

		this.filter_form = _fb.group({
			'month'		: [null, Validators.required],
		});
	}
		
	/* -- Lifecycle Hooks Code -- */
	ngOnInit() {

		this.month_list = [
			{label:'Pilih Tipe', value:null},
			{label:'Januari', value:1},
			{label:'Februari', value:2},
			{label:'Maret', value:3},
			{label:'April', value:4},
			{label:'Mei', value:5},
			{label:'Juni', value:6},
			{label:'Juli', value:7},
			{label:'Agustus', value:8},
			{label:'September', value:9},
			{label:'Oktober', value:10},
			{label:'November', value:11},
			{label:'Desember', value:12},
		];

		this.getMainData();
		this.zone_ = this.zone;
	}

	/* -- Function Code -- */
	getMainData(){
		// setup column P-table
		this.cols = [
			{ field: 'name', header: 'Doctor Name' },
			{ field: 'total_operasi', header: 'Total operasi' },
			{ field: 'total_penggunaan', header: 'Total Penggunaan' },
    	];
    
    	this.cols_rs = [
			{ field: 'name', header: 'Hospital Name' },
			{ field: 'total_operasi', header: 'Total operasi' },
			{ field: 'total_penggunaan', header: 'Total Penggunaan' },
		];


		this.cols_raw = [
			{ field: 'name', header: 'Doctor Name' },
			{ field: 'doctor_type', header: 'type' },
			{ field: 'no_ro', header: 'no RO' },
			{ field: 'date', header: 'Tanggal' },
			{ field: 'hospital_name', header: 'Hospital' },
			{ field: 'pasien', header: 'Pasien' },
			{ field: 'no_mr', header: 'No Mr' },
			{ field: 'product_name', header: 'Product' },
			{ field: 'code1', header: 'code1' },
			{ field: 'code2', header: 'code2' },
			{ field: 'code_box', header: 'box' },
			{ field: 'product_detail_name', header: 'Item' },
			{ field: 'quantity', header: 'Quantity' },
		];

		this.cols_raw_hospital = [
			{ field: 'name', header: 'Hospital' },
			{ field: 'hospital_type', header: 'Type' },
			{ field: 'area_name', header: 'Area' },
			{ field: 'subarea_name', header: 'Sub Area' },
			{ field: 'doctor_name', header: 'Doctor Name' },
			{ field: 'no_ro', header: 'No Ro' },
			{ field: 'date', header: 'tanggal' },
			{ field: 'pasien', header: 'Pasien' },
			{ field: 'no_mr', header: 'No Mr' },
			{ field: 'code1', header: 'Code 1' },
			{ field: 'code2', header: 'Code 2' },
			{ field: 'code_box', header: 'Box' },
			{ field: 'product_detail_name', header: 'Item' },
			{ field: 'quantity', header: 'Quantity' },
		];
		
		// get data from API
		this._api.getData('report/product_quantity/all').subscribe((data: any)=> {
			this.maindata_doc = data.doctor.filter( x => x['data']);
			this.maindata_rs  = data.hospital.filter( x => x['data']);
			this.rawdata  = data.raw;
			this.rawdata_hospital  = data.raw_hospital;
		});
	}

	onSubmitFilter(){
		// get data from API
		this._api.getData('report/product_quantity/'+this.filter_form.value.month).subscribe((data: any)=> {
			this.maindata_doc = data.doctor.filter( x => x['data']);
			this.maindata_rs  = data.hospital.filter( x => x['data']);
		});
	}

	show_raw(){
		console.log(this.rawdata);
		this.modalRaw.open('lg');
	}

	show_raw_hospital(){
		console.log(this.rawdata_hospital);
		this.modalRawHospital.open('lg');
	}

	OpenModalChart(param){
		this.idChart = param.info.name;
		this.zone_.runOutsideAngular(() => {
			let chart = am4core.create("chartdiv", am4charts.PieChart3D);
			chart.hiddenState.properties.opacity = 100; // this creates initial fade-in
			chart.legend = new am4charts.Legend();
			
			param.data.forEach(function(element) {
			chart.data.push(
				{'name':element.name,'value':element.total_penggunaan}
				);
			});
			
			var series = chart.series.push(new am4charts.PieSeries3D());
			series.dataFields.value = "value";
			series.dataFields.category = "name";
		});
		this.modalChart.open('lg');
	}

	close_refresh(){
		this.modalChart.dismiss();
		this.ngOnInit();
		window.location.reload();
	}
}
		