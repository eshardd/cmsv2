import { Component, OnInit } from '@angular/core';
import { Api } from '../../service/api';

declare var $: any;
declare var jQuery: any;

@Component({
	selector: 'app-home',
	templateUrl: './home.component.html',
	styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
	datanya: any;
	constructor(
		private _api: Api
	) { }


	/* -- Lifecycle Hooks Code -- */
	ngOnInit() {
		this.iCheckDeclaration();	
	}

	/* -- Function Code -- */
	iCheckDeclaration(){
		$('.i-checks').iCheck({
			checkboxClass: 'icheckbox_square-green',
			radioClass: 'iradio_square-green',
		});
	}
}
