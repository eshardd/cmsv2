import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { BsModalComponent  } from 'ng2-bs3-modal';
import { NgxSpinnerService } from 'ngx-spinner';
import { Api } from '../../service/api';
import { DISABLED } from '@angular/forms/src/model';

class count_data {
	total: any;
	free: any;
	used: any;
	maintenance: any;
}

@Component({
	selector: 'app-box',
	templateUrl: './box.html',
	styleUrls: ['./box.css']
})
export class BoxComponent implements OnInit {
	public title_table			: string = 'Tabel Box';
	
	public privilege_list		: any = JSON.parse(localStorage.getItem('currentUser')).data.privilege_list;
	
	public maindata				: any;
	public temp_maindata		: any;
	public count_maindata		= new count_data();
	
	public maindata_implant		: any;
	public data_buffer			: any;
	public maindata_instrument	: any;
	public buffer_form_state	: number = 0;
	public buffer_list_id	: any;
	
	public add_form				: FormGroup;
	public edit_form			: FormGroup;
	public filter_form			: FormGroup;
	public topup_form			: FormGroup;
	public buffer_form			: FormGroup;
	public topup_type			: any;
	
	public store_temp1			= new Array;
	public store_temp2			= new Array;
	
	public cols 				: any;
	public cols_isi_box			: any;
	public id_rowselect			: any;
	public topup_parameter		: any;
	public status_box			: any;
	
	public isi_implant			: any;
	public isi_instrument		: any;
	//dropdown
	public dropdown: any;
	public dropdown_filter: any;
	public dropdown_filter_temp: any;
	public dropdown_buffer_item: any;
	public type_box:any;
	
	//flag
	public show_rs: boolean = false;
	public selected_box	:any;
	public selected_wh	:any;
	
	@ViewChild('add_box')			public modaladdBox			: BsModalComponent;
	@ViewChild('edit_box')			public modaleditBox			: BsModalComponent;
	@ViewChild('checklist_box')		public modalchecklist		: BsModalComponent;
	@ViewChild('isi_dalam_box')		public modalisiBox			: BsModalComponent;
	@ViewChild('buffer_box')		public modalbufferBox		: BsModalComponent;
	
	constructor(
		private _api		: Api,
		private _fb			: FormBuilder,
		private _ts			: ToastrService,
		private _spinner	: NgxSpinnerService
		) {
			
			this.filter_form = _fb.group({
				'product'		: [null, Validators.required],
				'warehouse'		: [null, Validators.required],
				'status'		: [null, Validators.required],
			});
			
			this.add_form = _fb.group({
				'no_box'					: [null, Validators.required],
				'product'					: [null, Validators.required],
				'warehouse'					: [null, Validators.required],
				'status'					: [null, Validators.required],
				'type'						: [null, Validators.required],
				'rs'						: [null],
			});
			
			this.edit_form = _fb.group({
				'no_box'					: [null, Validators.required],
				'product'					: [null, Validators.required],
				'warehouse'					: [null, Validators.required],
				'status'					: [null, Validators.required],
				'type'						: [null, Validators.required],
				'rs'						: [null],
			});
			
			this.topup_form = _fb.group({
				'type'					: [null, Validators.required],
				'notes'					: [null],
			});
			
			this.buffer_form = _fb.group({
				'product_detail'			: [null, Validators.required],
				'quantity'					: [null, Validators.required]
			});
		}
		
		/* -- Lifecycle Hooks Code -- */
		ngOnInit() {
			this.buffer_form_state = 0;
			this.getDataProduct();
			this.getData();
			this.getFormFilter();
			this.topup_type = [
				{label:'Pilih Tipe', value:null},
				{label:'Standard', value:1},
				{label:'Skoliosis', value:2},
			];
			this.status_box = [
				{label:'Pilih Status', value:null},
				{label:'Stay', value:1},
				{label:'Mobile', value:2},
				{label:'Maintenance', value:3},
				{label:'Cadaver', value:4},
				{label:'OFF', value:5},
			];
			this.type_box = [
				{label:'Pilih Type', value:null},
				{label:'Standard', value:1},
				{label:'Scoliosis', value:2}
			];
		}
		
		getData(){
			this._api.getData('box/all_box').subscribe((data: any)=> {
				this.maindata = data;
				this.calculate_quantity_box(data);
			});
		}
		
		openModal() {
			this.modaladdBox.open();
		}
		
		checklist(param:any) {
			this.id_rowselect = param.id;
			this.topup_parameter = param;
			this.topup_form.get('type').setValue(param.type);
			this.cols = [
				{ field: 'code', header: 'Kode' },
				{ field: 'product_detail_name', header: 'Item' },
				{ field: 'quantity', header: 'Std' },
				{ field: 'quantity_skoliosis', header: 'Sco' },
				{ field: 'stok', header: 'Stok' }
			];

			if(this.id_rowselect == 108){
				this._api.getData('box/checklist108').subscribe((data: any)=> {
					this.maindata_implant = data.filter( x => x['type'] === '1');
					this.maindata_instrument = data.filter( x => x['type'] === '2');
				});
			}else{
				this._api.getData('box/checklist/'+param.product+'/'+param.id_warehouse).subscribe((data: any)=> {
					this.maindata_implant = data.filter( x => x['type'] === '1');
					this.maindata_instrument = data.filter( x => x['type'] === '2');
				});
			}
			this.isibox(this.id_rowselect);
			this.modalchecklist.open('lg');
		}
		
		isibox(id:any) {
			this.cols_isi_box = [
				{ field: 'code', header: 'Kode' },
				{ field: 'product_detail_name', header: 'Item' },
				{ field: 'quantity', header: 'Qty' },
				{ field: 'standar', header: 'Std' }
			];
			this._api.getData('box/isibox/'+id).subscribe((data: any)=> {
				this.isi_implant = data.filter( x => x['type'] === '1');
				this.isi_instrument = data.filter( x => x['type'] === '2');
			});
		}
		
		handleRowSelect(event: any){
			this.id_rowselect = event.id;
			this.edit_form.get('no_box').setValue(event.code_box);
			this.edit_form.get('product').setValue(event.product);
			this.edit_form.get('warehouse').setValue(event.id_warehouse);
			this.edit_form.get('status').setValue(event.status);
			this.edit_form.get('type').setValue(event.type);
			this.edit_form.get('rs').setValue(event.stay);
			
			if(event.status == 1){
				this.show_rs = true;
			}else{			
				this.show_rs = false;
			}
			
			this.modaleditBox.open();
		}
		
		select_status(param){
			if(param == 1){
				this.show_rs = true;
			}else{			
				this.show_rs = false;
			}
		}
		
		getDataProduct(){
			this.dropdown = [];
			this._api.getData('box/all_product').subscribe((data: any)=> {
				this.dropdown['product'] = data.product;
				this.dropdown['product'].unshift({label:'-Pilih product-', value:null});
				this.dropdown['product_detail'] = [];
				this.dropdown['product_detail'].unshift({label:'-Pilih product_detail-', value:null});
				this.dropdown['hospital'] = data.hospital;
				this.dropdown['hospital'].unshift({label:'-Pilih RS-', value:null});
			});
			this._api.getData('warehouse-stock/dropdown').subscribe((data: any)=> {
				this.dropdown['warehouse'] = data.warehouse;
				this.dropdown['warehouse'].unshift({label:'-Pilih Warehouse-', value:null});
			});
		}
		
		getFormFilter(){
			this.dropdown_filter = [];
			this._api.getData('box/dropdown_filter').subscribe((data: any)=> {
				this.dropdown_filter_temp = data;
				this.dropdown_filter['warehouse'] 		= data.warehouse;
				this.dropdown_filter['product'] 		= data.product;
				this.dropdown_filter['status'] 			= data.status;
				this.dropdown_filter['warehouse'].unshift({label:'All', value:0});
				this.dropdown_filter['warehouse'].unshift({label:'-Select-', value:null});
				this.dropdown_filter['product'].unshift({label:'All', value:0});
				this.dropdown_filter['product'].unshift({label:'-Select-', value:null});
				this.dropdown_filter['status'].unshift({label:'All', value:0});
				this.dropdown_filter['status'].unshift({label:'-Select-', value:null});
			});
		}
		
		buffer(param){
			this.selected_wh = param.id_warehouse;
			this.buffer_form_state = 0;
			this.selected_box = param.id;
			this.list_buffer();
			this.modalbufferBox.open('lg');
			this.getdropdownbuffer(param.product);
		}
		
		getdropdownbuffer(id_product){
			this.dropdown_buffer_item = [];
			this._api.getData('box/buffer_item_list/'+id_product).subscribe((data: any)=> {
				this.dropdown['product_detail'] = data.product_detail;
				this.dropdown['product_detail'].unshift({label:'-Pilih-', value:0});
			});
		}
		
		tambah_buffer(){
			this.buffer_form.value.id_box = this.selected_box;
			this.buffer_form.value.id_warehouse = this.selected_wh;
			this._api.insertData('box/add_buffer', this.buffer_form.value).subscribe((data: any) => {
				this._ts.success('Success', data.message);
				this.list_buffer();
			});
		}
		
		edit_buffer(){
			this.buffer_form.value.buffer_list_id = this.buffer_list_id;
			this.buffer_form.value.id_warehouse = this.selected_wh;
			this._api.updateData('box/edit_buffer', this.buffer_form.value).subscribe((data: any) => {
				this._ts.success('Success', data.message);
				this.list_buffer();
			});
		}
		
		editBufferList(select_data:any){
			this.buffer_form_state = 1;
			this.buffer_form.get('product_detail').setValue(select_data.id_product_detail);
			this.buffer_form.get('quantity').setValue(select_data.quantity);
			this.buffer_list_id = select_data.id;
		}
		
		removeBufferList(selected_data:any){		
			if(confirm("Apakah anda yakin ingin menghapus data?")) {
				this._api.deleteData('box/remove_buffer/'+selected_data.id+'/'+this.selected_wh).subscribe((data: any) => {
					this._ts.success('Success', data.message);
					this.list_buffer();
				});
			}
		}
		
		list_buffer(){
			this._api.getData('box/list_buffer/'+this.selected_box).subscribe((data: any) => {
				this.data_buffer = data;
			});
		}
		
		onSubmit(){
			this._spinner.show();
			this.add_form.value.user_id = JSON.parse(localStorage.getItem('currentUser')).data.id;
			this._api.insertData('box/add_box', this.add_form.value).subscribe((data: any) => {
				this._ts.success('Success', data.message);
			},(error: any) => {
				this._ts.error('Error','Duplicate Data');
			},() => {
				this._spinner.hide();
				this.add_form.reset();
				this.getData();
				this.modaladdBox.dismiss();
			});
		}
		
		onSubmitEdit(){
			this.edit_form.value.id = this.id_rowselect;
			this.edit_form.value.user_id = JSON.parse(localStorage.getItem('currentUser')).data.id;
			
			this._api.updateData('box/edit_box', this.edit_form.value).subscribe((data: any) => {
				this._ts.success('Success', data.message);
			},(error: any) => {
				this._ts.error('Error','Duplicate Data');
			},() => {
				this.getData();
				this.modaleditBox.dismiss();
			});
		}
		
		onSubmitFilter(){
			// console.log(this.filter_form.value.product);
			// console.log(this.filter_form.value.warehouse);
			// console.log(this.filter_form.value.status);
			// console.log(this.maindata);
			
			// var id_product = this.filter_form.value.product;
			// var id_wh = this.filter_form.value.warehouse;
			// var status = this.filter_form.value.status;
			
			// this.maindata = this.maindata.filter(function(result) {
			// 	if(result.product == id_product && result.id_warehouse == id_wh && result.status == status){
			// 		return true
			// 	}else{
			// 		return false
			// 	}
			// })
			this._api.getData('box/filter_box/'+this.filter_form.value.product+'/'+this.filter_form.value.warehouse+'/'+this.filter_form.value.status).subscribe((data: any)=> {
				this.maindata = data;
				this.temp_maindata = data;
				this.calculate_quantity_box(this.temp_maindata);
			});
		}
		
		new_topup(){
			if(confirm("Apakah anda yakin ingin Topup Box ini? Stok Gudang akan otomatis dikurangi dalam proses pengisian")) {
				this._spinner.show();
				this.topup_parameter.user_id = JSON.parse(localStorage.getItem('currentUser')).data.id;
				this.topup_parameter.type = this.topup_form.value.type;
				this.topup_parameter.notes = this.topup_form.value.notes;
				
				if(this.topup_parameter.id == 108){
					this._api.insertData('box/topup_box108', this.topup_parameter).subscribe((data: any) => {
						this._ts.success('Success', data.message);
					},(error: any) => {
						this._ts.error('Error','Duplicate Data');
					},() => {
						this._spinner.hide();
						this.modalchecklist.dismiss();
						this.getData();
					});
				}else{
					this._api.insertData('box/topup_box', this.topup_parameter).subscribe((data: any) => {
						this._ts.success('Success', data.message);
					},(error: any) => {
						this._ts.error('Error','Duplicate Data');
					},() => {
						this._spinner.hide();
						this.modalchecklist.dismiss();
						this.getData();
					});
				}
			}
		}
		
		calculate_quantity_box(data){
			this.temp_maindata = data;
			
			this.count_maindata.total = data.length;
			this.count_maindata.free = data.filter(function(result) {
				if(result.no_ro == null && result.status != 3){
					return true
				}else{
					return false
				}
			}).length
			
			this.count_maindata.used = data.filter( x => x['no_ro'] != null).length;
			this.count_maindata.maintenance = data.filter( x => (x['condition'] != 'Complete' && x['condition'] != 'Empty')).length;
		}
		
		filter_fix(param){
			if (param == 'free') {
				//this.maindata = this.temp_maindata.filter( (x => x['no_ro'] == null) && (x => x['status'] != '3') );
				
				this.maindata = this.temp_maindata.filter(function(result) {
					if(result.no_ro == null && result.status != 3){
						return true
					}else{
						return false
					}
				})
				
			}else if (param == 'used'){
				this.maindata = this.temp_maindata.filter( x => x['no_ro'] != null);
			}else if (param == 'maintenance'){
				this.maindata = this.temp_maindata.filter( x => (x['condition'] != 'Complete' && x['condition'] != 'Empty'));
			}else if (param == 'total'){
				this.maindata = this.temp_maindata;
			}
		}
	}
	