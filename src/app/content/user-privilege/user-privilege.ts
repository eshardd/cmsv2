import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, FormArray, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { BsModalComponent  } from 'ng2-bs3-modal';
import { NgxSpinnerService } from 'ngx-spinner';
import { NgForm } from '@angular/forms';
import { Api } from '../../service/api';

@Component({
  selector: 'app-user-privilege',
  templateUrl: './user-privilege.html',
  styleUrls: ['./user-privilege.css']
})
export class UserPrivilegeComponent implements OnInit {
	public title_table 			: string = 'Table Privilege';

	// privilege
	public privilege_list		: any = JSON.parse(localStorage.getItem('currentUser')).data.privilege_list;

	// data , table dan kolom
	public maindata				: any;
	public cols					: any[];
	public id_rowselect 		: number;

	//ngmodel
	public privilege			: any;
	public name					: any;

	//dropdown
	public user_level			: any;

	@ViewChild('create')	public modalCreate	: BsModalComponent;
	@ViewChild('edit')		public modalEdit	: BsModalComponent;
	@ViewChild('form')		public form			: NgForm;
	@ViewChild('formEdit')	public formEdit		: NgForm;

	constructor(
		private _api		: Api,
		private _fb			: FormBuilder,
		private _ts			: ToastrService,
		private _spinner	: NgxSpinnerService
	) {}

	/* -- Lifecycle Hooks Code -- */
	ngOnInit() {
		this.getDataUser();
		//this.getDataPrivilege();
	}

	/* -- Function Code -- */
	OpenModal() {
		this.form.reset();
		this.modalCreate.open('lg');
	}

	handleRowSelect(event: any){
		this.privilege = [];
		this.id_rowselect = event.id;
		let datatest = this.maindata.filter( x => x['id'] === this.id_rowselect);

		this.getDataPrivilegeAccess(this.id_rowselect);
		this.name 		= datatest[0]['name'];
		this.modalEdit.open('lg');
	}


	
	getDataPrivilegeAccess(id_privilege_group:number){
		//SELECT * FROM `user_privilege_access` WHERE `id_user_privilege_group` = 10
		this._api.getData('user-privilege/access/'+id_privilege_group).subscribe((data: any)=> {
			this.privilege 	= data;
		});
	}

	getDataUser(){
		this.cols = [
			{ field: 'name', header: 'Name'}
		];

		this._api.getData('user-privilege/all').subscribe((data: any)=> {
			this.maindata = data;
		});
	}

	// getDataPrivilege(){
	// 	this._api.getData('user-privilege/list').subscribe((data: any)=> {
	// 		//this.privilege_list = data;
	// 		/*this.user_level = data;
	// 		this.user_level.unshift({label:'-Select-', value:null});*/
	// 	});
	// }

	onSubmit(param: any){
		this._spinner.show();
		//console.log(param);
		this._api.insertData('user-privilege/add', param).subscribe((data: any) => {
			this._spinner.hide();
			this._ts.success('Success', data.message);
			this.getDataUser();
			this.modalCreate.dismiss();
		},(error: any) => {

		},() => {
			this.form.reset();
			this._spinner.hide();
		});
	}

	onSubmitEdit(param:any){
		/*this._spinner.show();*/
		param.id = this.id_rowselect;
		this._api.updateData('user-privilege/edit', param).subscribe((data: any) => {
			this._ts.success('Success', data.message);
			this.getDataUser();
			this.modalEdit.dismiss();
		},(error: any) => {
				
		},() => {
			this.formEdit.reset();
			this._spinner.hide();
		});
	}
}
