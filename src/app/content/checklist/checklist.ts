import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { BsModalComponent  } from 'ng2-bs3-modal';
import { NgxSpinnerService } from 'ngx-spinner';
import { Api } from '../../service/api';
import { Info_Rencana_Operasi } from '../../master/class';
import * as jsPDF from 'jspdf'; 
import html2canvas from 'html2canvas'; 
import { environment } from '../../../environments/environment';

class count_data {
	all: any;
	checked: any;
	unchecked: any;
}

@Component({
	selector: 'app-checklist',
	templateUrl: './checklist.html',
	styleUrls: ['./checklist.css']
})
export class ChecklistComponent implements OnInit {
	public title_table 			: string = 'Table Hospital';
	
	public privilege_list		: any = JSON.parse(localStorage.getItem('currentUser')).data.privilege_list;
	
	public maindata				: any;
	public temp_maindata		: any;
	public count_maindata		= new count_data();
	
	public temp_boxdata				: any;
	public boxdata				: any;
	public product_list				: any;
	public data_alat_pendukung_list : any;
	public id_rencana_operasi				: any;
	
	public cols						: any[];
	public add_form				: FormGroup;
	public edit_form			: FormGroup;
	public filter_form			: FormGroup;
	public id_rowselect 	: number;
	public checklist      : any;
	public info_ro 				= new Info_Rencana_Operasi();
	//
	public request_instrument      : any;
	public request_instrument_name : any;
	public show_information			:boolean = false;
	
	//dropdown
	public dropdown_temp	: any;
	public dropdown			: any;
	public dropdown2		: any;
	public dropdown_subarea	: any;
	
	// effect animate
	public animate_effect		: string = '';
	
	//box
	public isi_implant: any;
	public isi_instrument: any;
	
	//flag
	public button_export		: boolean = false;
	public allow_change_stage	: boolean = false;

	public API_URL = 'http://'+environment.API_URL;
	
	@ViewChild('create')	public modalCreate		: BsModalComponent;
	@ViewChild('edit')		public modalEdit		: BsModalComponent;
	@ViewChild('assign')	public modalAssign		: BsModalComponent;
	@ViewChild('print')		public modalPrint		: BsModalComponent;
	
	constructor(
		private _api		: Api,
		private _fb			: FormBuilder,
		private _ts			: ToastrService,
		private _spinner	: NgxSpinnerService,
		) {
			
			this.add_form = _fb.group({
				'no'					: [null, Validators.required],
				'name'					: [null, Validators.required],
				'type'					: [null, Validators.required],
				'area'					: [null, Validators.required],
				'subarea'				: [null, Validators.required],
			});
			
			this.edit_form = _fb.group({
				'id_ro'					: [null],
				'id_box'				: [null, Validators.required],
			});
		
			this.filter_form = _fb.group({
				'warehouse'		: [null, Validators.required],
			});
		}
		
		/* -- Lifecycle Hooks Code -- */
		ngOnInit() {
			this.getMainData();
			this.getDataDropdown();
		}
		
		downloadPDF(event){
			window.open(this.API_URL+'/print/checklist_box/'+event.id_box+'/'+this.id_rencana_operasi,'_blank');
			
			// var data = document.getElementById('cobatesyangini');
			// html2canvas(data).then(canvas => {
			// 	var imgWidth = 208;
			// 	var pageHeight = 295;
			// 	var imgHeight = canvas.height * imgWidth / canvas.width;
			// 	var heightLeft = imgHeight;
				
			// 	const contentDataURL = canvas.toDataURL('image/png')
			// 	let pdf = new jsPDF('p', 'mm', 'a4'); // A4 size page of PDF
			// 	var position = 0;
			// 	pdf.addImage(contentDataURL, 'PNG', 0, position, imgWidth, imgHeight)
			// 	pdf.save('MYPdf.pdf'); // Generated PDF
			// });
		}
		
		/* -- Function Code -- */
		OpenModal() {
			this.modalCreate.open('lg');
		}
		
		handleRowSelect(event){;
			this.id_rencana_operasi = event.id;
			this.info_ro.no_ro = event.no_ro;
			this.info_ro.date = event.date;
			this.info_ro.hospital_name = event.hospital_name;
			this.info_ro.doctor_name = event.doctor_name;
			this.info_ro.request_instrument_name = event.request_instrument_name;
			this.info_ro.jenis_tagihan_name = event.jenis_tagihan_name;
			this.info_ro.pasien = event.pasien;
			this.info_ro.type_ro_name = event.type_ro_name;
			this.info_ro.remarks = event.remarks;
			this.info_ro.status_box = event.status_box;
			this.info_ro.date = event.date;
			this.info_ro.stage = event.stage;

			this._api.getData('checklist_implant_instrument/product_list/'+event.id).subscribe((data: any)=> {
				this.product_list = data;
				this.show_information = true;
			});
			
			this._api.getData('rencana-operasi/alat_pendukung_list/' + event.id).subscribe((data: any) => {
				this.data_alat_pendukung_list = data;
			});
		}
		
		assignBox(event){
			this.id_rowselect = event.id;
			this.dropdown2 = [];
			
			this._api.getData('box/filter_box_assign/'+event.id_product+'/'+event.subarea+'/'+this.info_ro.status_box).subscribe((data: any)=> {
				// this.boxdata = data.filter( x => x['no_ro'] === null);
				// this.temp_boxdata = data.filter( x => x['no_ro'] === null);
				this.boxdata = data;
				this.temp_boxdata = data;
			});
			// this._api.getData('dropdown/box/'+event.id_product).subscribe((data: any)=> {
			// 	this.dropdown2 = data.box;
			// 	this.dropdown2.unshift({label:'-Select-', value:null});
			// });
			this.edit_form.get('id_box').setValue(null);
			this.modalAssign.open('lg');
		}
		
		onSelectBox(event){
			this.edit_form.get('id_box').setValue(event.id);
		}
		
		getMainData(){
			// setup column P-table
			this.cols = [
				{ field: 'no_ro', 			header: 'No', 			alias: ''},
				{ field: 'date', 			header: 'Tanggal', 		alias: ''},
				{ field: 'hospital_name', 	header: 'Rumah Sakit', 	alias: ''},
				{ field: 'doctor_name', 	header: 'Dokter', 		alias: ''},
			];
			
			// get data from API
			this._api.getData('checklist_implant_instrument/all').subscribe((data: any)=> {
				this.maindata = data;
				this.temp_maindata = data;
				this.calculate_quantity(data);
			});
		}

		calculate_quantity(data){
			this.temp_maindata = data;
			this.count_maindata.all = data.length;
			this.count_maindata.checked = data.filter( x => x['stage'] >2).length;
			this.count_maindata.unchecked = data.filter( x => x['stage'] == 2).length;
		}
		
		getDataDropdown(){
			this.dropdown = [];
			this._api.getData('checklist_implant_instrument/dropdown').subscribe((data: any)=> {
				this.dropdown_temp 		= data;
				this.dropdown['ro_list'] = data.ro;
				this.dropdown['subarea'] = data.subarea;
				this.dropdown['ro_list'].unshift({label:'-Select-', value:null});
				this.dropdown['subarea'].unshift({label:'-Select-', value:null});
			});
		}

		onChangeSubArea(event){
			this.boxdata = this.temp_boxdata.filter( x => x['id_subarea'] == event.value);
		}
		
		select_ro(id_ro){
			this.request_instrument = [];
			this.request_instrument_name = [];
			this._api.getData('checklist_implant_instrument/product_list/'+id_ro).subscribe((data: any)=> {
				
				if(data.request_instrument_name.indexOf(',') > -1){
					this.request_instrument_name = data.request_instrument_name.replace(" ", "-").split(',');
					this.request_instrument = data.request_instrument.replace(" ", "-").split(',');
				}else{
					this.request_instrument_name.push(data.request_instrument_name.replace(" ", "-"));
					this.request_instrument.push(data.request_instrument.replace(" ", "-"));
				}
			});
		}
		
		
		
		checklist_box(event:any){
			this.modalPrint.open('lg');
			this._api.getData('box/isibox/'+event.id_box).subscribe((data: any)=> {
				this.isi_implant = data.filter( x => x['type'] === '1');
				this.isi_instrument = data.filter( x => x['type'] === '2');
			});

			
			//window.open(this.API_URL+'/print/checklist_box/'+event.id_box+'/'+this.id_rencana_operasi,'_blank');

		}
		
		onSubmit(){
			
			this._api.insertData('hospital/add', this.add_form.value).subscribe((data: any) => {
				this._ts.success('Success', data.message);
				this.getMainData();
				this.modalCreate.dismiss();
			},(error: any) => {
				
			},() => {
				this._spinner.hide();
			});
			
			this.animate_effect = 'swing';
		}
		
		onSubmitEdit(){
			// get dulu id record nya
			this.edit_form.value.id = this.id_rowselect;
			this.edit_form.value.id_ro = this.id_rencana_operasi;
			this.edit_form.value.id_user = JSON.parse(localStorage.getItem('currentUser')).data.id;
			
			this._api.updateData('checklist_implant_instrument/assign_box', this.edit_form.value).subscribe((data: any) => {
				this._ts.success('Success', data.message);
				
				this._api.getData('checklist_implant_instrument/product_list/'+this.id_rencana_operasi).subscribe((data: any)=> {
					this.product_list = data;
				});
				
				this.modalAssign.dismiss();
			});
		}
		
		next_stage(param){
			// get dulu id record nya
			if(confirm("Apakah anda yakin ingin Update Progress rencana operasi ini?")) {
				if(this.info_ro.stage == 2 || this.info_ro.stage == 3){
					// masih boleh di update
					if(param == 'next'){
						this.edit_form.value.stage = 3;
					}else{					
						this.edit_form.value.stage = 2;
					}
					this.edit_form.value.id = this.id_rowselect;
					this.edit_form.value.id_ro = this.id_rencana_operasi;
					this.edit_form.value.id_user = JSON.parse(localStorage.getItem('currentUser')).data.id;
					this._api.updateData('checklist_implant_instrument/set_stage', this.edit_form.value).subscribe((data: any) => {
						this._ts.success('Success', data.message);
						this.getMainData();
						this.show_information = false;
						//window.location.reload();
					});
				}else{
					alert("Cannot Update.");
				}

			}
		}

		filter(param){
			this.show_information = false;
			if (param == 'all') {
				this.maindata = this.temp_maindata;
			}else if (param == 'checked'){
				this.maindata = this.temp_maindata.filter( x => x['stage'] > 2);
			}else if (param == 'unchecked'){
				this.maindata = this.temp_maindata.filter( x => x['stage'] == 2);
			}
		}
	}
	