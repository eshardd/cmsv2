import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { BsModalComponent  } from 'ng2-bs3-modal';
import { NgxSpinnerService } from 'ngx-spinner';
import { Api } from '../../service/api';
import { Info_Rencana_Operasi,spb_detail } from '../../master/class';

@Component({
  selector: 'app-spb',
  templateUrl: './spb.html',
  styleUrls: ['./spb.css']
})
export class SPBComponent implements OnInit {
	public title_table 			: string = 'Tabel Surat Pengiriman Barang';

	public maindata				: any;
	public cols					: any[];
	public add_form				: FormGroup;
	public edit_form			: FormGroup;
	public id_rowselect 		: number;
	public info_ro 				= new Info_Rencana_Operasi();
	public info_box 			: any;

	//store temp
	public store_data			= new spb_detail();
	public store_temp1			= new Array;
	public store_temp2			= new Array;

	//dropdown
	public user_level			: any;
	public dropdown				: any;

	// effect animate
	public animate_effect		: string = '';

	//flag
	public button_export		: boolean = false;

	@ViewChild('create')	public modalCreate	: BsModalComponent;
	@ViewChild('edit')		public modalEdit	: BsModalComponent;

	constructor(
		private _api		: Api,
		private _fb			: FormBuilder,
		private _ts			: ToastrService,
		private _spinner	: NgxSpinnerService
	) {

		this.add_form = _fb.group({
			'kurir'					: [null, Validators.required],
			'no_kendaraan'			: [null, Validators.required],
			'start'					: [null, Validators.required],
			'estimasi'				: [null, Validators.required],
		});

		this.edit_form = _fb.group({
			'ro'					: [null, Validators.required],
			'kurir'					: [null, Validators.required],
		});
	}

	/* -- Lifecycle Hooks Code -- */
	ngOnInit() {
		this.getMainData();
		this.getDataDropdown();
	}

	/* -- Function Code -- */
	OpenModal() {
		this.modalCreate.open();
	}

	store(){
		var valueToPush = { };
		valueToPush["id_ro"] = this.info_ro.id;
		valueToPush["no_ro"] = this.info_ro.no_ro;
		valueToPush["hospital_name"] = this.info_ro.hospital_name;
		// prevent duplicate row with same id
		var store = true;
		for( var i = 0, len = this.store_temp1.length; i < len; i++ ) {
			if( this.store_temp1[i]['id_ro'] == this.info_ro.id ) {
				store = false;
			}
		}
		if(store == true){
			this.store_temp1.push(valueToPush);
		}

		console.log(this.store_temp1);
	}

	removeList(selected_data:any){
		let data = this.store_temp1.filter( x => x['id_ro'] != selected_data.id_ro);
		this.store_temp1 = data;
	}

	submitpost(){
		if(confirm("Apakah anda yakin ingin Topup Box ini? Stok Gudang akan otomatis dikurangi dalam proses pengisian")) {
			this.store_data = this.add_form.value; 
			this.store_data.id_user = JSON.parse(localStorage.getItem('currentUser')).data.id;
			this.store_data.data = this.store_temp1;
			this._api.insertData('spb/add', this.store_data).subscribe((data: any) => {
				console.log(data);
				this._ts.success('Success', data.message);
				this.store_temp1 = new Array;
				this.add_form.reset();
			});
		}
		
	}

	getDataDropdown(){
		// dropdown user level
		this.dropdown = [];
		this._api.getData('spb/dropdown').subscribe((data: any)=> {
			this.dropdown['rencana_operasi'] = data.rencana_operasi,
			this.dropdown['kurir'] = data.kurir,
			this.dropdown['rencana_operasi'].unshift({label:'-Pilih RO-', value:null});
			this.dropdown['kurir'].unshift({label:'-Pilih Kurir-', value:null});
		});
	}

	onChangeRO(event){
		this._api.getData('spb/list/'+event.value).subscribe((data: any)=> {
			this.info_ro.id = data.id;
			this.info_ro.no_ro = data.no_ro;
			this.info_ro.hospital_name = data.hospital_name;
			this.info_ro.doctor_name = data.doctor_name;
			this.info_ro.request_instrument_name = data.request_instrument_name;
			this.info_ro.jenis_tagihan_name = data.jenis_tagihan_name;
			this.info_ro.pasien = data.pasien;
			this.info_ro.type_ro_name = data.type_ro_name;
			this.info_ro.remarks = data.remarks;
			this.info_ro.status_box = data.status_box;
			this.info_ro.date = data.date;
		});
		this._api.getData('spb/list_box/'+event.value).subscribe((data: any)=> {
			this.info_box = data;
		});
	}

	handleRowSelect(event: any){
		console.log(event);
		this.id_rowselect = event.id;

		this.edit_form.get('ro').setValue(event.id_rencana_operasi);
		this.edit_form.get('checklist').setValue(event.id_box_checklist);
		this.edit_form.get('kurir').setValue(event.courier);

		this.modalEdit.open();
	}

	getMainData(){
		// setup column P-table
		this.cols = [
			{ field: 'no_spb', header: 'No' },
			{ field: 'no_ro', header: 'RO' },
			{ field: 'courier_name', header: 'Kurir' },
		];

		// get data from API
		this._api.getData('spb/all').subscribe((data: any)=> {
			this.maindata = data;
			console.log(data);
		},(error: any) => {
				
		},() => {
			this._spinner.hide();
		});
	}

	onSubmit(){
		this.add_form.value.user_id = JSON.parse(localStorage.getItem('currentUser')).data.id;
		this._api.insertData('spb/add', this.add_form.value).subscribe((data: any) => {
			this._ts.success('Success', data.message);
			this.getMainData();
			this.modalCreate.dismiss();
		},(error: any) => {
				
		},() => {

		});

	}

	onSubmitEdit(){
		// get dulu id record nya
		this.edit_form.value.id = this.id_rowselect;
		
		console.log(this.edit_form.value);

		this._api.updateData('spb/edit', this.edit_form.value).subscribe((data: any) => {
			this._ts.success('Success', data.message);
			this.getMainData();
			this.modalEdit.dismiss();
		},(error: any) => {
				
		},() => {
			this._spinner.hide();
		});
	}

}
