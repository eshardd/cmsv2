import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { BsModalComponent  } from 'ng2-bs3-modal';
import { NgxSpinnerService } from 'ngx-spinner';
import { Api } from '../../service/api';
import * as jsPDF from 'jspdf'; 
import html2canvas from 'html2canvas'; 
import { environment } from '../../../environments/environment';

@Component({
  selector: 'app-box-topup',
  templateUrl: './box-topup.html',
  styleUrls: ['./box-topup.css']
})
export class BoxTopupComponent implements OnInit {
	public title_table 			: string = 'Tabel Box Topup';

	public maindata				: any;
	public cols					: any[];
	public add_form				: FormGroup;
	public edit_form			: FormGroup;
	public id_rowselect 		: number;
	public no_checklist 		: any;

	public maindata_implant		: any;
	public maindata_instrument	: any;

	//dropdown
	public user_level			: any;

	// effect animate
	public animate_effect		: string = '';

	//flag
	public button_export		: boolean = false;

	public API_URL = 'http://'+environment.API_URL;

	@ViewChild('create')	public modalCreate	: BsModalComponent;
	@ViewChild('edit')		public modalEdit	: BsModalComponent;

	constructor(
		private _api		: Api,
		private _fb			: FormBuilder,
		private _ts			: ToastrService,
		private _spinner	: NgxSpinnerService
	) {

		this.add_form = _fb.group({
			'name'					: [null, Validators.required],
		});

		this.edit_form = _fb.group({
			'name'					: [null, Validators.required],
		});
	}

	/* -- Lifecycle Hooks Code -- */
	ngOnInit() {
		this.getMainData();
	}

	/* -- Function Code -- */
	OpenModal() {
		this.modalCreate.open();
	}

	handleRowSelect(event: any){
		this.id_rowselect = event.id;
		this.no_checklist = event.no_checklist;

		this._api.getData('checklist/detail/'+this.id_rowselect).subscribe((data: any)=> {
			this.maindata_implant = data.filter( x => x['type'] === '1');
			this.maindata_instrument = data.filter( x => x['type'] === '2');
			console.log(event);
		});

		this.modalEdit.open('lg');
	}

	getMainData(){
		// setup column P-table
		this.cols = [
			{ field: 'no_topup', header: 'No Topup' },
			{ field: 'code_box', header: 'Kode Box' },
			{ field: 'created_date', header: 'dibuat Tanggal' },
			{ field: 'name', header: 'dibuat Oleh' },
		];

		// get data from API
		this._api.getData('checklist/all').subscribe((data: any)=> {
			this.maindata = data;
		},(error: any) => {
				
		},() => {
			this._spinner.hide();
		});
	}

	print(event){
		console.log(event);
		window.open(this.API_URL+'/print/box_topup/'+event.id,'_blank');
	}

	onSubmit(){
		this._api.insertData('diagnosa-case/add', this.add_form.value).subscribe((data: any) => {
			this._ts.success('Success', data.message);
			this.getMainData();
			this.modalCreate.dismiss();
		},(error: any) => {
				
		},() => {
			this._spinner.hide();
		});

		this.animate_effect = 'swing';
	}

	onSubmitEdit(){
		// get dulu id record nya
		this.edit_form.value.id = this.id_rowselect;
		
		this._api.updateData('diagnosa-case/edit', this.edit_form.value).subscribe((data: any) => {
			this._ts.success('Success', data.message);
			this.getMainData();
			this.modalEdit.dismiss();
		},(error: any) => {
				
		},() => {
			this._spinner.hide();
		});
	}

	downloadPDF(){
		
		var data = document.getElementById('cobatesyangini');
		html2canvas(data).then(canvas => {
			var imgWidth = 208;
			var pageHeight = 295;
			var imgHeight = canvas.height * imgWidth / canvas.width;
			var heightLeft = imgHeight;
			 
			const contentDataURL = canvas.toDataURL('image/png')
			let pdf = new jsPDF('p', 'mm', 'a4'); // A4 size page of PDF
			var position = 0;
			pdf.addImage(contentDataURL, 'PNG', 0, position, imgWidth, imgHeight)
			pdf.save('MYPdf.pdf'); // Generated PDF
		});
	}
}
