import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { BsModalComponent  } from 'ng2-bs3-modal';
import { NgxSpinnerService } from 'ngx-spinner';
import { Api } from '../../service/api';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-warehouse-stock-opname-result',
  templateUrl: './warehouse-stock-opname-result.html',
  styleUrls: ['./warehouse-stock-opname-result.css']
})
export class WarehouseStockOpnameResultComponent implements OnInit {

	public privilege_list		: any = JSON.parse(localStorage.getItem('currentUser')).data.privilege_list;

	public title_table 			: string = 'Stock Opname Results';
  public maindata				  : any;
  public id_so				  : any;
	rowGroupMetadata: any;
	cols: any;

	constructor(
		private _api		  : Api,
		private _fb			  : FormBuilder,
		private _ts			  : ToastrService,
		private _spinner	: NgxSpinnerService,
		private _route		: ActivatedRoute
	) {
		this.id_so = this._route.snapshot.paramMap.get('id');
	}

	/* -- Lifecycle Hooks Code -- */
	ngOnInit() {

		this.cols = [
			{ field: 'product_name', header: 'Product' },
			{ field: 'product_detail_name', header: 'Name' },
			{ field: 'total_stock_data', header: 'Total Stok Data' },
			{ field: 'stock_actual', header: 'Total Stok Actual' },
			{ field: 'selisih', header: 'Selisih' },
		];






		this.getMainData();
	}

	/* -- Function Code -- */
	getMainData(){
		// get data from API
		this._api.getData('stock_opname/result/'+this.id_so).subscribe((data: any)=> {
			this.maindata = data;
			this.updateRowGroupMetaData();
		});
	}

	updateRowGroupMetaData() {
    this.rowGroupMetadata = {};
    if (this.maindata) {
      for (let i = 0; i < this.maindata.length; i++) {
        let rowData = this.maindata[i];
        let product_name = rowData.product_name;
        if (i == 0) {
          this.rowGroupMetadata[product_name] = { index: 0, size: 1 };
        }
        else {
          let previousRowData = this.maindata[i - 1];
          let previousRowGroup = previousRowData.product_name;
          if (product_name === previousRowGroup)
            this.rowGroupMetadata[product_name].size++;
          else
            this.rowGroupMetadata[product_name] = { index: i, size: 1 };
        }
      }
    }
	}
	}
