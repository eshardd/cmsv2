import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { BsModalComponent  } from 'ng2-bs3-modal';
import { NgxSpinnerService } from 'ngx-spinner';
import { Api } from '../../service/api';
import * as jsPDF from 'jspdf'; 
import html2canvas from 'html2canvas';
import { Info_Rencana_Operasi } from '../../master/class';
import { environment } from '../../../environments/environment';

class count_data {
	all: any;
	on_the_way: any;
	arrived: any;
}

class Spb {
	no_spb;
	courier_name;
	etd;
	no_kendaraan;
	kendaraan;
}

@Component({
	selector: 'app-pengiriman-barang',
	templateUrl: './pengiriman-barang.html',
	styleUrls: ['./pengiriman-barang.css']
})
export class PengirimanBarangComponent implements OnInit {
	public title_table 			: string = 'Tabel Diagnosa Case';
	
	public privilege_list		: any = JSON.parse(localStorage.getItem('currentUser')).data.privilege_list;
	
	public maindata				: any;
	public temp_maindata		: any;
	public count_maindata		= new count_data();
	
	public data_spb				= new Spb();
	public printdata			: any;
	public cols					: any[];
	public add_form				: FormGroup;
	public edit_form			: FormGroup;
	public id_rowselect 		: number;
	public info_ro 				= new Info_Rencana_Operasi();
	public info_box 			: any;
	public info_alat_pendukung  : any;
	
	//dropdown
	public dropdown				: any;
	
	// effect animate
	public animate_effect		: string = '';
	
	//flag
	public button_export		: boolean = false;
	
	public API_URL = 'http://'+environment.API_URL;

	@ViewChild('create')	public modalCreate	: BsModalComponent;
	@ViewChild('edit')		public modalEdit	: BsModalComponent;
	@ViewChild('printview')		public modalPrint	: BsModalComponent;
	
	constructor(
		private _api		: Api,
		private _fb			: FormBuilder,
		private _ts			: ToastrService,
		private _spinner	: NgxSpinnerService
		) {
			
			this.add_form = _fb.group({
				'kurir'					: [null, Validators.required],
				'no_kendaraan'			: [null, Validators.required],
				'start'					: [null, Validators.required],
				'estimasi'				: [null, Validators.required],
				'id_ro'					: [null, Validators.required],
			});
			
			this.edit_form = _fb.group({
				'kurir'					: [null, Validators.required],
				'no_kendaraan'			: [null, Validators.required],
				'etd'					: [null, Validators.required],
				'eta'					: [null, Validators.required],
				'atd'					: [null, Validators.required],
				'receiver'				: [null, Validators.required],
			});
		}
		
		/* -- Lifecycle Hooks Code -- */
		ngOnInit() {
			this.getMainData();
			this.getDataDropdown();
		}
		
		/* -- Function Code -- */
		OpenModal() {
			this.add_form.reset();
			this.info_ro = new Info_Rencana_Operasi();
			this.info_box = null;
			this.info_alat_pendukung = null;
			this.modalCreate.open('lg');
		}
		
		onChangeRO(event){
			this._api.getData('spb/list/'+event.value).subscribe((data: any)=> {
				this.info_ro.id = data.id;
				this.info_ro.no_ro = data.no_ro;
				this.info_ro.hospital_name = data.hospital_name;
				this.info_ro.doctor_name = data.doctor_name;
				this.info_ro.request_instrument_name = data.request_instrument_name;
				this.info_ro.jenis_tagihan_name = data.jenis_tagihan_name;
				this.info_ro.pasien = data.pasien;
				this.info_ro.type_ro_name = data.type_ro_name;
				this.info_ro.remarks = data.remarks;
				this.info_ro.status_box = data.status_box;
				this.info_ro.date = data.date;
			});
			this._api.getData('spb/list_box/'+event.value).subscribe((data: any)=> {
				this.info_box = data;
			});
			this._api.getData('spb/list_alat_pendukung/'+event.value).subscribe((data: any)=> {
				this.info_alat_pendukung = data;
			});
		}
		
		downloadPDF(){
			var data = document.getElementById('cobatesyangini');
			html2canvas(data).then(canvas => {
				var imgWidth = 208;
				var pageHeight = 295;
				var imgHeight = canvas.height * imgWidth / canvas.width;
				var heightLeft = imgHeight;
				
				const contentDataURL = canvas.toDataURL('image/png')
				let pdf = new jsPDF('p', 'mm', 'a4'); // A4 size page of PDF
				var position = 0;
				pdf.addImage(contentDataURL, 'PNG', 0, position, imgWidth, imgHeight)
				pdf.save('MYPdf.pdf'); // Generated PDF
			});
		}
		
		getDataDropdown(){
			// dropdown user level
			this.dropdown = [];
			this._api.getData('spb/dropdown').subscribe((data: any)=> {
				this.dropdown['rencana_operasi'] = data.rencana_operasi,
				this.dropdown['kurir'] = data.kurir,
				this.dropdown['kendaraan'] = data.kendaraan,
				this.dropdown['rencana_operasi'].unshift({label:'-Pilih RO-', value:null});
				this.dropdown['kurir'].unshift({label:'-Pilih Kurir-', value:null});
				this.dropdown['kendaraan'].unshift({label:'-Pilih Kendaraan-', value:null});
			});
		}
		
		handleRowSelect(event: any){
			this.id_rowselect = event.id;
			
			this._api.getData('spb/list/'+event.id_ro).subscribe((data: any)=> {
				this.info_ro.id = data.id;
				this.info_ro.no_ro = data.no_ro;
				this.info_ro.hospital_name = data.hospital_name;
				this.info_ro.doctor_name = data.doctor_name;
				this.info_ro.request_instrument_name = data.request_instrument_name;
				this.info_ro.jenis_tagihan_name = data.jenis_tagihan_name;
				this.info_ro.pasien = data.pasien;
				this.info_ro.type_ro_name = data.type_ro_name;
				this.info_ro.remarks = data.remarks;
				this.info_ro.status_box = data.status_box;
				this.info_ro.date = data.date;
			});
			this._api.getData('spb/list_box/'+event.id_ro).subscribe((data: any)=> {
				this.info_box = data;
			});
			this._api.getData('spb/list_alat_pendukung/'+event.id_ro).subscribe((data: any)=> {
				this.info_alat_pendukung = data;
			});
			
			
			this.edit_form.get('kurir').setValue(event.courier);
			this.edit_form.get('no_kendaraan').setValue(event.no_kendaraan);
			this.edit_form.get('etd').setValue(new Date(event.etd));
			this.edit_form.get('eta').setValue(new Date(event.eta));
			this.edit_form.get('atd').setValue(new Date(event.atd));
			this.edit_form.get('receiver').setValue(event.receiver);
			
			this.modalEdit.open('lg');
		}
		
		getMainData(){
			// get data from API
			this._api.getData('spb/all').subscribe((data: any)=> {
				this.maindata = data;
				this.temp_maindata = data;
				this.calculate_quantity(data);
			},(error: any) => {
				
			},() => {
				this._spinner.hide();
			});
		}
		
		calculate_quantity(data){
			this.temp_maindata = data;
			
			this.count_maindata.all = data.length;
			this.count_maindata.on_the_way = data.filter( x => x['atd'] == null).length;
			this.count_maindata.arrived = data.filter( x => x['atd'] != null).length;
		}
		
		print(event){
			console.log(event);
			window.open(this.API_URL+'/print/spb/'+event.id+'/'+event.id_ro,'_blank');
			// this.data_spb.no_spb = event.no_spb;
			// this.data_spb.courier_name = event.courier_name;
			// this.data_spb.etd = event.etd;
			// this.data_spb.no_kendaraan = event.no_kendaraan;
			// this.data_spb.kendaraan = event.kendaraan;
			// this._api.getData('spb/printview/'+event.id).subscribe((data: any)=> {
			// 	this.printdata = data;
			// },(error: any) => {
				
			// },() => {
			// 	this._spinner.hide();
			// });
			
			// this.modalPrint.open('lg');
		}
		
		onSubmit(){
			this._spinner.show();
			this.add_form.value.id_user = JSON.parse(localStorage.getItem('currentUser')).data.id;
			this._api.insertData('spb/add', this.add_form.value).subscribe((data: any) => {
				this._ts.success('Success', data.message);
				this.getMainData();
				this.modalCreate.dismiss();
				this._spinner.hide();
			});
		}
		
		onSubmitEdit(){
			this._spinner.show();
			// get dulu id record nya
			this.edit_form.value.id = this.id_rowselect;
			this.edit_form.value.id_user = JSON.parse(localStorage.getItem('currentUser')).data.id;
			
			this._api.updateData('spb/edit', this.edit_form.value).subscribe((data: any) => {
				this._ts.success('Success', data.message);
				this.getMainData();
				this.modalEdit.dismiss();
				this.getDataDropdown();
				this._spinner.hide();
			});
		}
		
		filter_fix(param){
			if (param == 'all') {
				this.maindata = this.temp_maindata;
			}else if (param == 'on_the_way'){
				this.maindata = this.temp_maindata.filter( x => x['atd'] == null);
			}else if (param == 'arrived'){
				this.maindata = this.temp_maindata.filter( x => x['atd'] != null);
			}
		}
	}
	