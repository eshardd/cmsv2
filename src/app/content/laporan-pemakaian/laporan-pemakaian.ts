import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { BsModalComponent  } from 'ng2-bs3-modal';
import { NgxSpinnerService } from 'ngx-spinner';
import { Api } from '../../service/api';
import { environment } from '../../../environments/environment';

class LaporanPemakaian {
	info: any;
	detail: any;
}

class LaporanPemakaianDetail {
	id_laporan_pemakaian: any;
	detail: any;
}

@Component({
  selector: 'app-laporan-pemakaian',
  templateUrl: './laporan-pemakaian.html',
  styleUrls: ['./laporan-pemakaian.css']
})
export class LaporanPemakaianComponent implements OnInit {
	public title_table 			: string = 'Tabel Laporan Pemakaian';

	public privilege_list		: any = JSON.parse(localStorage.getItem('currentUser')).data.privilege_list;

	//store temp
	public store				= new LaporanPemakaian();
	public store_detail			= new LaporanPemakaianDetail();
	public store_temp1			= new Array;

	API_URL = 'http://'+environment.API_URL;

	public maindata				: any;
	public maindata_detail		: any;
	public cols					: any[];
	public add_form				: FormGroup;
	public edit_form			: FormGroup;
	public tambahan_form		: FormGroup;
	public filter_form			: FormGroup;
	public id_rowselect 		: number;

	//dropdown
	public dropdown_temp		: any;
	public dropdown				: any;

	// effect animate
	public animate_effect		: string = '';

	//flag
	public loading		: boolean = false;
	public button_export		: boolean = false;
	public integrated_status	: boolean = false;

	@ViewChild('create')	public modalCreate	: BsModalComponent;
	@ViewChild('edit')		public modalEdit	: BsModalComponent;
	@ViewChild('tambahan')	public modalTambahan: BsModalComponent;

	constructor(
		private _api		: Api,
		private _fb			: FormBuilder,
		private _ts			: ToastrService,
		private _spinner	: NgxSpinnerService
	) {

		this.add_form = _fb.group({
			'tanggal'					: [null, Validators.required],
			'dokter'					: [null, Validators.required],
			'hospital'					: [null, Validators.required],
			'pasien'					: [null, Validators.required],
			'no_mr'						: [null, Validators.required],
			'product'					: [null],
			'subdist'					: [null],
			'box'						: [null, Validators.required],
			'product_detail'			: [null, Validators.required],
			'quantity'					: [null, Validators.required],
		});

		this.edit_form = _fb.group({
			'tanggal'					: [null, Validators.required],
			'dokter'					: [null, Validators.required],
			'hospital'					: [null, Validators.required],
			'pasien'					: [null, Validators.required],
			'no_mr'						: [null, Validators.required],
			'subdist'					: [null],
			'product'					: [null],
			'product_detail'			: [null],
			'quantity'					: [null],
			'box'						: [null],
		});

		this.tambahan_form = _fb.group({
			'id'					: [null],
			'product'					: [null],
			'product_detail'			: [null],
			'quantity'					: [null],
		});

		this.filter_form 	= _fb.group({
			'date': [''],
			'month': [''],
			'year': [''],
		});
	}

	/* -- Lifecycle Hooks Code -- */
	ngOnInit() {
		this.getDataDropdown();
		this.getMainData();
	}

	/* -- Function Code -- */
	OpenModal() {
		this.modalCreate.open('lg');
	}

	getDataDropdown(){
		this.dropdown = [];
		this.dropdown_temp = [];
		this._api.getData('laporan-pemakaian/dropdown').subscribe((data: any)=> {
			this.dropdown_temp = data;
			this.dropdown['dokter'] = data.dokter;
			this.dropdown['hospital'] = data.hospital;
			this.dropdown['product'] = data.product;
			this.dropdown['subdist'] = data.subdist;

			this.dropdown['product'].unshift({ label: '-Pilih-', value: null });
			this.dropdown['hospital'].unshift({ label: '-Pilih-', value: null });
			this.dropdown['dokter'].unshift({ label: '-Pilih-', value: null });
			this.dropdown['subdist'].unshift({ label: '-Pilih-', value: null });
		});

		this.dropdown['date'] = [
			{value:1,label:'1'},
			{value:2,label:'2'},
			{value:3,label:'3'},
			{value:4,label:'4'},
			{value:5,label:'5'},
			{value:6,label:'6'},
			{value:7,label:'7'},
			{value:8,label:'8'},
			{value:9,label:'9'},
			{value:10,label:'10'},
			{value:11,label:'11'},
			{value:12,label:'12'},
			{value:13,label:'13'},
			{value:14,label:'14'},
			{value:15,label:'15'},
			{value:16,label:'16'},
			{value:17,label:'17'},
			{value:18,label:'18'},
			{value:19,label:'19'},
			{value:20,label:'20'},
			{value:21,label:'21'},
			{value:22,label:'22'},
			{value:23,label:'23'},
			{value:24,label:'24'},
			{value:25,label:'25'},
			{value:26,label:'26'},
			{value:27,label:'27'},
			{value:28,label:'28'},
			{value:29,label:'29'},
			{value:30,label:'30'},
			{value:31,label:'31'},
		];

		this.dropdown['month'] = [
			{value:1,label:'Jan'},
			{value:2,label:'Feb'},
			{value:3,label:'Mar'},
			{value:4,label:'Apr'},
			{value:5,label:'Mei'},
			{value:6,label:'Jun'},
			{value:7,label:'Jul'},
			{value:8,label:'Aug'},
			{value:9,label:'Sep'},
			{value:10,label:'Oct'},
			{value:11,label:'Nov'},
			{value:12,label:'Des'},
		];

		this.dropdown['year'] = [
			{value:2019,label:'2019'},
		];

		this.dropdown['date'].unshift({ label: '-all-', value: null });
		this.dropdown['month'].unshift({ label: '-all-', value: null });
		this.dropdown['year'].unshift({ label: '-all-', value: null });
	}

	onSubmitFilter(){
		// setup column P-table
		this.cols = [
			{ field: 'date', header: 'Tanggal' },
			{ field: 'doctor_name', header: 'Dokter' },
			{ field: 'hospital_name', header: 'Rumah sakit' },
			{ field: 'pasien', header: 'Pasien' },
			{ field: 'no_mr', header: 'No MR' },
		];

		// get data from API
		this._api.insertData('laporan-pemakaian/filter',this.filter_form.value).subscribe((data: any)=> {
			this.maindata = data;
		},(error: any) => {
				
		},() => {
			this._spinner.hide();
		});
	}

	onChangeBox(event:any){
		var id_box = event.value;
		this._api.getData('laporan-pemakaian/product_detail_list/'+id_box).subscribe((data: any)=> {
			this.dropdown['product_detail'] = data.product_detail;
			this.dropdown['product_detail'].unshift({ label: '-Pilih-', value: null });
		});
		// this.dropdown['product_detail'] = this.dropdown_temp['product_detail'].filter( x => x['id_product'] === id_product);
		// this.dropdown['product_detail'].unshift({ label: '-Pilih-', value: null });
		
	}

	onChangeProduct(event:any){
		var id_product = event.value;
		this._api.getData('laporan-pemakaian/product_item_list/'+id_product).subscribe((data: any)=> {
			this.dropdown['product_detail'] = data.product_detail;
			this.dropdown['product_detail'].unshift({ label: '-Pilih-', value: null });
		});
		// this.dropdown['product_detail'] = this.dropdown_temp['product_detail'].filter( x => x['id_product'] === id_product);
		// this.dropdown['product_detail'].unshift({ label: '-Pilih-', value: null });
		
	}

	select_hospital(event){
		this.dropdown['box_list'] = [];
		this._api.getData('laporan-pemakaian/dropdown_box_list/'+event).subscribe((data: any)=> {
			this.dropdown['box_list'] = data.box;
			this.dropdown['box_list'].unshift({label:'-Pilih-', value:null});
		});
	}

	handleRowSelect(event: any){
		this.id_rowselect = event.id;

		if (event.integrated == 0){
			this.integrated_status = false;
		}else{
			this.integrated_status = true;
		}

		this.select_hospital(event.id_hospital);
		this.edit_form.get('dokter').setValue(event.id_dokter);
		this.edit_form.get('hospital').setValue(event.id_hospital);
		this.edit_form.get('pasien').setValue(event.pasien);
		this.edit_form.get('no_mr').setValue(event.no_mr);
		this.edit_form.get('product').setValue(event.product);

		this._api.getData('laporan-pemakaian/detail/'+event.id).subscribe((data: any)=> {
			this.maindata_detail = data;
		});

		this.modalEdit.open('lg');
	}

	product_request(event: any){
		this.id_rowselect = event.id;

		if (event.integrated == 0){
			this.integrated_status = false;
		}else{
			this.integrated_status = true;
		}

		this.tambahan_form.get('product').setValue(event.product);

		this._api.getData('laporan-pemakaian/detail/'+event.id).subscribe((data: any)=> {
			this.maindata_detail = data;
		});

		this.modalTambahan.open('lg');
	}

	getMainData(){
		// setup column P-table
		this.cols = [
			{ field: 'doctor_name', header: 'Dokter' },
			{ field: 'hospital_name', header: 'Rumah sakit' },
			{ field: 'pasien', header: 'Pasien' },
			{ field: 'no_mr', header: 'No MR' },
		];

		// get data from API
		this._api.getData('laporan-pemakaian/all').subscribe((data: any)=> {
			this.maindata = data;
		},(error: any) => {
				
		},() => {
			this._spinner.hide();
		});
	}

	add_to_table(){
		this._api.insertData('laporan-pemakaian/product_detail',this.add_form.value).subscribe((data: any)=> {
			var valueToPush = { };
			valueToPush["product_code"] 		= data.code;
			valueToPush["box"] 					= data.id_box;
			valueToPush["code_box"]				= data.code_box;
			valueToPush["product_detail"] 		= data.id_product_detail;
			valueToPush["product_detail_name"] 	= data.name;
			valueToPush["quantity"] 			= this.add_form.value.quantity;
			
			// prevent duplicate row with same id
			var store = true;
			for( var i = 0, len = this.store_temp1.length; i < len; i++ ) {
				if( this.store_temp1[i]['product_detail'] == this.add_form.value.product_detail) {
					store = false;
				}
			}
			if (store == true) {
				this.store_temp1.push(valueToPush);
			}
		});
	}

	add_tambahan_to_table(){
		// get dulu id record nya
		this.tambahan_form.value.id = this.id_rowselect;
		
		if(this.tambahan_form.value.quantity > 0){
			
			this._api.insertData('laporan-pemakaian/tambahan', this.tambahan_form.value).subscribe((data: any) => {
				this._ts.success('Success', data.message);
				this.getMainData();
				//this.modalTambahan.dismiss();
				this._api.getData('laporan-pemakaian/detail/'+this.id_rowselect).subscribe((data: any)=> {
					this.maindata_detail = data;
				});
			});
		}else{
			alert("quantity harus lebih dari 0");
		}
	}

	add_to_table_data(){
		this.store_detail.id_laporan_pemakaian = this.id_rowselect;
		this.store_detail.detail = this.edit_form.value;
		this._api.insertData('laporan-pemakaian/add_detail', this.store_detail).subscribe((data: any) => {
			this._ts.success('Success', data.message);
			this._api.getData('laporan-pemakaian/detail/'+this.id_rowselect).subscribe((data: any)=> {
				this.maindata_detail = data;
			});
			this.modalCreate.dismiss();
		});
	}

	removeList(selected_data:any){
		let data = this.store_temp1.filter( x => x['product_detail'] != selected_data.product_detail);
		this.store_temp1 = data;
	}

	removeListData(selected_data:any){
		if(confirm("Apakah anda yakin ingin menghapus data?")) {
			this._api.deleteData('laporan-pemakaian/detail/'+selected_data.id).subscribe((data: any) => {
				this._ts.success('Success', data.message);
				this._api.getData('laporan-pemakaian/detail/'+this.id_rowselect).subscribe((data: any)=> {
					this.maindata_detail = data;
				});
			});
		}
	}

	onSubmit(){
		this.add_form.value.id_user = JSON.parse(localStorage.getItem('currentUser')).data.id;
		this.store.info = this.add_form.value;
		this.store.detail = this.store_temp1;
		this._api.insertData('laporan-pemakaian/add', this.store).subscribe((data: any) => {
			this._ts.success('Success', data.message);
			this.getMainData();
			this.modalCreate.dismiss();
		});
	}

	onSubmitEdit(){
		// get dulu id record nya
		this.edit_form.value.id = this.id_rowselect;
		
		this._api.updateData('laporan-pemakaian/edit', this.edit_form.value).subscribe((data: any) => {
			this._ts.success('Success', data.message);
			this.getMainData();
			this.modalEdit.dismiss();
		},(error: any) => {
				
		},() => {
			this._spinner.hide();
		});
	}

	syncronize(event){
		console.log(event);
		if(confirm("Apakah anda yakin Akan melakukan Syncronize? Stok Box akan disesuaikan dalam proses ini, harap mengecek kembali penggunaan barang")) {
			this._api.updateData('laporan-pemakaian/syncronize', event).subscribe((data: any) => {
				this._ts.success('Success', data.message);
				this.getMainData();
			});
		}
	}

	master_excel(){
		window.open(this.API_URL+'/print/master_lp/','_blank');
		// this._api.getData('dashboard/master').subscribe((data: any)=> {
		// 	this.masterdata = data;
		// 	this.modalMaster.open('lg');
		// });
	}

}
