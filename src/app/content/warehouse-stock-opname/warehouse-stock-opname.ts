import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';
import { Api } from '../../service/api';
import { ActivatedRoute } from '@angular/router';

class Postdata {
	id_so		: string;
  data 		: any;
}

@Component({
  selector: 'app-warehouse-stock-opname',
  templateUrl: './warehouse-stock-opname.html',
  styleUrls: ['./warehouse-stock-opname.css']
})
export class WarehouseStockOpnameComponent implements OnInit {

	public privilege_list		: any = JSON.parse(localStorage.getItem('currentUser')).data.privilege_list;

	public selected_product_detail 			: string;
	public selected_product 				: string;
	public selected_warehouse 				: string;
	public selected_product_detail_label 	: string;
  public show_add				: boolean = false;
  public data_product_detail	: any;
  public postdata	= new Postdata();
	public id_so:any;
	public wh_name:any;

  public sisa_stok;
  public data_stok;
  public dropdown_form_temp	: any;
	public dropdown_form		: any;

  public filter_form			: FormGroup;

  constructor(
		private _api			: Api,
		private _fb				: FormBuilder,
		private _ts				: ToastrService,
		private _spinner	: NgxSpinnerService,
		private _route		: ActivatedRoute
	) {
		this.id_so = this._route.snapshot.paramMap.get('id');
    const filter = _fb.group({
			'warehouse'		: [null, Validators.required],
			'principle'		: [null, Validators.required],
			'product'			: [null, Validators.required]
    });
    
    this.filter_form 	= filter;
  }

	/* -- Lifecycle Hooks Code -- */
	ngOnInit() {
		this.getdataParam(this.id_so);
		this.getDataForm();
	}
	
	getdataParam(id_so:number){ // not optimized
		this._api.getData('stock_opname/order/'+id_so).subscribe((data: any)=> {
			this.selected_warehouse = data.id_warehouse;
			this.wh_name = data.warehouse_name;
		});
	}
  
  getdata(){
    this._api.getData('stock_opname/wh/1').subscribe((data: any)=> {
      this.data_stok['data'] = data.filter( x => x['id_product'] === "12");
    });
  }

  select_principle(id_principle){
		this.dropdown_form['product'] = this.dropdown_form_temp.product.filter( x => x['id_principle'] === id_principle);
		this.dropdown_form['product'].unshift({label:'-Select-', value:null});
	}

  getDataForm(){
		this.dropdown_form = [];
		this._api.getData('warehouse-stock/form_dropdown').subscribe((data: any)=> {
			this.dropdown_form_temp = data;
			this.dropdown_form['principle'] 			= data.principle;
			this.dropdown_form['product'] 				= [];
			this.dropdown_form['product_detail'] 	= [];
			this.dropdown_form['principle'].unshift({label:'-Select-', value:null});
			this.filter_form.get('warehouse').setValue("1");
		});
  }
  
  onSubmitFilter(){
		this.show_add 						= false;
		this.data_stok 						= null;
		this._api.getData('stock_opname/wh/'+this.selected_warehouse+'/'+this.id_so).subscribe((data: any)=> {
			this.data_stok 					= data.filter( x => x['id_product'] === this.filter_form.value.product);
			console.log(this.data_stok);
		});
	}

  submit_data(){		
		this._spinner.show();
		this.postdata.id_so 	= this.id_so;
		this.postdata.data 		= this.data_stok;
		this._api.insertData('stock_opname/add',this.postdata).subscribe((data: any)=> {
			if(data){
				this._ts.success('Success', data.message);
				this._spinner.hide();
			}
		});
  }
}
