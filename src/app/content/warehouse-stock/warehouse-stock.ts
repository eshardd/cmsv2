import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, FormArray, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { BsModalComponent  } from 'ng2-bs3-modal';
import { NgxSpinnerService } from 'ngx-spinner';
import { Api } from '../../service/api';

@Component({
  selector: 'app-warehouse-stock',
  templateUrl: './warehouse-stock.html',
  styleUrls: ['./warehouse-stock.css']
})

export class WarehouseStockComponent implements OnInit {

	public privilege_list		: any = JSON.parse(localStorage.getItem('currentUser')).data.privilege_list;

	// tampung param filter untuk menghindari bug
	public selected_product_detail 			: string;
	public selected_product 				: string;
	public selected_warehouse 				: string;
	public selected_product_detail_label 	: string;

	// privilege
	public show_add				: boolean = false;

	// data , table dan kolom
	public maindata				: any;
	public data_product_detail	: any;
	public data_stok			: any;
	public sisa_stok			: any;
	public cols					: any[];
	public cols_product_detail	: any[];
	public cols_stok			: any[];
	public plus_form			: FormGroup;
	public minus_form			: FormGroup;
	public edit_form			: FormGroup;
	public filter_form			: FormGroup;
	public id_rowselect 		: number;

	//dropdown
	public dropdown				: any;
	public dropdown_form_temp	: any;
	public dropdown_form		: any;

	public warehouse			: any;
	public principle			: any;
	public product				: any;
	public product_detail		: any;

	@ViewChild('create')		public modalCreate	: BsModalComponent;
	@ViewChild('create_plus')	public modalCreate_plus	: BsModalComponent;
	@ViewChild('create_minus')	public modalCreate_minus	: BsModalComponent;
	@ViewChild('edit')			public modalEdit	: BsModalComponent;

	constructor(
		private _api		: Api,
		private _fb			: FormBuilder,
		private _ts			: ToastrService,
		private _spinner	: NgxSpinnerService
	) {

		this.cols_product_detail = [
			{ field: 'code', header: 'Code'},
			{ field: 'name', header: 'Nama'},
			{ field: 'sisa', header: 'Sisa'},
		];

		this.cols_stok = [
			{ field: 'created_date', header: 'Tanggal'},
			{ field: 'in', header: 'Masuk'},
			{ field: 'out', header: 'Keluar'},
			{ field: 'description', header: 'Keterangan'},
		];

		const filter = _fb.group({
			'warehouse'		: [null, Validators.required],
			'principle'		: [null, Validators.required],
			'product'		: [null, Validators.required]
		});

		const plus = _fb.group({
			'in'			: [null],
			'description'	: [null, Validators.required],
		});

		const minus = _fb.group({
			'out'			: [null],
			'description'	: [null, Validators.required],
		});

		const edit = _fb.group({
			'id'			: [null],
			'in'			: [null],
			'out'			: [null],
			'description'	: [null, Validators.required],
		});

		this.filter_form 	= filter;
		this.plus_form 		= plus;
		this.minus_form 	= minus;
		this.edit_form 		= edit;
	}

	/* -- Lifecycle Hooks Code -- */
	ngOnInit() {
		this.getDataDropdown();
		this.getDataForm();
	}

	OpenModal_plus() {
		this.modalCreate_plus.open('sm');
	}

	OpenModal_minus() {
		this.modalCreate_minus.open('sm');
	}
	
	show_stock_card(event: any){
		console.log(event.data);
		this.selected_product_detail 		= event.data.id;
		this.selected_product_detail_label 	= event.data.name;
		this.selected_product 				= event.data.id_product;
		this.show_add 						= true;
		this.getData(event.data.id);
	}

	getDataForm(){
		this.dropdown_form = [];
		this._api.getData('warehouse-stock/form_dropdown').subscribe((data: any)=> {
			this.dropdown_form_temp = data;
			this.dropdown_form['warehouse'] 		= data.warehouse;
			this.dropdown_form['principle'] 		= data.principle;
			this.dropdown_form['product'] 			= [];
			this.dropdown_form['product_detail'] 	= [];
			this.dropdown_form['principle'].unshift({label:'-Select-', value:null});
			this.filter_form.get('warehouse').setValue("1");
		},(error: any) => {
				
		},() => {
			this._spinner.hide();
		});
	}

	getData(selected_product_detail){
		this._spinner.show();
		this._api.getData('warehouse-stock/wh/'+this.filter_form.value.warehouse+'/'+selected_product_detail).subscribe((data: any)=> {
			this.data_stok = data.stok.filter( x => x['id_product_detail'] === selected_product_detail);
			this.sisa_stok = data.sisa;
		});
	}

	getDataDropdown(){
		this._api.getData('warehouse-stock/dropdown').subscribe((data: any)=> {
			this.dropdown = data;
			this.warehouse 				= data.warehouse;
			this.product_detail 		= data.product_detail;
		},(error: any) => {
				
		},() => {
			this._spinner.hide();
		});
	}

	select_principle(id_principle){
		this.dropdown_form['product'] = this.dropdown_form_temp.product.filter( x => x['id_principle'] === id_principle);
		this.dropdown_form['product'].unshift({label:'-Select-', value:null});
	}

	select_product(id_product){
		this._api.getData('warehouse-stock/pd_filter/'+id_product).subscribe((data: any)=> {
			this.data_product_detail = data;
		});
	}

	edit_data(event:any){
		this.edit_form.get('id').setValue(event.data.id);
		this.edit_form.get('in').setValue(event.data.in);
		this.edit_form.get('out').setValue(event.data.out);
		this.edit_form.get('description').setValue(event.data.description);
		this.modalEdit.open();
	}

	onSubmit(param){
		if(param == 'plus'){
			this.plus_form.value.warehouse 		= this.filter_form.value.warehouse; 
			this.plus_form.value.product_detail = this.selected_product_detail;
			this.plus_form.value.id_user 		= JSON.parse(localStorage.getItem('currentUser')).data.id;
			this._api.insertData('warehouse-stock/add', this.plus_form.value).subscribe((data: any) => {
				this._ts.success('Success', data.message);
				this.modalCreate_plus.dismiss();
				this.getData(this.selected_product_detail);
				this.reload_product_detail_list();
				this.plus_form.reset();
			});
		}else if(param == 'minus'){
			this.minus_form.value.warehouse 		= this.filter_form.value.warehouse; 
			this.minus_form.value.product_detail 	= this.selected_product_detail;;
			this.minus_form.value.id_user 			= JSON.parse(localStorage.getItem('currentUser')).data.id;
			this._api.insertData('warehouse-stock/add', this.minus_form.value).subscribe((data: any) => {
				this._ts.success('Success', data.message);
				this.modalCreate_minus.dismiss();
				this.getData(this.selected_product_detail);
				this.reload_product_detail_list();
				this.minus_form.reset();
			});
		}
	}

	onSubmitEdit(){
		this._api.updateData('warehouse-stock/edit', this.edit_form.value).subscribe((data: any) => {
			this._ts.success('Success', data.message);
			this.getData(this.selected_product_detail);
			this.modalEdit.dismiss();
		});
	}

	onSubmitFilter(){
		this.show_add = false;
		this.data_stok = null;
		this.selected_warehouse 			= this.filter_form.value.warehouse;
		this._api.getData('warehouse-stock/pd_filter/'+this.filter_form.value.warehouse+'/'+this.filter_form.value.product).subscribe((data: any)=> {
			this.data_product_detail = data;
		});
	}

	reload_product_detail_list(){
		console.log(this.selected_warehouse);
		console.log(this.selected_product);
		this._api.getData('warehouse-stock/pd_filter/'+this.selected_warehouse+'/'+this.selected_product).subscribe((data: any)=> {
			this.data_product_detail = data;
		});
	}
}
