import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, FormArray, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { BsModalComponent  } from 'ng2-bs3-modal';
import { NgxSpinnerService } from 'ngx-spinner';
import { Api } from '../../service/api';

declare var $		: any;
declare var jQuery	: any;

@Component({
	selector: 'app-user',
	templateUrl: './user.html',
	styleUrls: ['./user.css']
})
export class UserComponent implements OnInit {
	public title_table 			: string = 'Tabel User';

	// privilege
	public privilege			: any;
	public privilege_list		: any = JSON.parse(localStorage.getItem('currentUser')).data.privilege_list;

	// data , table dan kolom
	public maindata				: any;
	public cols					: any[];
	public add_form				: FormGroup;
	public edit_form			: FormGroup;
	public id_rowselect 		: number;

	//dropdown
	public user_level			: any;
	public user_privilege		: any;
	public user_division		: any;
	public user_parent			: any;

	@ViewChild('create')	public modalCreate	: BsModalComponent;
	@ViewChild('edit')		public modalEdit	: BsModalComponent;

	constructor(
		private _api		: Api,
		private _fb			: FormBuilder,
		private _ts			: ToastrService,
		private _spinner	: NgxSpinnerService
	) {

		this.add_form = _fb.group({
			'name'					: [null, Validators.required],
			'email'					: [null, Validators.email],
			'username'				: [null, Validators.required],
			'level'					: [null, Validators.required],
			'privilege'				: [null, Validators.required],
			'division'				: [null, Validators.required],
			'parent'				: [null]
		});

		this.edit_form = _fb.group({
			'name'					: [null, Validators.required],
			'email'					: [null, Validators.email],
			'username'				: [null, Validators.required],
			'level'					: [null, Validators.required],
			'privilege'				: [null, Validators.required],
			'division'				: [null, Validators.required],
			'parent'				: [null]
		});
	}

	/* -- Lifecycle Hooks Code -- */
	ngOnInit() {
		this.getDataUser();
		this.getDataDropdown();
	}

	/* -- Function Code -- */
	OpenModal() {
		this.modalCreate.open();
	}

	handleRowSelect(event: any){
		this.id_rowselect = event.id;

		this.edit_form.get('name').setValue(event.name);
		this.edit_form.get('username').setValue(event.username);
		this.edit_form.get('email').setValue(event.email);
		this.edit_form.get('level').setValue(event.level);
		this.edit_form.get('privilege').setValue(event.privilege);
		this.edit_form.get('division').setValue(event.division);
		this.edit_form.get('parent').setValue(event.parent);

		this.modalEdit.open();
	}

	getDataUser(){
		this.cols = [
			{ field: 'name', header: 'Nama'},
			//{ field: 'username', header: 'Username'},
			{ field: 'email', header: 'Email'},
			{ field: 'level_name', header: 'Level'},
			{ field: 'privilege_name', header: 'Privilege'}
		];

		this._api.getData('user/all').subscribe((data: any)=> {
			this.maindata = data;
		});
	}

	getDataDropdown(){
		this._api.getData('user/dropdown').subscribe((data: any)=> {
			this.user_level 		= data.level;
			this.user_privilege 	= data.privilege;
			this.user_division 		= data.division;
			this.user_parent 		= data.parent
			this.user_level.unshift({label:'-Select-', value:null});
			this.user_privilege.unshift({label:'-Select-', value:null});
			this.user_division.unshift({label:'-Select-', value:null});
			this.user_parent.unshift({label:'-Select-', value:null});
		});
	}

	onChangeAdd(value: string, isChecked: boolean) {
		const privilege = <FormArray>this.add_form.controls.privilege;

		if (isChecked) {
		  privilege.push(new FormControl(value));
		} else {
		  let index = privilege.controls.findIndex(x => x.value == value)
		  privilege.removeAt(index);
		}
	}

	onChangeEdit(value: string, isChecked: boolean) {
		const privilege = <FormArray>this.edit_form.controls.privilege;

		if (isChecked) {
		  privilege.push(new FormControl(value));
		} else {
		  let index = privilege.controls.findIndex(x => x.value == value)
		  privilege.removeAt(index);
		}
	}

	onSubmit(){
		this._spinner.show();
		this._api.insertData('user/add', this.add_form.value).subscribe((data: any) => {
			this._spinner.hide();
			this._ts.success('Success', data.message);
			this.getDataUser();
			this.modalCreate.dismiss();
		},(error: any) => {
				
		},() => {
			this._spinner.hide();
		});
	}

	onSubmitEdit(){
		this._spinner.show();
		this.edit_form.value.id = this.id_rowselect;
		
		this._api.updateData('user/edit', this.edit_form.value).subscribe((data: any) => {
			this._ts.success('Success', data.message);
			this.getDataUser();
			this.modalEdit.dismiss();
		},(error: any) => {
				
		},() => {
			this._spinner.hide();
		});
	}

	forgot_password(){
		
		
		if(confirm("Are you sure to Reset Password?")) {
		    this._spinner.show();
			this.edit_form.value.id = this.id_rowselect;
			this._api.updateData('user/forgot_password', this.edit_form.value).subscribe((data: any) => {
				this._ts.success('Success', data.message);
			},(error: any) => {
					
			},() => {
				this._spinner.hide();
			});
		}

	}
}
