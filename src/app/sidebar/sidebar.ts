import { Component, OnInit } from '@angular/core';
import { Api } from '../service/api';
import { DomSanitizer } from '@angular/platform-browser';
declare var $:any;

@Component({
	selector: 'app-sidebar',
	templateUrl: './sidebar.html',
	styleUrls: ['./sidebar.css']
})
export class SidebarComponent implements OnInit {

	selected_sidebar: string = '';
	public user_id = JSON.parse(localStorage.getItem('currentUser')).data.id;
	public privilege_id = JSON.parse(localStorage.getItem('currentUser')).data.privilege;
	public privilege_list: any;
	public display:any;
	datauser:any;

	constructor(
		private _api: Api,
		private _sanitize	: DomSanitizer,
		) { }

	ngOnInit() {
		this.userProfile();
		$("#side-menu").hide();
		setTimeout(function () {
			$("#side-menu").metisMenu();
			$("#side-menu").show();
		}, 0);
		var datauser 		= JSON.parse(localStorage.currentUser).data;
		this.privilege_list = datauser.privilege_list;
	}

	userProfile(){
		this.datauser = [];
		this._api.getData('profile/user/'+this.user_id).subscribe((data: any)=> {
			this.datauser['name'] = data.name;
			this.datauser['email'] = data.email;
			this.datauser['image'] = data.image;
		});
	}

	showImage(img:string) {
		return this._sanitize.bypassSecurityTrustUrl(img);
	}

	setactive(name: string){
		this.selected_sidebar = name;
	}

	logout(){

		localStorage.clear();
		//location.reload();
	}
}
	