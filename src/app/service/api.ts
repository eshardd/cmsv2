import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { environment } from '../../environments/environment';

@Injectable()
export class Api{
	
	public apiurl	: string = 'http://'+environment.API_URL+'/';
	private valid	: boolean = true;
	
	private httpExternal	: any;
	private httpClient_header	: any;
	private httpClient_header_restrict	: any;
	private jwtAuth	: boolean = false;
	
	constructor(
		private _httpClient: HttpClient,
		){
			let currentUser = JSON.parse(localStorage.getItem('currentUser'));
			
			this.httpExternal = {
				headers: new HttpHeaders({
					'Content-Type': 'application/json'
				})
			};
			
			this.httpClient_header = {
				headers: new HttpHeaders({
					'Content-Type': 'application/json',
					'Authorization': 'Bearer '+currentUser.token
				})
			};
			
			this.httpClient_header_restrict = {
				headers: new HttpHeaders({
					'Content-Type': 'application/json',
					'Authorization': currentUser.token
				})
			};
		}
		
		public getData(table: string) {
			if(this.jwtAuth){
				
				this._httpClient.get(this.apiurl+'verified', this.httpClient_header_restrict).subscribe((r) => {
				},(error: any) => {
					this.valid = false;
					alert('Session Expired #2197162617');
					localStorage.removeItem('currentUser');
				},()=>{});
			}
			
			if(this.valid == true){
				return this._httpClient.get(this.apiurl+table, this.httpClient_header);
			}
		}
		
		public insertData(table: string, body: object) {
			if(this.jwtAuth){
				this._httpClient.get(this.apiurl+'verified', this.httpClient_header_restrict).subscribe((r) => {
					
				},(error: any) => {
					this.valid = false;
					alert('Access Denied #2197162617');
					localStorage.removeItem('currentUser');
				},()=>{});
			}
			
			if(this.valid == true){
				return this._httpClient.post(this.apiurl+table, body, this.httpClient_header);
			}
		}
		
		public updateData(table: string, body: object) {
			if(this.jwtAuth){
				this._httpClient.get(this.apiurl+'verified', this.httpClient_header_restrict).subscribe((r) => {
					
				},(error: any) => {
					this.valid = false;
					alert('Access Denied #2197162617');
					localStorage.removeItem('currentUser');
				},()=>{});
			}
			
			if(this.valid == true){
				return this._httpClient.put(this.apiurl+table, body, this.httpClient_header);
			}
		}
		
		public deleteData(table: string) {
			if(this.jwtAuth){
				this._httpClient.get(this.apiurl+'verified', this.httpClient_header_restrict).subscribe((r) => {
					
				},(error: any) => {
					this.valid = false;
					alert('Access Denied #2197162617');
					localStorage.removeItem('currentUser');
				},()=>{});
			}
			
			if(this.valid == true){
				return this._httpClient.delete(this.apiurl+table, this.httpClient_header);
			}
		}
		
		public getUrlApi(url: string) {
			return this._httpClient.get(url, this.httpExternal);
		}
	}