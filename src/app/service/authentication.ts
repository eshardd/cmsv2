import { Injectable }     from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Router } from '@angular/router';
import { environment } from '../../environments/environment';

@Injectable()
export class Authentication {
	public token: string;
	private apiurl = 'http://'+environment.API_URL+'/';
	private header: any;

	constructor(
		private _httpClient: HttpClient, 
		private _router: Router
	) {
		this.header = new HttpHeaders({
			'Access-Control-Allow-Origin': '*',
			'Access-Control-Allow-Methods': 'GET, POST, OPTIONS, PUT, PATCH, DELETE',
			'Access-Control-Allow-Headers': 'X-Requested-With, Content-Type, Accept, Origin, Authorization'
		});
	}

	public login(body: object){
		return this._httpClient.post(this.apiurl+'authenticate/login', body, this.header);
	}

	public logout(): void {
		this.token = null;
		localStorage.removeItem('currentUser');
		this._router.navigate(['Login']);
	}
}
