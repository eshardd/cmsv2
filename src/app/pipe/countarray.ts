import { PipeTransform, Pipe } from '@angular/core';
@Pipe({
    name: 'countarray',
    pure: false
})
export class CountArrayPipe implements PipeTransform {
    transform(items:string, arraydata:any): any {
        let hitung = 0;

        /*
            pakai for karena ada array data yang isinya null
            biar bisa di validasi
        */

        for ( let a of arraydata ){
            if(a.id != null){
                hitung = hitung+1;
            }
        }
        return items+' ('+hitung+')';
    }
}