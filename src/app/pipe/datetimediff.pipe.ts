import { Pipe, PipeTransform } from '@angular/core';
//import * as moment from 'moment-timezone';
import * as moment from 'moment';

@Pipe({name: 'datetimediff'})
export class DatetimediffPipe implements PipeTransform {
	transform(date: any): any {
		//let indon = moment.tz(date, "Asia/Jakarta");
		//let datenow = new Date();
		//let datecreated = new Date(data[0][0]['date_inserted']);
		//let datecreated = new Date(date);
		//let ms = moment(datenow,"DD/MM/YYYY HH:mm:ss").diff(moment(datecreated,"DD/MM/YYYY HH:mm:ss"));
		//let d = moment.duration(ms);
		//let result = moment(new Date(date), "DD/MM/YYYY HH:mm:ss").startOf('hour').fromNow();
		//console.log(d.days(), d.hours(), d.minutes(), d.seconds());
		//let result = d.hours()+':'+d.minutes()+' Hour ago';

		let datenow = new Date();
		let datecreated = new Date(date);
		let ms = moment(datenow,"DD/MM/YYYY HH:mm:ss").diff(moment(datecreated,"DD/MM/YYYY HH:mm:ss"));
		let d = moment.duration(ms);
		let result = '';
		let day = d.days();
		let hour = d.hours();
		let min = d.minutes();
		let sec = d.seconds();

		if(day == 0 && hour == 0 && min == 0 && sec == 0){
			result = 'a second ago';
		}else if(day == 0 && hour > 0 && min > 0 && sec > 0){
			if(hour > 0) {
				result = hour+' hour ago';
			} else {
				result = 'a hour ago';
			}
		}else if (day == 0 && hour == 0 && min > 0 && sec > 0) { 
			if(min > 0) {
				result = min+' minutes ago';
			} else {
				result = 'a minutes ago';
			}
		} else if(day == 0 && hour == 0 && min ==0 && sec > 0){
			if(sec > 0) {
				result = sec+' seconds ago';
			} else {
				result = 'a seconds ago';
			}
		}else if(day > 0){
			if(day > 0) {
				result = day+' day ago';
			} else {
				result = 'a day ago';
			}
		}

		return result;
	}
}