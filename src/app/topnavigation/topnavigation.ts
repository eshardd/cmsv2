import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-topnavigation',
  templateUrl: './topnavigation.html',
  styleUrls: ['./topnavigation.css']
})
export class TopnavigationComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }


  logout(){

  	localStorage.clear();
  	//location.reload();
  }
}
