import { Component, OnInit, DoCheck } from '@angular/core';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  	selector: 'app-root',
  	templateUrl: './app.component.html',
  	styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, DoCheck {
  	display:any;

  	constructor(
  		private location: Location,
  		private router: Router,
  		private _spinner	: NgxSpinnerService
	){}

  	public ngOnInit() {
		//const myLocation = this.location.path();
	}

	public ngDoCheck() {
		this.display = localStorage.getItem('currentUser');
		
		if(this.display == null) {
			document.body.classList.add('gray-bg');
		} else {
			document.body.classList.remove('gray-bg');	
		}
	}
}
