export class spb_detail {
	id_user 	: any;
	kurir		: any;
    no_kendaraan: any;
    start		: any;
    estimasi	: any;
    data		: any;
}

export class Wh_store_data {
	from		: any;
    to 			: any;
    id_user 	: any;
    data		: any;
}
export class Wh_store_data_detail {
	from		: number;
    to 			: number;
    id_user 	: number;
}

export class Info_Rencana_Operasi {
	remarks: string;
	box_status_name: string;
	case: string;
	case_name: string;
	created_by: string;
	created_date: string;
	date: string;
	doctor_name: string;
	hospital_name: string;
	id: string;
	id_doctor: string;
	id_hospital: string;
	id_spine_anatomi: string;
	instrument_name: string;
	jenis_operasi: string;
	jenis_tagihan: string;
	jenis_tagihan_name: string;
	last_update: string;
	no_mr: string;
	no_ro: string;
	pasien: string;
	request_implant: string;
	request_instrument: string;
	request_instrument_name: string;
	spine_anatomi_name: string;
	status_box: string;
	status_ro: string;
	status_ro_name: string;
	subcase: string;
	subcase_name: string;
	technical_support: string;
	technical_support_name: string;
	type_ro: string;
	type_ro_name: string;
	user_name: string;
	integrated: string;
	stage: number;
}