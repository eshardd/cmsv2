import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// front page
import { LoginComponent } from './login/login';

// content page
import { UserComponent } from './content/user/user';
import { RencanaOperasiComponent } from './content/rencana-operasi/rencana-operasi';
import { WarehouseStockComponent } from './content/warehouse-stock/warehouse-stock';
import { WarehouseLocationComponent } from './content/warehouse-location/warehouse-location';
import { ProfileComponent } from './content/profile/profile';
import { UserPrivilegeComponent } from './content/user-privilege/user-privilege';
import { BoxComponent } from './content/box/box';
import { BoxStandardComponent } from './content/box-standard/box-standard';
import { BoxTopupComponent } from './content/box-topup/box-topup';
//import { SPBComponent } from './content/spb/spb';
import { TopupStokListComponent } from './content/topup-stok-list/topup-stok-list';
import { WarehouseStockOpnameComponent } from './content/warehouse-stock-opname/warehouse-stock-opname';
import { WarehouseStockOpnameOrderComponent } from './content/warehouse-stock-opname-order/warehouse-stock-opname-order';
import { WarehouseStockOpnameListComponent } from './content/warehouse-stock-opname-list/warehouse-stock-opname-list';
import { WarehouseStockOpnameResultComponent } from './content/warehouse-stock-opname-result/warehouse-stock-opname-result';
import { WarehouseReplacementComponent } from './content/warehouse-replacement/warehouse-replacement';
import { PengirimanBarangComponent } from './content/pengiriman-barang/pengiriman-barang';
import { PenggunaanBarangComponent } from './content/penggunaan-barang/penggunaan-barang';
import { PenarikanBarangComponent } from './content/penarikan-barang/penarikan-barang';
import { EconomicOrderComponent } from './content/economic-order/economic-order';
import { EconomicOrderAlertComponent } from './content/economic-order-alert/economic-order-alert';
import { LaporanPemakaianComponent } from './content/laporan-pemakaian/laporan-pemakaian';
import { PurchaseRequestComponent } from './content/purchase-request/purchase-request';
import { PemesananBarangComponent } from './content/pemesanan-barang/pemesanan-barang';
import { PurchaseRequestDetailComponent } from './content/purchase-request/purchase-request-detail/purchase-request-detail';
import { PurchaseReceiveComponent } from './content/purchase-receive/purchase-receive';
import { PurchaseReceiveDetailComponent } from './content/purchase-receive/purchase-receive-detail/purchase-receive-detail';
import { AreaSalesComponent } from './content/report/area-sales/area-sales';
import { ProductQuantityComponent } from './content/report/product-quantity/product-quantity';
import { DeliveryPerformanceComponent } from './content/report/delivery-performance/delivery-performance';
import { InstrumentRusakComponent } from './content/instrument-rusak/instrument-rusak';
import { ReportInstrumentRusakComponent } from './content/report/report-instrument-rusak/report-instrument-rusak';
import { TopInstrumentComponent } from './content/report/top-instrument/top-instrument';
import { TopImplantComponent } from './content/report/top-implant/top-implant';
import { SummaryStokComponent } from './content/summary-stok/summary-stok';
import { PotongRodComponent } from './content/potong-rod/potong-rod';

// master
import { PrincipleComponent } from './content/master/principle/principle';
import { DoctorComponent } from './content/master/doctor/doctor';
import { HospitalComponent } from './content/master/hospital/hospital';
import { ProductComponent } from './content/master/product/product';
import { ProductDetailComponent } from './content/master/product-detail/product-detail';
import { WarehouseComponent } from './content/master/warehouse/warehouse';
import { DiagnosaCaseComponent } from './content/master/diagnosa-case/diagnosa-case';
import { DiagnosaSubcaseComponent } from './content/master/diagnosa-subcase/diagnosa-subcase';
import { JenisTagihanComponent } from './content/master/jenis-tagihan/jenis-tagihan';
import { SpineAnatomiComponent } from './content/master/spine-anatomi/spine-anatomi';
import { TopupStokComponent } from './content/topup-stok/topup-stok';
import { ChecklistComponent } from './content/checklist/checklist';
import { DashboardComponent } from './content/dashboard/dashboard';
import { KendaraanComponent } from './content/master/kendaraan/kendaraan';
import { DivisionComponent } from './content/master/division/division';
import { AreaComponent } from './content/master/area/area';
import { SubareaComponent } from './content/master/subarea/subarea';
import { SubdistComponent } from './content/master/subdist/subdist';

// report
import { TopPrincipleComponent } from './content/report/top-principle/top-principle';

const routes: Routes = [
	{ path: '', component: ProfileComponent},
	{ path: 'login', component: LoginComponent},
	{ path: 'user', component: UserComponent},
	{ path: 'principle', component: PrincipleComponent},
	{ path: 'doctor', component: DoctorComponent},
	{ path: 'hospital', component: HospitalComponent},
	{ path: 'product', component: ProductComponent},
	{ path: 'productdetail', component: ProductDetailComponent},
	{ path: 'warehouse', component: WarehouseComponent},
	{ path: 'rencana-operasi', component: RencanaOperasiComponent},
	{ path: 'warehouse-stock', component: WarehouseStockComponent},
	{ path: 'warehouse-location', component: WarehouseLocationComponent},
	{ path: 'diagnosa-case', component: DiagnosaCaseComponent},
	{ path: 'diagnosa-subcase', component: DiagnosaSubcaseComponent},
	{ path: 'jenis-tagihan', component: JenisTagihanComponent},
	{ path: 'spine-anatomi', component: SpineAnatomiComponent},
	{ path: 'profile', component: ProfileComponent},
	{ path: 'userprivilege', component: UserPrivilegeComponent},
	{ path: 'boxlist', component: BoxComponent},
	{ path: 'boxtopup', component: BoxTopupComponent},
	{ path: 'boxstandard', component: BoxStandardComponent},
	{ path: 'topupstok', component: TopupStokComponent},
	{ path: 'topupstoklist', component: TopupStokListComponent},
	{ path: 'checklist', component: ChecklistComponent},
	{ path: 'warehouse-stock-opname/:id', component: WarehouseStockOpnameComponent},
	{ path: 'warehouse-stock-opname-order', component: WarehouseStockOpnameOrderComponent},
	{ path: 'warehouse-stock-opname-list', component: WarehouseStockOpnameListComponent},
	{ path: 'warehouse-stock-opname-result/:id', component: WarehouseStockOpnameResultComponent},
	{ path: 'warehouse-replacement', component: WarehouseReplacementComponent},
	{ path: 'send-item', component: PengirimanBarangComponent},
	{ path: 'penggunaan-barang', component: PenggunaanBarangComponent},
	{ path: 'retract-item', component: PenarikanBarangComponent},
	{ path: 'economic-order', component: EconomicOrderComponent},
	{ path: 'economic-order-alert', component: EconomicOrderAlertComponent},
	{ path: 'dashboard', component: DashboardComponent},
	{ path: 'laporan-pemakaian', component: LaporanPemakaianComponent},
	{ path: 'purchase-request', component: PurchaseRequestComponent},
	{ path: 'purchase-request-detail/:id', component: PurchaseRequestDetailComponent},
	{ path: 'pemesanan-barang', component: PemesananBarangComponent},
	{ path: 'kendaraan', component: KendaraanComponent},
	{ path: 'division', component: DivisionComponent},
	{ path: 'area', component: AreaComponent},
	{ path: 'subarea', component: SubareaComponent},
	{ path: 'top-principle', component: TopPrincipleComponent},
	{ path: 'purchase-receive', component: PurchaseReceiveComponent},
	{ path: 'purchase-receive-detail/:id', component: PurchaseReceiveDetailComponent},
	{ path: 'area-sales', component: AreaSalesComponent},
	{ path: 'product-quantity', component: ProductQuantityComponent},
	{ path: 'delivery-performance', component: DeliveryPerformanceComponent},
	{ path: 'instrument-rusak', component: InstrumentRusakComponent},
	{ path: 'report-instrument-rusak', component: ReportInstrumentRusakComponent},
	{ path: 'top-instrument', component: TopInstrumentComponent},
	{ path: 'top-implant', component: TopImplantComponent},
	{ path: 'summary-stok', component: SummaryStokComponent},
	{ path: 'subdist', component: SubdistComponent},
	{ path: 'potongrod', component: PotongRodComponent},	
]

@NgModule({
	imports: [RouterModule.forRoot(routes)],
	exports: [RouterModule]
})
export class AppRoutingModule { }
